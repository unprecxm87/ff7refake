﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum CheckPoint 
{
    Open,
    FirstEasy, 
    SecondEasy, 
    ThirdEasy, 
    Middle, 
    Boss 
}
public static class StorySystem
{
    private static bool open;
    private static bool firstEasy; 
    private static bool secondEasy; 
    private static bool thirdEasy; 
    private static bool middle; 
    private static bool boss;
    public static Dictionary<CheckPoint, bool> Progress = new Dictionary<CheckPoint, bool>()
    {
        { CheckPoint.Open, open },
        { CheckPoint.FirstEasy, firstEasy },
        { CheckPoint.SecondEasy, secondEasy },
        { CheckPoint.ThirdEasy, thirdEasy },
        { CheckPoint.Middle, middle },
        { CheckPoint.Boss, boss}
    };
}
