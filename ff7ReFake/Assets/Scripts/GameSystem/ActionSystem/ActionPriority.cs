﻿public enum ActionPriority
{
    Empty = 0,
    Move = 0,
    Fight = 1,
    Slash = 1,
    Defense = 1,
    Dodge = 2,
    LaunchSkill = 3,
}
