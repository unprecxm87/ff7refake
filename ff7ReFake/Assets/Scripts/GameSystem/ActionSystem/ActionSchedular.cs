﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActionSchedular : MonoBehaviour
{
    public IAction CurrentAction { get; private set; }
    public UnityEvent onCancelAction;

    public void SetCurrentAction(IAction newAction)
    {
        if (CurrentAction == null)
        {
            CurrentAction = newAction;
            return;
        }
        if (newAction.Priority < CurrentAction.Priority) newAction.CancelAction();
        else CurrentAction = newAction;
    }

    //public bool BlockBy(IAction newAction, params IAction[] blockActions)
    //{
    //    if (blockActions != null)
    //    {
    //        foreach (IAction blockAction in blockActions)
    //        {
    //            if (CurrentAction == blockAction)
    //            {
    //                newAction.CancelAction();
    //                return true;
    //            }
    //        }
    //    }
    //    CurrentAction = newAction;
    //    return false;
    //}

    public void SetCurrentActionAsNull()
    {
        CurrentAction = null;
        onCancelAction.Invoke();
    }

}
