﻿public interface IAction
{
    int Priority { get; }
    void StartAction();
    void CancelAction();
}
