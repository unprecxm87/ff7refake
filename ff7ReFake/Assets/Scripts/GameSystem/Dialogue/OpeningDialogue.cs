﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using TMPro;

public class OpeningDialogue : MonoBehaviour
{
    private bool isFinishOpening;
    public UnityEvent onFinishFadeIn;
    [SerializeField] private CanvasGroup fadeBlack;
    [SerializeField] private DialogueSystem dialogue;

    private bool isFinishAll;

    private void Awake()
    {
        dialogue.onFinishDialogue.AddListener(() => StartCoroutine(FadeIn()));
        onFinishFadeIn.AddListener(() => StartCoroutine(WaitAndDialogue(1, 1)));
    }

    private IEnumerator FadeIn()
    {
        if (!isFinishAll)
        {
            yield return new WaitForSeconds(1);
            fadeBlack.DOFade(0, 0.1f);
            onFinishFadeIn.Invoke();
            isFinishAll = true;
        }
    }

    private IEnumerator WaitAndDialogue(float seconds , int order)
    {
        if (!isFinishAll)
        {
            yield return new WaitForSeconds(seconds);
            dialogue.ShowDialogue(order);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !isFinishOpening)
        {
            StartCoroutine(WaitAndDialogue(2, 0));
            //dialogue.ShowDialogue(0);
            isFinishOpening = true;
        }
    }
}
