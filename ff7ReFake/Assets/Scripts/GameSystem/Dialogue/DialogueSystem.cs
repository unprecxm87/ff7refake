﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class DialogueSystem : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvas;
    [SerializeField] private Text speakerName;
    [SerializeField] private Text dialogueDisplay;
    [SerializeField] private List<Dialogue> dialogues;
    private Queue<string> sentencesInQueue = new Queue<string>();
    public UnityEvent onFinishDialogue;
    private bool isFinishPhase;

    public void ShowDialogue(int order)
    {
        if (order < 0) return;
        foreach (string sentence in dialogues[order].sentences)
        {
            sentencesInQueue.Enqueue(sentence);
        }
        StartCoroutine(ShowSentence());
    }

    private IEnumerator ShowSentence()
    {
        isFinishPhase = false;
        while (sentencesInQueue.Count != 0)
        {
            string sentence = sentencesInQueue.Dequeue();
            string[] words = sentence.Split(':');
            speakerName.text = words[0];
            dialogueDisplay.text = words[1];
            canvas.DOFade(1, 0.1f);
            yield return new WaitForSeconds(4);
            while (canvas.alpha > 0.001f)
            {
                canvas.DOFade(0, 0.1f);
                yield return 0;
            }
        }
        isFinishPhase = true;
        onFinishDialogue.Invoke();
    }
}

[System.Serializable]
public class Dialogue
{
    [TextArea(1, 3)]
    public List<string> sentences;
}