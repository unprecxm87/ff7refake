﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInput : MonoBehaviour
{
    CinemachineFreeLook cf;

    private void Awake()
    {
        cf = GetComponent<CinemachineFreeLook>();
    }

    void Update()
    {
        cf.m_XAxis.m_InputAxisName = InputModule.AnalogStick("R X Axis");
        cf.m_YAxis.m_InputAxisName = InputModule.AnalogStick("R Y Axis");
        //if (InputModule.JoystickIsConnected())
        //{
        //    cf.m_XAxis.m_InputAxisName = "R X Axis";
        //    cf.m_YAxis.m_InputAxisName = "R Y Axis";
        //}
        //else
        //{
        //    cf.m_XAxis.m_InputAxisName = "Mouse X";
        //    cf.m_YAxis.m_InputAxisName = "Mouse Y";
        //}
    }


}
