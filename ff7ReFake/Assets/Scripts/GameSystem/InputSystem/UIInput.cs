﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIInput : MonoBehaviour
{
    StandaloneInputModule sim;

    private void Awake()
    {
        sim = GetComponent<StandaloneInputModule>();
    }
    void Update()
    {
        sim.horizontalAxis = InputModule.AnalogStick("D-Pad X Axis");
        sim.verticalAxis = InputModule.AnalogStick("D-Pad Y Axis");

        //if (InputModule.JoystickIsConnected())
        //{
        //    sim.horizontalAxis = "D-Pad X Axis";
        //    sim.verticalAxis = "D-Pad Y Axis";
        //}
        //else
        //{
        //    sim.horizontalAxis = "Horizontal";
        //    sim.verticalAxis = "Vertical";
        //}

    }
}
