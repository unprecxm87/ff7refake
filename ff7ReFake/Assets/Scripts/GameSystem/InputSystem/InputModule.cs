﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputModule
{
    private enum Joystick
    {
        R_X_Axis,
        R_Y_Axis,
        L_X_Axis,
        L_Y_Axis,
        D_Pad_X_Axis,
        D_Pad_Y_Axis
    }

    private enum Keyboard
    {
        Mouse_X,
        Mouse_Y,
        Horizontal,
        Vertical,
        Debug_Horizontal,
        Debug_Vertical
    }

    public static bool JoystickIsConnected()
    {
        if (Input.GetJoystickNames().Length > 0)
        {
            for (int i = 0; i < Input.GetJoystickNames().Length; i++)
            {
                if (!string.IsNullOrEmpty(Input.GetJoystickNames()[i])) return true;
            }
        }
        return false;
    }

    public static string AnalogStick(string stickType)
    {
        string joystickName = stickType.Replace(" ", "_").Replace("-", "_");
        int joystickID = (int)Enum.Parse(typeof(Joystick), joystickName);
        Keyboard keyboardObject = (Keyboard)joystickID;
        string keyboardName = keyboardObject.ToString().Replace("_", " ");

        return JoystickIsConnected() ? stickType : keyboardName;
    }
}
