﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable] public class ValueEvent : UnityEvent<float> { }

[System.Serializable] public class ObjectEvent : UnityEvent<GameObject> { }

[System.Serializable] public class CheckEvent : UnityEvent<CheckPoint> { }

public static class GameSystem
{
    public static bool IsCombat = false;

    public static bool IsCommand = false;

    public static int CurrentCharacterOrder { get; private set; }

    public static int ChangeCharacterOrder()
    {
        CurrentCharacterOrder++;
        if (CurrentCharacterOrder == 2) CurrentCharacterOrder = 0;
        return CurrentCharacterOrder;
    }

    public static int GetActionPriority(ActionPriority action)
    {
        //return (int)Enum.Parse(typeof(ActionPriority), action);
        return (int)action;
    }

    public static Vector3 GetHorizontalDirection(Vector3 origin, Vector3 target)
    {
        Vector3 direction = target - origin;
        direction.y = 0;
        return direction;
    }

    public static Vector3 GetDirectionWithCamera(float v, float h)
    {
        Vector3 camForward = Camera.main.transform.forward;
        Vector3 camRight = Camera.main.transform.right;

        camForward.y = 0;
        camRight.y = 0;
        camForward.Normalize();
        camRight.Normalize();

        return camForward * v + camRight * h;
    }

    public static Vector3 VectorOnSelfTransform(Vector3 worldVector, Transform selfTransform)
    {
        Vector3 selfVector = new Vector3();
        selfVector = selfTransform.right * worldVector.x + selfTransform.up * worldVector.y + selfTransform.forward * worldVector.z;
        return selfVector;
    }
}
