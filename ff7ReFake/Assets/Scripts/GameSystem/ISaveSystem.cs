﻿public interface ISaveSystem
{
    void SetSaveListener();

    void SaveCheckPoint();

    void LoadCheckPoint();
}
