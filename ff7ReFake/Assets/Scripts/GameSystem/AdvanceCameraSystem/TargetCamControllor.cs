﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TargetCamControllor : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] Transform lookpoint;
    [SerializeField] private CinemachineFreeLook freeLook;
    private Mover mover;
    private float horizontal;
    private float vertical;


    private void Awake()
    {
        mover = player.GetComponent<Mover>();
    }

    private void OnEnable()
    {
        Vector3 initialPosition = freeLook.transform.position;
        initialPosition.y = 2.43f;
        transform.position = initialPosition;
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxis("L X Axis");
        vertical = Input.GetAxis("L Y Axis");
        ControlTargetCam();
    }

    private void ControlTargetCam()
    {
        //Debug.Log("moveDirection: " + moveDirection);
        
        //Debug.Log("position: " + transform.position);
        transform.Translate(CalculateDirection() * Time.deltaTime * 6.0f, Space.World);
    }

    private Vector3 CalculateDirection()
    {
        Vector3 moveForward = lookpoint.position - transform.position;
        Vector3 moveRight = Quaternion.Euler(0, 90, 0) * moveForward;
        //Debug.Log("moveForward: " + moveForward);
        //Debug.Log("moveRight" + moveRight);
        moveForward.y = 0;
        moveRight.y = 0;
        moveForward.Normalize();
        moveRight.Normalize();

        return moveForward * vertical + moveRight * horizontal / 2;
    }
}
