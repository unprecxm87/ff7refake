﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    //public Vector3 offset;
    public float offsetY;
    [HideInInspector] public List<GameObject> targets;
    [SerializeField] private CinemachineFreeLook freeLook1;
    //[SerializeField] private CinemachineFreeLook freeLook2;
    [SerializeField] private CinemachineVirtualCamera targetCam;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject lookPoint;
    private TargetCamControllor targetCamControllor;
    private TargetSelector targetSelector;
    private CinemachineTransposer transposer;
    private PlayerController playerController;
    private float cameraToPlayer;
    private bool isStartLock;

    private int index;
    private Vector3 screenCenter;
    private void Awake()
    {
        targetSelector = player.GetComponent<TargetSelector>();
        transposer = targetCam.GetCinemachineComponent<CinemachineTransposer>();
        playerController = player.GetComponent<PlayerController>();
        //playerController.onLock.AddListener(() => SetTargetCamPosition());
        targetCamControllor = targetCam.GetComponent<TargetCamControllor>();

    }

    private void SetTargetCamPosition()
    {
        //transposer.m_FollowOffset = Camera.main.transform.position - lookPoint.transform.position;
        //transposer.m_FollowOffset.y = offsetY;
        //cameraToPlayer = (Camera.main.transform.position - player.transform.position).z;
        //isStartLock = true;
        

        //Debug.Log("setTargetCamPosition");
        //transposer.m_FollowOffset = Camera.main.transform.position - player.transform.position;
        //transposer.m_FollowOffset.y = offsetY;
        //cameraToPlayer = (Camera.main.transform.position - player.transform.position).z;
        isStartLock = true;
    }

    void Update()
    {
        SwitchToTargetCam();
        //SetTargetCamFollowPoint();
        //if (Input.GetKeyDown(KeyCode.T))
        //{
        //    //freeLook2.enabled = false;
        //    freeLook1.enabled = false;
        //    targetCam.enabled = true;
        //    int i = index % targets.Count;
        //    targetCam.Follow = targets[i].transform;
        //    targetCam.LookAt = targets[i].transform;
        //    index++;
        //}
        //if (Input.GetKeyDown(KeyCode.U))
        //{
        //    freeLook2.enabled = false;
        //    freeLook1.enabled = true;
        //    targetCam.enabled = false;
        //}
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            targets.Add(other.gameObject);
        }
    }

    private bool SwitchToTargetCam()
    {
        freeLook1.enabled = !targetSelector.IsLockTarget;
        targetCam.enabled = targetSelector.IsLockTarget;
        //targetCamControllor.enabled = targetSelector.isLock;


        //targetCam.LookAt = targetSelector.lockTarget.transform;
        return targetSelector.IsLockTarget;
    }

    private void SetTargetCamFollowPoint()
    {
        transform.position = player.transform.position;

        if (isStartLock)
        {
            float horizontal = Input.GetAxis("L X Axis");
            float vertical = Input.GetAxis("L Y Axis");

            Vector3 camForward = Camera.main.transform.forward;
            Vector3 camRight = Camera.main.transform.right;

            camForward.y = 0;
            camRight.y = 0;
            camForward.Normalize();
            camRight.Normalize();
            Vector3 moveDirection = camForward * vertical + camRight * horizontal;

            Camera.main.transform.Translate(moveDirection * Time.deltaTime * 6.0f);
            //transposer.m_FollowOffset.z;
        }
        //Debug.Log("check");
        //transposer.m_FollowOffset = player.transform.GetChild(5).position - lookPoint.transform.position;
        //transposer.m_FollowOffset = Camera.main.transform.position - lookPoint.transform.position;
        //Debug.Log("followOffset: " + transposer.m_FollowOffset);
    }

    //internal GameObject NearestToCenter()
    //{
    //    if (targets != null)
    //    {
    //        GameObject nearestCenterObject = targets[0];
    //        foreach (GameObject target in targets)
    //        {
    //            Vector3 objectOnScreen = new Vector3(Camera.main.WorldToScreenPoint(target.transform.position).x, Camera.main.WorldToScreenPoint(target.transform.position).y, 0.0f);
    //            if (Vector3.Distance(objectOnScreen, screenCenter) < Vector3.Distance(nearestCenterObject.transform.position, screenCenter)) nearestCenterObject = target;
    //        }
    //        return nearestCenterObject;
    //    }
    //    return null;
    //}

}
