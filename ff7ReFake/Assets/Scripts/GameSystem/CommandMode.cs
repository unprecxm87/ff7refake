﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandMode : MonoBehaviour
{
    [SerializeField] private UIManager uIManager;
    //private TacticalMode tacticalMode;

    private void Awake()
    {
        //tacticalMode = GetComponent<TacticalMode>();
    }

    public bool EnterCommand()
    {
        Debug.Log("ppppp");
        //tacticalMode.EnterTacticalMode(true);
       // uIManager.ShowTacticalMenu(true);
        GameSystem.IsCommand = true;
        Debug.Log("enterCommand");
        return true;
    }

    public void CancelCommand()
    {
        uIManager.AllCanvasGroupReset();
        //if (uIManager.Attack.interactable)
        //{
        Debug.Log("cancel");
        //tacticalMode.EnterTacticalMode(false);
        GameSystem.IsCommand = false;
            
        Debug.Log("isCommand: " + GameSystem.IsCommand);
        //}
            
    }
}
