﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPool : MonoBehaviour, ISaveSystem
{
    [SerializeField] GameObject soldier;
    [SerializeField] int soldierAmount;
    [SerializeField] GameObject captain;
    [SerializeField] int captainAmount;
    [SerializeField] GameObject elite;
    [SerializeField] int eliteAmount;
    [SerializeField] GameObject boss;
    [SerializeField] int bossAmount;
    [SerializeField] State playerState;
    private SceneController sceneManager;
    private Dictionary<EnemyType, List<GameObject>> enemyPool;
    private List<GameObject> soldiers = new List<GameObject>();
    private List<GameObject> captains = new List<GameObject>();
    private List<GameObject> elites = new List<GameObject>();
    private List<GameObject> bosses = new List<GameObject>();
    public ObjectEvent onSetEnemy;

    private void Awake()
    {
        sceneManager = GetComponent<SceneController>();
        SetSaveListener();
        InitiateEnemy(soldiers, soldier, soldierAmount, SetSoldierSetting);
        InitiateEnemy(captains, captain, captainAmount, SetCaptainSetting);
        InitiateEnemy(elites, elite, eliteAmount, SetEliteSetting);
        InitiateEnemy(bosses, boss, bossAmount, SetBossSetting);
        enemyPool = new Dictionary<EnemyType, List<GameObject>>()
        {
            { EnemyType.Soldier, soldiers },
            { EnemyType.Captain, captains },
            { EnemyType.Elite, elites },
            { EnemyType.Boss, bosses}
        };
    }

    private void InitiateEnemy(List<GameObject> enemies, GameObject enemy, int amount, Action<GameObject, int> action)
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject enemyInstance = Instantiate(enemy, Vector3.zero, Quaternion.identity);
            enemyInstance.SetActive(false);
            enemies.Add(enemyInstance);
            //if (enemies == soldiers) SetEnemySetting(enemyInstance);
            //if (enemies == captains) SetEnemySetting(enemyInstance);
            //if (enemies == elites) SetEliteSetting(enemyInstance);
            //if (enemies == bosses) SetEliteSetting(enemyInstance);
            action(enemyInstance, i + 1);
        }

    }

    private void SetSoldierSetting(GameObject enemyInstance, int order)
    {
        enemyInstance.name = "幻影士兵" + order;
        EnemyDataBase enemyDataBase = enemyInstance.GetComponent<EnemyDataBase>();
        enemyDataBase.TargetNowCurrentState = GameObject.Find("Kenny").GetComponent<State>();
        enemyDataBase.Ft = GameObject.Find("Kenny").GetComponent<Fighter>();
        enemyDataBase.onFinishDisappear.AddListener((obj) => RetrieveEnemy(obj));
        enemyDataBase.onGainEXP.AddListener((value) => playerState.ModifyEXP(Convert.ToInt32(value)));
        Enemyhealthbar enemyhealthbar = enemyInstance.transform.GetChild(1).gameObject.GetComponent<Enemyhealthbar>();
        enemyhealthbar.MainCam = Camera.main.transform;
        GameObject gameObject = Resources.Load("enemyhealthbar") as GameObject;
        enemyhealthbar.HealthBar = gameObject.GetComponent<Canvas>();
    }

    private void SetCaptainSetting(GameObject enemyInstance, int order)
    {
        enemyInstance.name = "幻影隊長";
        EnemyDataBase enemyDataBase = enemyInstance.GetComponent<EnemyDataBase>();
        enemyDataBase.TargetNowCurrentState = GameObject.Find("Kenny").GetComponent<State>();
        enemyDataBase.Ft = GameObject.Find("Kenny").GetComponent<Fighter>();
        enemyDataBase.onFinishDisappear.AddListener((obj) => RetrieveEnemy(obj));
        enemyDataBase.onGainEXP.AddListener((value) => playerState.ModifyEXP(Convert.ToInt32(value)));
        Enemyhealthbar enemyhealthbar = enemyInstance.transform.GetChild(1).gameObject.GetComponent<Enemyhealthbar>();
        enemyhealthbar.MainCam = Camera.main.transform;
        GameObject gameObject = Resources.Load("enemyhealthbar") as GameObject;
        enemyhealthbar.HealthBar = gameObject.GetComponent<Canvas>();
    }

    private void SetEliteSetting(GameObject enemyInstance, int order)
    {
        enemyInstance.name = "幻影堡壘";
        EnemyDataBase enemyDataBase = enemyInstance.GetComponent<EnemyDataBase>();
        enemyDataBase.TargetNowCurrentState = GameObject.Find("Kenny").GetComponent<State>();
        enemyDataBase.Ft = GameObject.Find("Kenny").GetComponent<Fighter>();
        enemyDataBase.mData.TargetObject = GameObject.Find("Kenny");
        enemyDataBase.onFinishDisappear.AddListener((obj) => RetrieveEnemy(obj));
        enemyDataBase.onGainEXP.AddListener((value) => playerState.ModifyEXP(Convert.ToInt32(value)));
        //Enemyhealthbar enemyhealthbar = enemyInstance.transform.GetChild(1).gameObject.GetComponent<Enemyhealthbar>();
        //enemyhealthbar.MainCam = Camera.main.transform;
        //GameObject gameObject = Resources.Load("enemyhealthbar") as GameObject;
        //enemyhealthbar.HealthBar = gameObject.GetComponent<Canvas>();
        MasterFSM masterFSM = enemyInstance.GetComponent<MasterFSM>();
        masterFSM.father_ChangePoints = GameObject.Find("Changepionts");
        masterFSM.PS3 = GameObject.Find("Changepionts").transform.GetChild(12).gameObject.GetComponent<ParticleSystem>();
    }

    private void SetBossSetting(GameObject enemyInstance, int order)
    {
        enemyInstance.name = "衝鋒鋼球王";
        EnemyDataBase enemyDataBase = enemyInstance.GetComponent<EnemyDataBase>();
        enemyDataBase.TargetNowCurrentState = GameObject.Find("Kenny").GetComponent<State>();
        enemyDataBase.Ft = GameObject.Find("Kenny").GetComponent<Fighter>();
        enemyDataBase.mData.TargetObject = GameObject.Find("Kenny");
        enemyDataBase.onFinishDisappear.AddListener((obj) => RetrieveEnemy(obj));
        enemyDataBase.onGainEXP.AddListener((value) => playerState.ModifyEXP(Convert.ToInt32(value)));
        //Enemyhealthbar enemyhealthbar = enemyInstance.transform.GetChild(1).gameObject.GetComponent<Enemyhealthbar>();
        //enemyhealthbar.MainCam = Camera.main.transform;
        //GameObject gameObject = Resources.Load("enemyhealthbar") as GameObject;
        //enemyhealthbar.HealthBar = gameObject.GetComponent<Canvas>();
        BossFSM bossFSM = enemyInstance.GetComponent<BossFSM>();
        bossFSM.tmp_Target = GameObject.Find("tmp_target");

    }

    private void RetrieveEnemy(GameObject enemy)
    {
        enemy.SetActive(false);
    }

    public GameObject GenerateEnemy(EnemyType enemyType, Vector3 position)
    {
        GameObject enemyInstance = null;
        foreach (GameObject enemy in enemyPool[enemyType])
        {
            if (enemy.activeSelf == false)
            {
                enemyInstance = enemy;
                break;
            }
        }
        //Debug.Log(enemyInstance.name);
        enemyInstance.transform.position = position;
        enemyInstance.SetActive(true);
        return enemyInstance;
    }

    private void ResetObject(List<GameObject> list)
    {
        foreach (GameObject gameObject in list) gameObject.SetActive(false);
    }

    public void SaveCheckPoint()
    {

    }

    public void LoadCheckPoint()
    {
        ResetObject(soldiers);
        ResetObject(captains);
        ResetObject(elites);
        ResetObject(bosses);
    }

    public void SetSaveListener()
    {
        sceneManager.onArriveCheckPoint.AddListener(() => SaveCheckPoint());
        sceneManager.onLoad.AddListener(() => LoadCheckPoint());
    }
}

public enum EnemyType
{
    Soldier,
    Captain,
    Elite,
    Boss
}