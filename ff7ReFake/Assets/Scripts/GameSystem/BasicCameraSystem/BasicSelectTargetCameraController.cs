﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BasicSelectTargetCameraController : MonoBehaviour
{
    [SerializeField] private BasicCameraManager cameraManager;
    public UnityEvent onSwitchTo;
    public UnityEvent onSwitchBack;

    private void OnEnable()
    {
        onSwitchTo.Invoke();
        transform.position = cameraManager.lastPosition;
    }

    private void OnDisable()
    {
        onSwitchBack.Invoke();
    }

}
