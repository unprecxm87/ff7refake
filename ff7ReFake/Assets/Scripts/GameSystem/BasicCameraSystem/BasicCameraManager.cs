﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicCameraManager : MonoBehaviour
{
    [SerializeField] private GameObject lockCam;
    [SerializeField] private GameObject selectCam;

    [SerializeField] private GameObject characterCam;

    public State currentCharacterState;
    private CinemachineFreeLook characterFreeLook;

    //[SerializeField] private List<GameObject> characterCams = new List<GameObject>();
    //private List<CinemachineFreeLook> characterFreeLooks = new List<CinemachineFreeLook>();
    private GameObject currentCharacterCam;

    [SerializeField] private CharacterSelector characterSelector;
    private CinemachineVirtualCamera lockVirtualCam;
    private CinemachineVirtualCamera selectVirtualCam;
    private BasicLockTargetCameraController lockCamController;
    //private BasicSelectTargetCameraController selectCamController;
    private BasicLockTargetCameraController selectCamController;
    private BasicFreeLookCameraController characterCamController;
    private TargetSelector targetSelector;
    private Vector3 cameraForward;
    private ActionSchedular playerAction;

    public Vector3 lastPosition { get; set; }

    public bool StopMoving { get; private set; }

    public float syncDegree { get; private set; }

    private void Awake()
    {
        lockVirtualCam = lockCam.GetComponent<CinemachineVirtualCamera>();
        selectVirtualCam = selectCam.GetComponent<CinemachineVirtualCamera>();

        characterFreeLook = characterCam.GetComponent<CinemachineFreeLook>();
        characterCamController = characterCam.GetComponent<BasicFreeLookCameraController>();
        //for (int i = 0; i < 2; i++)
        //{
        //    characterFreeLooks.Add(characterCams[i].GetComponent<CinemachineFreeLook>());
        //}
        //currentCharacterCam = characterCams[0];
        lockCamController = lockCam.GetComponent<BasicLockTargetCameraController>();
        lockCamController.onSwitchTo.AddListener(() => cameraForward = transform.forward);
        lockCamController.onSwitchBack.AddListener(() => SyncCameraForward());
        selectCamController = selectCam.GetComponent<BasicLockTargetCameraController>();
        selectCamController.onSwitchTo.AddListener(() => cameraForward = transform.forward);
        selectCamController.onSwitchBack.AddListener(() => SyncCameraForward());
        //characterSelector.onChangeCharacter.AddListener(() => SetNewCharacterInfo());
        
    }

    private void Start()
    {
        targetSelector = characterSelector.CurrentCharacter.GetComponent<TargetSelector>();
        playerAction = characterSelector.CurrentCharacter.GetComponent<ActionSchedular>();
    }

    private void SetNewCharacterInfo()
    {
        targetSelector = characterSelector.CurrentCharacter.GetComponent<TargetSelector>();
        playerAction = characterSelector.CurrentCharacter.GetComponent<ActionSchedular>();
        currentCharacterState = characterSelector.CurrentCharacter.GetComponent<State>();
        characterFreeLook.Follow = null;
        characterFreeLook.LookAt = null;
        characterCamController.NewCharacterPosition = transform.position - characterSelector.NonCurrentCharacter.transform.position + characterSelector.CurrentCharacter.transform.position;
        characterFreeLook.enabled = false;
        Time.timeScale = 0.01f;

        //characterFreeLook.Follow = characterSelector.CurrentCharacter.transform;
        //characterFreeLook.LookAt = characterSelector.CurrentCharacter.transform;
        lockVirtualCam.Follow = characterSelector.CurrentCharacter.transform;
        selectVirtualCam.Follow = characterSelector.CurrentCharacter.transform;
        //for (int i = 0; i < 2; i++)
        //{
        //    if (characterFreeLooks[i].LookAt == characterSelector.CurrentCharacter.transform)
        //    {
        //        currentCharacterCam = characterCams[i];
        //        currentCharacterCam.SetActive(true);
        //    }
        //    else characterCams[i].SetActive(false);
        //}
    }

    void Update()
    {
        selectCam.SetActive(!targetSelector.IsLockTarget && targetSelector.IsSelectTarget);
        lockCam.SetActive(targetSelector.IsLockTarget && !targetSelector.IsSelectTarget);
        characterCam.SetActive(!targetSelector.IsLockTarget && !targetSelector.IsSelectTarget);
        
        if (lockCam.activeSelf) lockVirtualCam.LookAt = targetSelector.LockTarget.transform;
        if (selectCam.activeSelf) selectVirtualCam.LookAt = targetSelector.SelectTarget.transform;

        TransitFreeLookCam();

    }

    private void TransitFreeLookCam()
    {
        transform.position = Vector3.MoveTowards(transform.position, characterCamController.NewCharacterPosition, 0.3f);
        if (Vector3.Distance(transform.position, lastPosition) < 0.001f && characterFreeLook.LookAt == null && characterFreeLook.Follow == null)
        {
            characterFreeLook.LookAt = characterSelector.CurrentCharacter.transform;
            characterFreeLook.Follow = characterSelector.CurrentCharacter.transform;
            characterFreeLook.enabled = true;
            Time.timeScale = 1;
        }
    }

    private void LateUpdate()
    {
        lastPosition = transform.position;
    }

    private void SyncCameraForward()
    {
        float dot = Vector3.Dot(cameraForward, transform.forward);
        syncDegree = Mathf.Rad2Deg * Mathf.Acos(dot);
        if (Vector3.Cross(cameraForward, transform.forward).y < 0) syncDegree = -syncDegree;
    }
}
