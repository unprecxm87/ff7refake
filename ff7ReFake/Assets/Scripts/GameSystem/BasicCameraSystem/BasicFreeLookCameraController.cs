﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class BasicFreeLookCameraController : MonoBehaviour, ISaveSystem
{
    [SerializeField] private BasicCameraManager cameraManager;
    [SerializeField] private CinemachineFreeLook freeLook;
    [SerializeField] private CharacterSelector characterSelector;
    [SerializeField] private SceneController sceneController;
    private CinemachineFreeLook cinemachineFree;
    private FreeLookData freeLookData;

    public Vector3 NewCharacterPosition { get; set; }

    private void Awake()
    {
        SetSaveListener();
        cinemachineFree = GetComponent<CinemachineFreeLook>();
    }

    private void Start()
    {
        NewCharacterPosition = transform.position;
    }
    
    private void OnEnable()
    {
        freeLook.m_Heading.m_Bias += cameraManager.syncDegree;
    }

    private void Update()
    {
        //transform.position = Vector3.Lerp(transform.position, NewCharacterPosition, 0.1f);
        //if (Vector3.Distance(transform.position, lastPosition) < 0.01f && cinemachineFree.LookAt == null && cinemachineFree.Follow == null)
        //{
        //    cinemachineFree.LookAt = characterSelector.CurrentCharacter.transform;
        //    cinemachineFree.Follow = characterSelector.CurrentCharacter.transform;
        //    cinemachineFree.enabled = true;
        //}
    }

    public void SetSaveListener()
    {
        sceneController.onArriveCheckPoint.AddListener(() => SaveCheckPoint());
        sceneController.onLoad.AddListener(() => LoadCheckPoint());
    }

    public void SaveCheckPoint()
    {
        freeLookData = new FreeLookData(cinemachineFree);
    }

    public void LoadCheckPoint()
    {
        cinemachineFree.m_XAxis.Value = freeLookData.XAxis;
        cinemachineFree.m_YAxis.Value = freeLookData.YAxis;
    }

    private class FreeLookData
    {
        public float XAxis;
        public float YAxis;
        
        public  FreeLookData(CinemachineFreeLook freeLook)
        {
            XAxis = freeLook.m_XAxis.Value;
            YAxis = freeLook.m_YAxis.Value;
        }
    }
}
