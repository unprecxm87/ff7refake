﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BasicLockTargetCameraController : MonoBehaviour
{
    [SerializeField] private Vector3 offset;
    [SerializeField] private float damp;
    [SerializeField] private BasicCameraManager cameraManager;
    [SerializeField] private Vector3 raycastOffset;
    public UnityEvent onSwitchTo;
    public UnityEvent onSwitchBack;
    private CinemachineVirtualCamera vCam;
    private CinemachineTransposer transposer;
    private RaycastHit rh;
    private Vector3 velocity = Vector3.zero;
    private float distance;
    private Vector3 offset2;




    private void OnEnable()
    {
        offset2 = new Vector3(transposer.m_FollowOffset.x, 0, transposer.m_FollowOffset.z);
        distance = offset.z;
        onSwitchTo.Invoke();
        transposer.m_FollowOffset = SetOffsetCollider();
    }

    private void OnDisable()
    {
        onSwitchBack.Invoke();
    }

    private void Awake()
    {
        vCam = GetComponent<CinemachineVirtualCamera>();
        transposer= vCam.GetCinemachineComponent<CinemachineTransposer>();
    }

    void Update()
    {
        transposer.m_FollowOffset = Vector3.SmoothDamp(transposer.m_FollowOffset, SetOffsetCollider(), ref velocity, damp);

        //offset2 = RotateTo(offset2, new Vector3(SetOffsetCollider().x, 0, SetOffsetCollider().z), 0.1f);
        //transposer.m_FollowOffset.x = offset2.x;
        //transposer.m_FollowOffset.z = offset2.z;

        //transposer.m_FollowOffset.x += 0.1f * (SetOffsetCollider().x - transposer.m_FollowOffset.x);
        //transposer.m_FollowOffset.z += 0.1f * (SetOffsetCollider().z - transposer.m_FollowOffset.z);
    }

    //private Vector3 RotateTo(Vector3 current, Vector3 target, float speed)
    //{
    //    float rotateDegree = Mathf.Acos(Vector3.Dot(current.normalized, target.normalized)) * Mathf.Rad2Deg;
    //    Vector3 crossResult = Vector3.Cross(current.normalized, target.normalized);
    //    int inverse = crossResult.y > 0 ? 1 : -1;
    //    return Quaternion.Euler(0, inverse * rotateDegree * speed, 0) * current;
    //}

    private void LateUpdate()
    {
        transform.position = cameraManager.lastPosition;
    }

    private Vector3 SetOffsetCollider()
    {
        if (Physics.Linecast(vCam.Follow.position + raycastOffset, vCam.Follow.position + raycastOffset + GetNewOffset(offset.z), out rh, 1))
        {
            return GetNewOffset(rh.distance);
        }

        return GetNewOffset(offset.z);

        //if (Physics.Linecast(vCam.Follow.position + raycastOffset, vCam.Follow.position + raycastOffset + GetNewOffset(offset.z), out rh, 1))
        //{
        //    distance = distance + 0.1f * (rh.distance - distance);
        //    Debug.Log("GetNewOffset(distance).x: " + GetNewOffset(distance).x);
        //    Debug.Log("GetNewOffset(distance).z: " + GetNewOffset(distance).z);
        //    return GetNewOffset(distance);
        //}
        //distance = distance + 0.1f * (offset.z - distance);
        //return GetNewOffset(distance);
    }

    private Vector3 GetNewOffset(float distance)
    {
        return new Vector3((GetOffsetDirection() * distance).x, offset.y, (GetOffsetDirection() * distance).z);
    }

    private Vector3 GetOffsetDirection()
    {
        Vector3 direction = vCam.Follow.position - vCam.LookAt.position;
        if (vCam.Follow.position == vCam.LookAt.position) direction = -vCam.Follow.forward;
        direction.y = 0;
        direction.Normalize();
        return direction;
    }
}
