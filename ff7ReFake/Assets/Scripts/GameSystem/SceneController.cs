﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SceneController : MonoBehaviour
{
    public CheckPoint currentCheckPoint = CheckPoint.Open;
    public UnityEvent onArriveCheckPoint;
    public UnityEvent onLoad;
}
