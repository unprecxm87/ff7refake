﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.VFX;

[System.Serializable] public class CollisionEvent : UnityEvent<GameObject> {}

public class HitPlayerTrigger : MonoBehaviour
{
    [SerializeField] private VisualEffect spark;
    [SerializeField] private Fighter ownerFighter;
    public CollisionEvent onHit;
    private Collider weapon;


    private void Awake()
    {
        weapon = GetComponent<Collider>();
        //weapon.enabled = false;
    }


    private void OnTriggerEnter(Collider other)
    {
       
        if (other.CompareTag("Player"))
        {

            State otherState = other.GetComponent<State>();
            Fighter otherFight = other.GetComponent<Fighter>();
            if (!otherState.IsDead)
            {
                otherFight.BeHit(10);
            }
        }
    }
}
