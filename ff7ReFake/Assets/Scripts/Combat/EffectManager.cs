﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class EffectManager : MonoBehaviour
{

    [SerializeField] private ParticleSystem heavySlash;
    [SerializeField] private Vector3 heavySlashRotation;
    [SerializeField] private Vector3 heavySlashPositionOffset;
    [SerializeField] private ParticleSystem combo0Slash;
    [SerializeField] private Vector3 combo0SlashRotation;
    [SerializeField] private Vector3 combo0SlashPositionOffset;
    [SerializeField] private ParticleSystem combo1Slash;
    [SerializeField] private Vector3 combo1SlashRotation;
    [SerializeField] private Vector3 combo1SlashPositionOffset;
    [SerializeField] private ParticleSystem combo2Slash;
    [SerializeField] private Vector3 combo2SlashRotation;
    [SerializeField] private Vector3 combo2SlashPositionOffset;
    [SerializeField] private Vector3 combo2SparkRotation;
    [SerializeField] private Vector3 combo2SparkPositionOffset;

    [SerializeField] private VisualEffect abilitySpark;
    [SerializeField] private ParticleSystem combo0Spike;
    [SerializeField] private Transform sword;
    [SerializeField] private DialogueSystem dialogueSystem;
    private Fighter fighter;

    private void Awake()
    {
        fighter = GetComponent<Fighter>();
    }
    public void TriggerHeavyHitEffect()
    {
        ParticleSystem particleSystemInstance = Instantiate(heavySlash, sword.position + GameSystem.VectorOnSelfTransform(heavySlashPositionOffset, sword), Quaternion.LookRotation(sword.forward));
        particleSystemInstance.transform.Rotate(heavySlashRotation, Space.Self);
        Destroy(particleSystemInstance.gameObject, 0.5f);
    }

    public void TriggerCombo2Slash()
    {
        if (!StorySystem.Progress[CheckPoint.ThirdEasy])
        {
            dialogueSystem.ShowDialogue(17);
            return;
        }
        ParticleSystem particleSystemInstance = Instantiate(combo2Slash, sword.position + GameSystem.VectorOnSelfTransform(combo2SlashPositionOffset, sword), Quaternion.LookRotation(sword.forward));
        particleSystemInstance.transform.Rotate(combo2SlashRotation, Space.Self);
        VisualEffect visualEffectInstance = Instantiate(abilitySpark, sword.position + GameSystem.VectorOnSelfTransform(combo2SparkPositionOffset, sword), Quaternion.LookRotation(sword.forward));
        visualEffectInstance.transform.Rotate(combo2SparkRotation, Space.Self);
        visualEffectInstance.Play();
        Destroy(particleSystemInstance.gameObject, 0.5f);
        Destroy(visualEffectInstance.gameObject, 1);
        StartCoroutine(fighter.SetWeaponRange(0.06f));
    }

    public void TriggerCombo0Spike(int state)
    {
        if (!StorySystem.Progress[CheckPoint.FirstEasy])
        {
            dialogueSystem.ShowDialogue(17);
            return;
        }
        if (state == 1) combo0Spike.Play();
        else if (state == 0) combo0Spike.Stop();
        else if (state == 2)
        {
            ParticleSystem particleSystemInstance = Instantiate(combo0Slash, sword.position + GameSystem.VectorOnSelfTransform(combo2SlashPositionOffset, sword), Quaternion.LookRotation(sword.forward));
            particleSystemInstance.transform.Rotate(combo0SlashRotation, Space.Self);
            Destroy(particleSystemInstance.gameObject, 0.5f);
        }
    }

    public void TriggerCombo1Slash(int order)
    {
        if (!StorySystem.Progress[CheckPoint.SecondEasy])
        {
            dialogueSystem.ShowDialogue(17);
            return;
        }
        if (order <= 1)
        {
            ParticleSystem particleSystemInstance = Instantiate(combo1Slash, transform.position + GameSystem.VectorOnSelfTransform(combo1SlashPositionOffset, transform), Quaternion.LookRotation(-transform.right));
            particleSystemInstance.transform.Rotate(combo1SlashRotation, Space.Self);
            ParticleSystem particleSystemInstanceChild = particleSystemInstance.transform.GetChild(order).GetComponent<ParticleSystem>();
            particleSystemInstanceChild.Play();
            Destroy(particleSystemInstance.gameObject, 2f);
        }
        else
        {
            ParticleSystem heavySlashInstance = Instantiate(heavySlash, sword.position + GameSystem.VectorOnSelfTransform(heavySlashPositionOffset, sword), Quaternion.LookRotation(sword.forward));
            heavySlashInstance.transform.Rotate(heavySlashRotation, Space.Self);
            Destroy(heavySlashInstance.gameObject, 0.5f);
            StartCoroutine(fighter.SetWeaponRange(0.06f));
        }
       

    }
}
