﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Fighter : MonoBehaviour, IAction
{
    [SerializeField] private HitEnemyTrigger hitTrigger;
    [SerializeField] private float pressInterval;
    private State state;
    
    private Animator animator;
    private ActionSchedular actionSchedular;
    private BoxCollider hitTriggerCollider;
    private TargetSelector targetSelector;
    

    private bool isTransition;
    private object isEnd;


    public bool hitByMagic;
    public float HitStopDuration { get; private set; }
    public string HitType { get; private set; }

    public int Priority { get; private set; }

    private void Awake()
    {
        Priority = GameSystem.GetActionPriority(ActionPriority.Fight);
        state = GetComponent<State>();
        
        animator = GetComponent<Animator>();
        actionSchedular = GetComponent<ActionSchedular>();
        //hitTrigger.onHit.AddListener((target) => HitTarget(target));
        hitTriggerCollider = hitTrigger.GetComponent<BoxCollider>();
        targetSelector = GetComponent<TargetSelector>();
        actionSchedular.onCancelAction.AddListener(() => CancelAction());
    }

    public void StartAction()
    {
        actionSchedular.SetCurrentAction(this);
        if (targetSelector.IsLockTarget) transform.forward = GameSystem.GetHorizontalDirection(transform.position, targetSelector.LockTarget.transform.position);
        animator.SetBool("Attack", true);
        //if (!animator.GetBool("Attack")) StartCoroutine(SetFightAnimation());
        //StartCoroutine(SetFightAnimation());
    }

    private IEnumerator SetFightAnimation()
    {
        animator.SetBool("Attack", true);
        yield return new WaitForSeconds(pressInterval);
        //if (isTransition) StartCoroutine(SetFightAnimation());
        //else CancelAction();
        CancelAction();
    }

    //private bool HitTarget(GameObject target)
    //{
    //    Debug.Log(gameObject.name + " hit " + target.gameObject.name);
    //    Fighter targetFighter = target.GetComponent<Fighter>();

    //    if (target != null)
    //    {
    //        switch (HitType)
    //        {
    //            case "lightHit":
    //                HitStopDuration = 0;
    //                //targetFighter.BeHit(state.lightHit);
    //                state.ModifyATB(10);
    //                state.ModifyEXP(10);
    //                break;
    //            case "heavyHit":
    //                HitStopDuration = 0.1f;
    //                //targetFighter.BeHit(state.heavyHit);
    //                state.ModifyATB(10);
    //                state.ModifyEXP(10);
    //                break;
    //            case "abilityHit":
    //                HitStopDuration = 0.2f;
    //                //targetFighter.BeHit(state.abilityHit);
    //                state.ModifyATB(10);
    //                state.ModifyEXP(10);
    //                break;
    //        }
    //        return true;
    //    }
    //    return false;
    //}

    public float GetHitValueAndReward()
    {
        if (hitByMagic)
        {
            hitByMagic = false;
            return 50;
        } 
        //state.ModifyEXP(10);
        state.ModifyATB(5);
        return state.HitValue[state.currentHitType];
    }

    public bool BeHit(float damage)
    {
        //Debug.Log(gameObject.name + "isHit: " + damage);
        if (state.IsDodge) return false;
        if (state.IsDefense) state.ModifyHP(damage * 0.1f, 0);
        else state.ModifyHP(damage, 0);
        return true;
    }


    public void SetWeaponCollider(int state)
    {
        if (state == 0) hitTriggerCollider.enabled = false;
        else hitTriggerCollider.enabled = true;
    }

    public IEnumerator SetWeaponRange(float range)
    {
        hitTriggerCollider.size = new Vector3(0.00293f, range, range);
        yield return new WaitForSeconds(0.2f);
        hitTriggerCollider.size = new Vector3(0.00293f, 0.01600857f, 0.000390001f);
    }

    public void SetHitType(string Type)
    {
        HitType = Type;
    }

    public void CancelAction()
    {
        animator.SetBool("Attack", false);
    }
}
