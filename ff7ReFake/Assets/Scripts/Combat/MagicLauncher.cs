﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class MagicLauncher : MonoBehaviour, IAction
//{
//    [SerializeField] private List<Magic> magicSlots = new List<Magic>();
//    [SerializeField] private Transform hand;
//    [SerializeField] private TargetDetector targetDetector;
//    [SerializeField] private FriendsContainer friendsContainer;
//    private ActionSchedular actionSchedular;
//    private TargetSelector targetSelector;
 
//    private Animator animator;
//    private State state;

//    public bool IsLaunchMagic { get; set; }

//    public int Priority { get; private set; }

//    public int MagicID { get; set; }

//    public GameObject LaunchTarget { get; set; }

//    private void Awake()
//    {
//        Priority = GameSystem.GetActionPriority("LaunchMagic");
//        actionSchedular = GetComponent<ActionSchedular>();
//        targetSelector = GetComponent<TargetSelector>();
//        animator = GetComponent<Animator>();
//        state = GetComponent<State>();
//    }

//    public void StartAction()
//    {
//        actionSchedular.SetCurrentAction(this);
//        //LaunchTarget = targetSelector.SearchTarget(180);
//        //direction = GameSystem.GetHorizontalDirection(transform.position, LaunchTarget.transform.position);
//        if (LaunchTarget != null)
//        {
//            animator.SetTrigger("LaunchMagic");
//            magicSlots[MagicID].GenerateMagic(LaunchTarget.transform, hand);
//            state.ModifyMP(magicSlots[MagicID].cost, 0);
//            state.ModifyATB(10);
//            state.ModifyEXP(10);
//            actionSchedular.SetCurrentActionAsNull();
//        }
//        actionSchedular.SetCurrentActionAsNull();
//    }

//    internal void LaunchMagic(bool launch)
//    {
//        IsLaunchMagic = launch;
//        switch (magicSlots[MagicID].targets.ToString())
//        {
//            case "friends":
//                targetSelector.currentTargetList = friendsContainer.we;
//                break;
//            case "enemies":
//                targetSelector.currentTargetList = targetDetector.Targets;
//                break;
//        }
//    }

//    public void CancelAction()
//    {

//    }
//}
