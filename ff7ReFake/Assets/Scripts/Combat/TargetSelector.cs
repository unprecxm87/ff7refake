﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
//using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;

public class TargetSelector : MonoBehaviour, ISaveSystem
{
    [HideInInspector] public List<GameObject> CurrentTargetList { get; set; }
    [SerializeField] private SceneController sceneController;
    
    private bool canChooseTarget = false;
    private bool selectWhileLock = false;
    private EnemyDataBase lockDataBase;
    private EnemyDataBase selectDataBase;
    private int order;

    public GameObject LockTarget { get; private set; }

    public GameObject SelectTarget { get; private set; }

    public bool IsLockTarget { get; private set; }

    public bool IsSelectTarget { get; private set; }

    public List<GameObject> EnemyList { get; set; }

    private void Awake()
    {
        SetSaveListener();
        CurrentTargetList = new List<GameObject>();
    }

    private void Update()
    {
        if (IsLockTarget && CurrentTargetList.Count != 0)
        {
            LockTarget = ChooseTarget(Input.GetAxis("R X Axis"), LockTarget.transform, CurrentTargetList, TargetType.Lock);
            if (lockDataBase.mData.b_IsDead) Lock();
            //if (lockEnemyFSM01 != null && lockEnemyFSM01.mData.b_IsDead) Lock();
            //else if (lockEnemyFSM02 != null && lockEnemyFSM02.mData.b_IsDead) Lock();
            //else if (lockEnemyFSM03 != null && lockEnemyFSM03.mData.b_IsDead) Lock();
        }
        else if (CurrentTargetList.Count == 0) Unlock();

        if (IsSelectTarget && CurrentTargetList.Count != 0)
        {
            SelectTarget = ChooseTarget(Input.GetAxis("R X Axis"), SelectTarget.transform, CurrentTargetList, TargetType.Select);
            if (SelectTarget != gameObject)
            {
                if (selectDataBase.mData.b_IsDead) Select();
            }
            //if (selectEnemyFSM01 != null && selectEnemyFSM01.mData.b_IsDead) Select();
            //else if (selectEnemyFSM02 != null && selectEnemyFSM02.mData.b_IsDead) Select();
            //else if (selectEnemyFSM03 != null && selectEnemyFSM03.mData.b_IsDead) Select();
        }
        else if (CurrentTargetList.Count == 0) UnSelect();

        //Debug.Log("LockTarget: " + LockTarget);
        //Debug.Log("SelectTarget: " + SelectTarget);
        //Debug.Log("IsLockTarget: " + IsLockTarget);
        //Debug.Log("IsSelectTarget: " + IsSelectTarget);
        //Debug.Log("currentTargetList: " + currentTargetList);
        if (Input.GetKeyDown(KeyCode.K))
        {
            KillAllEnemies();
        }
    }

    private void KillAllEnemies()
    {
        foreach (GameObject enemy in EnemyList)
        {
            EnemyDataBase enemyDataBase = enemy.GetComponent<EnemyDataBase>();
            enemyDataBase.BeHit();
        }
    }

    public GameObject SearchTarget(float SearchDegree, List<GameObject> list)
    {
        //if (IsLockTarget) return LockTarget;
        float minDistance = float.MaxValue;
        GameObject closestObject = null;
        //if (list.Contains(gameObject)) return gameObject;

        foreach (GameObject target in list)
        {
            if (!list.Contains(gameObject))
            {
                EnemyFSM01 state01 = target.GetComponent<EnemyFSM01>();
                //EnemyFSM02 state02 = target.GetComponent<EnemyFSM01>();
                //EnemyFSM03 state03 = target.GetComponent<EnemyFSM01>();
                //if (GameSystem.FindObjectType(target).mData.b_IsDead) continue;
                if (state01 != null && state01.mData.b_IsDead) continue;
                //else if (state02 != null && state02.mData.b_IsDead) continue;
                //else if (state03 != null && state03.mData.b_IsDead) continue;

            }
            Vector3 targetDirection = Vector3.Normalize(target.transform.position - transform.position);
            float dot = Vector3.Dot(transform.forward, targetDirection);
            if ( dot > Mathf.Cos(Mathf.Deg2Rad * SearchDegree))
            {
                //Debug.Log(target.name + " dot: " + dot);
                //Debug.Log(target.name + " distance: " + Vector3.Distance(transform.position, target.transform.position));
                if (Vector3.Distance(transform.position, target.transform.position) < minDistance)
                {
                    minDistance = Vector3.Distance(transform.position, target.transform.position);
                    closestObject = target;
                }
            }
        }
        return closestObject;
    }

    public GameObject Lock()
    {
        CurrentTargetList = EnemyList;
        if (selectWhileLock) LockTarget = SelectTarget;
        else LockTarget = SearchTarget(180, CurrentTargetList);
        //Debug.Log("LockTarget: " + LockTarget.name);
        //lockTargetState = LockTarget.GetComponent<EnemyFSM01>();  //01
        lockDataBase = LockTarget.GetComponent<EnemyDataBase>();
        //lockEnemyFSM02 = LockTarget.GetComponent<EnemyFSM02>();
        //lockEnemyFSM03 = LockTarget.GetComponent<EnemyFSM03>();
        if (LockTarget == null)
        {
            IsLockTarget = false;
            return null;
        }
        else
        {
            IsLockTarget = true;
            return LockTarget;
        }
    }

    public GameObject Unlock()
    {
        LockTarget = null;
        IsLockTarget = false;
        return LockTarget;
    }

    public void Select()
    {
        
        SelectTarget = SearchTarget(180, CurrentTargetList);
        //selectTargetState = SelectTarget.GetComponent<EnemyFSM01>();  //01
        selectDataBase = SelectTarget.GetComponent<EnemyDataBase>();
        //selectEnemyFSM02 = SelectTarget.GetComponent<EnemyFSM02>();
        //selectEnemyFSM03 = SelectTarget.GetComponent<EnemyFSM03>();
        if (SelectTarget == null)
        {
            IsSelectTarget = false;
            //return null;
        }
        else
        {
            IsSelectTarget = true;
            Debug.Log("select: "+ IsSelectTarget);
            if (IsLockTarget)
            {
                Unlock();
                selectWhileLock = true;
            }

            //return SelectTarget;
        }
    }

    public GameObject UnSelect()
    {
        if (selectWhileLock && !CurrentTargetList.Contains(gameObject)) Lock();
        SelectTarget = null;
        IsSelectTarget = false;
        selectWhileLock = false;
        return SelectTarget;
    }

    //public GameObject FindTarget(ref GameObject target, ref bool state)
    //{
    //    target = SearchTarget(180, currentTargetList);
    //    if (target == null)
    //    {
    //        state = false;
    //        return null;
    //    }
    //    else
    //    {
    //        state = true;
    //        return target;
    //    }
    //}

    private GameObject ChooseTarget(float input, Transform target, List<GameObject> list, TargetType targetType)
    {
        float minDistance = float.MaxValue;
        Vector3 currentTargetToPlayer = transform.position - target.position;
        GameObject closestTarget = null;
        Func<Vector3, bool> checkOrientation = null;

        if (input < 0.01f && input > -0.01f)
        {
            canChooseTarget = true;
            return target.gameObject;
        }

        if (input >= 0.01f && canChooseTarget) checkOrientation = (v) => (v.y < 0 ? true : false);
        else if (input >= 0.01f && !canChooseTarget) return target.gameObject;
        if (input <= -0.01f && canChooseTarget) checkOrientation = (v) => (v.y > 0 ? true : false);
        else if (input <= -0.01f && !canChooseTarget) return target.gameObject;

        if (list.Contains(gameObject))
        {
            if ((input >= 0.01f || input <= -0.01f) && canChooseTarget)
            {
                order++;
                canChooseTarget = false;
                return list[order % list.Count];
            }
        }

        foreach (GameObject newTarget in list)
        {
            Vector3 currentTargetToNewTarget = newTarget.transform.position - target.position;

            if (checkOrientation(Vector3.Cross(currentTargetToPlayer, currentTargetToNewTarget))
                && Vector3.Distance(target.position, newTarget.transform.position) < minDistance)
            {
                minDistance = Vector3.Distance(target.position, newTarget.transform.position);
                closestTarget = newTarget;
            }
        }
        canChooseTarget = false;
        SetTargetState(targetType, closestTarget);
        if (closestTarget != null) return closestTarget;
        //SetTargetState(targetType, target.gameObject);
        return target.gameObject;
    }

    private void SetTargetState(TargetType targetType, GameObject target)
    {
        if (target == null) return;
        if (targetType == TargetType.Lock)
        {
            lockDataBase = target.GetComponent<EnemyDataBase>();
            //lockEnemyFSM02 = target.GetComponent<EnemyFSM02>();
            //lockEnemyFSM03 = target.GetComponent<EnemyFSM03>();

        }
        else if (targetType == TargetType.Select)
        {
            selectDataBase = target.GetComponent<EnemyDataBase>();
            //selectEnemyFSM02 = target.GetComponent<EnemyFSM02>();
            //selectEnemyFSM03 = target.GetComponent<EnemyFSM03>();

        }
    }

    public void SetSaveListener()
    {
        sceneController.onArriveCheckPoint.AddListener(() => SaveCheckPoint());
        sceneController.onLoad.AddListener(() => LoadCheckPoint());
    }

    public void SaveCheckPoint()
    {
        
    }

    public void LoadCheckPoint()
    {
        Unlock();
        UnSelect();
    }

    private enum TargetType { Lock, Select }
}
