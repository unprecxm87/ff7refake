﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TargetDetector : MonoBehaviour
{
    [SerializeField] CharacterSelector characterSelector;
    private List<ActionSchedular> characterActions = new List<ActionSchedular>();
    public List<GameObject> Targets = new List<GameObject>();
    private List<EnemyDataBase> enemyDataBases = new List<EnemyDataBase>();
    private bool isTrigger = false;
    

    private void Awake()
    {
        foreach (GameObject target in Targets)
        {
            EnemyDataBase dataBase = target.GetComponent<EnemyDataBase>();
            //if (testState == null) break;
            dataBase.onDead.AddListener((obj) => RemoveFromTargets(obj));
            enemyDataBases.Add(dataBase);
        }
        foreach (GameObject character in characterSelector.Characters)
        {
            ActionSchedular action = character.GetComponent<ActionSchedular>();
            characterActions.Add(action);
        }
    }

    private void RemoveFromTargets(GameObject removedTarget)
    {
        Targets.Remove(removedTarget);
        if (Targets.Count == 0)
        {
            GameSystem.IsCombat = false;
            foreach (ActionSchedular action in characterActions)
            {
                Debug.Log("cancel in loop");
                action.SetCurrentActionAsNull();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //if (other.CompareTag("Enemy"))
        //{
        //    Targets.Add(other.gameObject);
        //}
        if (other.CompareTag("Player") && isTrigger == false)
        {
            foreach (GameObject character in characterSelector.Characters)
            {
                TargetSelector ts = character.gameObject.GetComponent<TargetSelector>();
                ts.EnemyList = Targets;
            }
            GameSystem.IsCombat = true;
            isTrigger = true;
        }
    }



}
