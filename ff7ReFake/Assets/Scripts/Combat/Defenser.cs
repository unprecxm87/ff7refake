﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defenser : MonoBehaviour, IAction
{
    private Animator animator;
    private ActionSchedular actionSchedular;
    public int Priority { get; private set; }

    private void Awake()
    {
        Priority = GameSystem.GetActionPriority(ActionPriority.Defense);
        animator = GetComponent<Animator>();
        actionSchedular = GetComponent<ActionSchedular>();
    }

    public void CancelAction()
    {
        animator.SetBool("OnDefense", false);
    }

    public void StartAction()
    {
        actionSchedular.SetCurrentAction(this);
        animator.SetBool("OnDefense", true);
    }
}
