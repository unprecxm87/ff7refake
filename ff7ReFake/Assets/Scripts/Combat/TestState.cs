﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TestState : MonoBehaviour
{
    [SerializeField] private float HP;
    private Renderer rend;
    public ObjectEvent onDead;

    public bool IsDead { get; private set; }

    public void MoidfyHP(float damage)
    {
        HP -= damage;
        if (HP <= 0.001f)
        {
            IsDead = true;
            onDead.Invoke(gameObject);
            //rend.material.SetColor("_BaseColor", Color.black);
            transform.localScale = new Vector3(1, 2, 1);
        }
    }
}
