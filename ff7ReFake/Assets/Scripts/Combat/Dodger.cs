﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Dodger : MonoBehaviour, IAction
{
    private ActionSchedular actionSchedular;
    private Animator animator;
    private float horizontal;
    private float vertical;

    public bool StopDodge { get; private set; }

    public int Priority { get; private set; }

    private void Awake()
    {
        Priority = GameSystem.GetActionPriority(ActionPriority.Dodge);
        actionSchedular = GetComponent<ActionSchedular>();
        animator = GetComponent<Animator>();
    }

    public void CancelAction()
    {
        
    }

    public void StartAction()
    {
        horizontal = Input.GetAxis(InputModule.AnalogStick("L X Axis"));
        vertical = Input.GetAxis(InputModule.AnalogStick("L Y Axis"));

        actionSchedular.SetCurrentAction(this);

        Vector3 dodgeDirection = GameSystem.GetDirectionWithCamera(vertical, horizontal);
        transform.forward = dodgeDirection;
        animator.SetTrigger("Dodge");

        if (!StopDodge)
        {
     
        }
    }

    public void SetStopDodge(int state)
    {
        if (state == 0) StopDodge = false;
        else StopDodge = true;
    }
}
