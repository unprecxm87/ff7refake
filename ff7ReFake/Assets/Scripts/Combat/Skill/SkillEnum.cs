﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Targets { Friends, Enemies }

public enum SkillType { Ability, Magic }

public enum GenerateType { LaunchFromHand, GenerateOnTarget, Ability }
