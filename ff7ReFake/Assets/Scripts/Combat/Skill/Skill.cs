﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http.Headers;
using UnityEditor;
//using UnityEditor.Experimental.TerrainAPI;
using UnityEngine;
using UnityEngine.VFX;

[CreateAssetMenu(menuName = "Magic")]


public class Skill : ScriptableObject
{
    public SkillType skillType;
    public string skillName;
    public float cost;
    public float recoverMP;
    public float damageHP;
    public float recoverHP;
    public AudioClip audio;
    public Projectile launchTypeMagic;
    public ParticleSystem MagicParticle;
    public VisualEffect MagicEffect;
    public Targets targets;
    public GenerateType generateType;
    public CheckPoint checkPoint;
    public string abilityType;

    public void GenerateMagic(Transform target, Vector3 hand, Transform instigator)
    {
        switch (generateType)
        {
            case GenerateType.LaunchFromHand:
                Projectile magicInstance = Instantiate(launchTypeMagic, hand, Quaternion.identity);
                ParticleSystem particleSystemInstance = Instantiate(MagicParticle, hand, Quaternion.LookRotation(target.position - hand));
                Destroy(particleSystemInstance.gameObject, 5);
                magicInstance.Target = target.position;
                magicInstance.Damage = damageHP;
                break;
            case GenerateType.GenerateOnTarget:
                //VisualEffect VisualEffectInstance = Instantiate(MagicEffect, target.position + new Vector3(0, 0.5f, 0), Quaternion.identity);
                VisualEffect VisualEffectInstance = Instantiate(MagicEffect, target.position + new Vector3(0, 0.5f, 0), Quaternion.identity);
                VisualEffectInstance.transform.parent = target.transform;
                VisualEffectInstance.Play();
                Destroy(VisualEffectInstance.gameObject, 1);
                break;
        }
    }

    internal string GenerateAdility()
    {
        return abilityType;    
    }
}
