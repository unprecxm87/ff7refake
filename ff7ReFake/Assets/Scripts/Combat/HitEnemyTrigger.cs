﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class HitEnemyTrigger : MonoBehaviour
{
    //[SerializeField] private ParticleSystem spark;
    [SerializeField] private VisualEffect spark;
    [SerializeField] private ParticleSystem slashTrail;
    [SerializeField] private GameObject owner;
    [SerializeField] private Vector3 sparkOffset;
    private Fighter ownerFighter;
    private State ownerState;

    public CollisionEvent onHitEnemy;
    private CinemachineImpulseSource cinemachineImpulseSource;
    private BoxCollider weapon;
    public GameObject objAllMusicManager;
    AllMusicManager scAllMusicManager;


    private void Awake()
    {
        cinemachineImpulseSource = Camera.main.GetComponent<CinemachineImpulseSource>();
        weapon = GetComponent<BoxCollider>();
        weapon.enabled = false;
        ownerFighter = owner.GetComponent<Fighter>();
        ownerState = owner.GetComponent<State>();
        slashTrail.Stop();
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            //TestState ts = other.GetComponent<TestState>();
            //ts.MoidfyHP(ownerFighter.GetHitValueAndReward());
            //hit value = ownerFighter.GetHitValueAndReward();
            //EnemyFSM01 efsm = other.GetComponent<EnemyFSM01>();
            EnemyDataBase enemyDataBase = other.gameObject.GetComponent<EnemyDataBase>();
            enemyDataBase.BeHit();
            RaycastHit[] hits = Physics.RaycastAll(transform.TransformPoint(weapon.center - new Vector3(weapon.size.x / 2, weapon.size.y / 2, 0)), transform.up, 4f);
            Hit(other, hits);
        }
    }

    private void Update()
    {
        if (ownerState.IsAttack) slashTrail.Play();
        else slashTrail.Stop();
        Debug.DrawLine(transform.TransformPoint(weapon.center - new Vector3(weapon.size.x / 2, weapon.size.y / 2, 0)), transform.up, Color.red);

    }

    private void Hit(Collider other, RaycastHit[] hits)
    {
        if (hits.Length == 0) return;
        //VisualEffect visualEffectInstance = Instantiate(spark, ownerFighter.transform.position + ownerFighter.transform.forward + sparkOffset, Quaternion.LookRotation(transform.right));
        VisualEffect visualEffectInstance = Instantiate(spark, hits[hits.Length - 1].point, Quaternion.LookRotation(transform.right));
        Destroy(visualEffectInstance.gameObject, 1);
        cinemachineImpulseSource.GenerateImpulse();
        //onHitEnemy.Invoke(other.gameObject);
        StartCoroutine(HitStop(ownerFighter.HitStopDuration));
        //int tmp = Random.Range(0, 3);
        //scAllMusicManager.MainScene_SwordSlashHitF();
        //if(tmp == 0)
        //{
        //    scAllMusicManager.Normal(scAllMusicManager.MainScene_SwordSlashHit1);
        //}
        //else
        //{
            scAllMusicManager.Normal(scAllMusicManager.MainScene_SwordSlashHit2);
        //}
    }

    private IEnumerator HitStop(float duration)
    {
        Time.timeScale = 0.01f;
        yield return new WaitForSeconds(0.01f * duration);
        Time.timeScale = 1;
    }
}
