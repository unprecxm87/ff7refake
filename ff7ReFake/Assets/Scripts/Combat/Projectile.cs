﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Vector3 Target { get; internal set; }
    public float Damage { get; internal set; }

    public GameObject objAllMusicManager;
    AllMusicManager scAllMusicManager;

    private Vector3 originalPosition;
    private Vector3 moveDirection;
    private GameObject kenny;
    [SerializeField] private ParticleSystem explosion;

    // Start is called before the first frame update
    void Start()
    {
        originalPosition = transform.position;
        moveDirection = Vector3.Normalize(GameSystem.GetHorizontalDirection(transform.position, Target));
        kenny = GameObject.Find("Kenny");
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Target == null) return;
        transform.Translate(GameSystem.GetHorizontalDirection(transform.position, Target) * Time.deltaTime);
        transform.position = Vector3.MoveTowards(transform.position, originalPosition + moveDirection * 10, 1);
        if (transform.position == (originalPosition + moveDirection * 10)) Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            EnemyDataBase otherEnemy = other.gameObject.GetComponent<EnemyDataBase>();
            Fighter kennyFighter = kenny.GetComponent<Fighter>();
            kennyFighter.hitByMagic = true;
            otherEnemy.BeHit();
            ParticleSystem particleSystemInstance = Instantiate(explosion, transform.position, Quaternion.LookRotation(transform.forward));
            Destroy(particleSystemInstance.gameObject, 2);
            scAllMusicManager.Normal(scAllMusicManager.MainScene_thunderMagic);

            //Fighter otherFighter = other.GetComponent<Fighter>();
            //otherFighter.BeHit(Damage);
            //Destroy(gameObject);
        }
    }

}
