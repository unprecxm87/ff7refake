﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SetSword : MonoBehaviour
{
    public GameObject weapon;
    public UnityEvent onPutWeapon;
    public UnityEvent onGetWeapon;
    [SerializeField] private Transform back;
    [SerializeField] private Vector3 backPosition;
    [SerializeField] private Vector3 backRotation;
    [SerializeField] private Transform hand;
    [SerializeField] private Vector3 handPosition;
    [SerializeField] private Vector3 handRotation;
    [SerializeField] private SceneController sceneController;

    public GameObject objAllMusicManager;
    AllMusicManager scAllMusicManager;

    // Start is called before the first frame update
    private void Awake()
    {
        sceneController.onLoad.AddListener(() => SetWeaponPosition(backPosition, backRotation, back));
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
    }
    void Start()
    {
        //onPutWeapon.AddListener(() => GetWeaponAlternate());
        //onGetWeapon.AddListener(() => PutWeaponAlternate());
        SetWeaponPosition(backPosition, backRotation, back);
    }

    private void SetWeaponPosition(Vector3 position, Vector3 rotationInEuler, Transform parent)
    {
        weapon.transform.parent = parent;
        weapon.transform.localPosition = position;
        Quaternion quaternion = Quaternion.Euler(rotationInEuler);
        weapon.transform.localRotation = quaternion;

    }

    //private void PutWeaponAlternate()
    //{
    //    weaponAlternate.SetActive(false);
    //}

    //private void GetWeaponAlternate()
    //{
    //    weaponAlternate.SetActive(true);
    //}

    // Update is called once per frame
    void PutWeapon()
    {
        SetWeaponPosition(backPosition, backRotation, back);
        scAllMusicManager.Normal(scAllMusicManager.MainScene_Unarm);
        //weapon.SetActive(false);
        //onPutWeapon.Invoke();
    }

    void GetWeapon()
    {
        SetWeaponPosition(handPosition, handRotation, hand);
        scAllMusicManager.Normal(scAllMusicManager.MainScene_Equip);
        //weapon.SetActive(true);
        //onGetWeapon.Invoke();
    }
}
