﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillLauncher : MonoBehaviour, IAction
{
    //[SerializeField] private List<List<Magic>> skillSlots = new List<List<Magic>>();
    [SerializeField] private SkillSlots skillSlots = new SkillSlots();
    [SerializeField] private Transform hand;
    [SerializeField] private Vector3 handOffset;
    //[SerializeField] private TargetDetector targetDetector;
    [SerializeField] private CharacterSelector characterSelector;
    private ActionSchedular actionSchedular;
    private TargetSelector targetSelector;
    private Animator animator;
    private State state;
    

    public int SkillItem { get; set; }

    public int SkillType { get; set; }

    public bool IsLaunchSkill { get; set; }

    public int Priority { get; private set; }

    public int MagicID { get; set; }

    public GameObject LaunchTarget { get; set; }

    public GameObject objAllMusicManager;
    AllMusicManager scAllMusicManager;

    private void Awake()
    {
        Priority = GameSystem.GetActionPriority(ActionPriority.LaunchSkill);
        actionSchedular = GetComponent<ActionSchedular>();
        targetSelector = GetComponent<TargetSelector>();
        animator = GetComponent<Animator>();
        state = GetComponent<State>();
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
    }

    public void StartAction()
    {
        actionSchedular.SetCurrentAction(this);
        if (LaunchTarget != null)
        {
            if (SkillType == 1) //Magic
            {
                if (skillSlots.skillTypes[SkillType].skillItem[SkillItem].name == "Heal")
                {
                    State launchTargetState = LaunchTarget.GetComponent<State>();
                    launchTargetState.ModifyHP(0, skillSlots.skillTypes[SkillType].skillItem[SkillItem].recoverHP);
                    StartCoroutine(scAllMusicManager.WaitAndPlay(1, scAllMusicManager.MainScene_HealMagic));
                    //scAllMusicManager.Main_Scene_HealMagicF();
                }
                transform.forward = LaunchTarget.transform.position - transform.position;
                SetLaunchAnimation();
            }
            if (SkillType == 0) //Ability
            {
                transform.forward = targetSelector.SelectTarget.transform.position - transform.position;
                animator.SetTrigger(skillSlots.skillTypes[SkillType].skillItem[SkillItem].GenerateAdility());
                if (StorySystem.Progress[skillSlots.skillTypes[SkillType].skillItem[SkillItem].checkPoint]) state.HitValue[State.HitType.AbilityHit] = skillSlots.skillTypes[SkillType].skillItem[SkillItem].damageHP;
                else state.HitValue[State.HitType.AbilityHit] = state.lightHit;
            }
                
            state.ModifyMP(skillSlots.skillTypes[SkillType].skillItem[SkillItem].cost, 0);
            state.ModifyATB(-state.MaxATB / 2);
            //state.ModifyEXP(10);
            //state.ATBTimes -= 1;
            //actionSchedular.SetCurrentActionAsNull();
        }
        //actionSchedular.SetCurrentActionAsNull();
    }

    private void SetLaunchAnimation()
    {
        animator.SetTrigger("Launch");
    }

    public void DoGenerateMagic ()
    {
        skillSlots.skillTypes[SkillType].skillItem[SkillItem].GenerateMagic(LaunchTarget.transform, hand.position + GameSystem.VectorOnSelfTransform(handOffset, transform), transform);
    }

    public void SetTargetList()
    {
        IsLaunchSkill = true;
        switch (skillSlots.skillTypes[SkillType].skillItem[SkillItem].targets)
        {
            case Targets.Friends:
                targetSelector.CurrentTargetList = characterSelector.Characters;
                break;
            case Targets.Enemies:
                targetSelector.CurrentTargetList = targetSelector.EnemyList;
                break;
        }
    }

    public void CancelAction()
    {

    }
}

[System.Serializable]
public class Slots
{
    public List<Skill> skillItem;
}

[System.Serializable]
public class SkillSlots
{
    public List<Slots> skillTypes;
}

//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class SkillLauncher : MonoBehaviour, IAction
//{
//    Animator animator;
//    ActionSchedular actionSchedular;

//    public int Priority { get; private set; }

//    private void Awake()
//    {
//        Priority = GameSystem.GetActionPriority("LaunchAbility");
//        animator = GetComponent<Animator>();
//        actionSchedular = GetComponent<ActionSchedular>();
//    }

//    private void Update()
//    {
//        if (Input.GetKeyDown(KeyCode.Alpha1)) Ability1();
//        if (Input.GetKeyDown(KeyCode.Alpha2)) Ability2();
//        if (Input.GetKeyDown(KeyCode.Alpha3)) Ability3();
//    }

//    public void Ability1()
//    {
//        actionSchedular.SetCurrentAction(this);
//        animator.SetTrigger("Combo 0");
//    }

//    public void Ability2()
//    {
//        actionSchedular.SetCurrentAction(this);
//        animator.SetTrigger("Combo 2");
//    }

//    public void Ability3()
//    {
//        actionSchedular.SetCurrentAction(this);
//        animator.SetTrigger("Combo 4");
//    }

//    public void StartAction()
//    {

//    }

//    public void CancelAction()
//    {

//    }
//}
