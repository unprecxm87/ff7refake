﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTargetLock : MonoBehaviour
{
    [SerializeField] private TargetSelector targetSelector;
    [SerializeField] private TargetDetector targetDetector;
    [SerializeField] private Color originalColor;
    private Renderer rend;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (targetSelector.SearchTarget(60, targetDetector.Targets) == gameObject) rend.material.SetColor("_BaseColor", Color.black);
        //else rend.material.SetColor("_BaseColor", originalColor);
        rend.material.SetColor("_BaseColor", originalColor);
    }
}
