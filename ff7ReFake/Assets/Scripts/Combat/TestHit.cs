﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestHit : MonoBehaviour
{
    Collider hit;
    private void Awake()
    {
        hit = GetComponent<Collider>();
        hit.enabled = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K)) hit.enabled = true;
        else hit.enabled = false;
    }
}
