﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{
    [SerializeField] private UIManager uIManager;
    [SerializeField] private CharacterSelector characterSelector;
    [SerializeField] private int characterOrder;
    [SerializeField] private float holdTime;
    [SerializeField] private Image holdImage;
    [SerializeField] private ParticleSystem drawMagicEffect;
    public UnityEvent onInteractWithScene;
    private ActionSchedular actionSchedular;
    private TargetSelector targetSelector;
    private SkillLauncher skillLauncher;
    private Defenser defenser;
    private Animator animator;
    private Fighter fighter;
    private Dodger dodger;
    private State state;
    private bool canChooseCharacter;
    private bool isAvoidTrigger;
    private float currentHoldTime;
    private TankHandler tankHandler;
    
    public GameObject objAllMusicManager;
    AllMusicManager scAllMusicManager;

    private void Awake()
    {
        actionSchedular = GetComponent<ActionSchedular>();
        targetSelector = GetComponent<TargetSelector>();
        skillLauncher = GetComponent<SkillLauncher>();
        defenser = GetComponent<Defenser>();
        animator = GetComponent<Animator>();
        fighter = GetComponent<Fighter>();
        dodger = GetComponent<Dodger>();
        state = GetComponent<State>();
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
    }
    private void SetWalkSound()
    {
        scAllMusicManager.Normal(scAllMusicManager.MainScene_KennyWalk);
    }
    private void SetNormalSwordSound4()
    {
        scAllMusicManager.Normal(scAllMusicManager.MainScene_SwordWave);
    }
    private void SetNormalSwordSound123()
    {
        scAllMusicManager.Normal(scAllMusicManager.MainScene_SwordWave123);
    }
    private void SetHealMagicSound()
    {
        scAllMusicManager.Normal(scAllMusicManager.MainScene_HealMagic);
    }
    private void SetDodgeSound()
    {
        scAllMusicManager.Normal(scAllMusicManager.MainScene_Dodge);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            GameSystem.IsCombat = !GameSystem.IsCombat;
        }
        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    ScreenCapture.CaptureScreenshot(name.ToString()+".png");
        //    name++;
        //}
        //Debug.Log(actionSchedular.CurrentAction);
        if(StorySystem.Progress[CheckPoint.Boss])
        {
            if (Input.GetButtonDown("X")) SceneManager.LoadScene("MainMenu");
            return;
        }

        if (state.IsDead) return;

        if (GameSystem.IsCommand)
        {
            isAvoidTrigger = true;
            return;
        }
        else if ((Input.GetButtonUp("A") || Input.GetButtonUp("B"))) isAvoidTrigger = false;
        if (!GameSystem.IsCombat) isAvoidTrigger = true;
        else if (Input.GetButtonUp("X")) isAvoidTrigger = false;
                    
        if (!GameSystem.IsCombat) NonCombatState();
        else CombatState();
    }

    private void CombatState()
    {
 
        //if (targetSelector.IsSelectTarget)
        //{
        //    Time.timeScale = 0.1f;
        //    if (skillLauncher.IsLaunchSkill)
        //    {
        //        if (Input.GetButtonDown("B"))
        //        {
        //            Debug.Log("launch");
        //            skillLauncher.LaunchTarget = targetSelector.SelectTarget;
        //            skillLauncher.StartAction();
        //            targetSelector.UnSelect();
        //            Time.timeScale = 1;
        //        }
        //    }
        //    return;
        //}
        //ChooseCharacter(Input.GetAxis(InputModule.AnalogStick("D-Pad X Axis")));
        // Choose the SelectTarget @ TargetSelector
        if (Input.GetButtonDown("B") && state.ATBTimes != 0 && !isAvoidTrigger)
        {
            uIManager.EnterCommand();
        }

        if (Input.GetButtonDown("R3"))
        {
            if (targetSelector.IsLockTarget) targetSelector.Unlock();
            else targetSelector.Lock();
        }
        // Choose the LockTarget @ TargetSelector
        //if (Input.GetButtonDown("X")) fighter.StartAction();
        if (Input.GetButton("X") && !isAvoidTrigger) fighter.StartAction();
        else fighter.CancelAction();
        if (Input.GetButtonDown("A") && !dodger.StopDodge && !isAvoidTrigger) dodger.StartAction();
        if (Input.GetButton("L1")) defenser.StartAction();
        else defenser.CancelAction();
    }

    private void ChooseCharacter(float input)
    {
        if ((input >= 0.01f || input <= -0.01f) && canChooseCharacter)
        {
            characterSelector.SetCurrentPlayer(GameSystem.ChangeCharacterOrder());
            canChooseCharacter = false;
        }
        else if (input < 0.01f && input > -0.01f && !canChooseCharacter) canChooseCharacter = true;
    }

    private void NonCombatState()
    {
        //if (Input.GetButtonDown("B")) uIManager.EnterCommand();
        if (tankHandler != null)
        {
            if (Input.GetButton("X") && tankHandler.isPlayerEnter && !tankHandler.isCompleteHandleTank && animator.GetCurrentAnimatorStateInfo(0).IsName("NonCombatIdle"))
            {
                currentHoldTime += Time.deltaTime;
                holdImage.fillAmount = currentHoldTime / holdTime;
                drawMagicEffect.Play();
                if (currentHoldTime > holdTime)
                {
                    onInteractWithScene.Invoke();
                    scAllMusicManager.Normal(scAllMusicManager.MainScene_DrawMagic);
                    holdImage.fillAmount = currentHoldTime = 0;
                }
            }
        }
        else if (Input.GetButtonUp("X"))
        {
            holdImage.fillAmount = currentHoldTime = 0;
            drawMagicEffect.Stop();
        }
    }

 

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("TankHandler"))
        {
            tankHandler = other.gameObject.GetComponent<TankHandler>();
        }
    }
}
