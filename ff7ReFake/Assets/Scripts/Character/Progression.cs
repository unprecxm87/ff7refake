﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.Analytics;

[CreateAssetMenu(menuName = "Progression")]
public class Progression : ScriptableObject
{
    public ProgressionState[] states;

    Dictionary<StateType, float[]> lookUpTable;
    

    public float GetState(StateType stateType, int level)
    {
        BuildLookUpTable();
        return lookUpTable[stateType][level];
    }

    public void BuildLookUpTable()
    {
        if (lookUpTable != null) return;

        lookUpTable = new Dictionary<StateType, float[]>();

        foreach (ProgressionState state in states)
        {
            lookUpTable[state.stateType] = state.levels;
        }

    }
}

[System.Serializable]
public class ProgressionState
{
    public StateType stateType;
    public float[] levels;
}

public enum StateType
{
    MaxHP,
    MaxMP,
    Damage,
    LevelUpEXP
}