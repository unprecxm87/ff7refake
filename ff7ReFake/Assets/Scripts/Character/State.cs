﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



public class State : MonoBehaviour, ISaveSystem
{
    public float currentHP;
    public float currentMP;
    public float totalAttackValue;
    public float totalDefenseValue;
    public int ATBTimes;
    public float abilityHit;
    public float MaxHP;
    public float MaxMP;
    public float MaxATB;
    public int LevelUpEXP;

    [SerializeField] private float currentEXP;
    [SerializeField] private float currentATB;
    [SerializeField] public float lightHit;
    [SerializeField] public float heavyHit;
    [SerializeField] private Progression progression;
    [SerializeField] private SceneController sceneManager;
    [SerializeField] private int currentLevel = 0;
    [SerializeField] private ParticleSystem levelUpEffect;
    
    private Animator animator;
    private ActionSchedular actionSchedular;
    public ValueEvent onModifyHP;
    public ValueEvent onModifyMP;
    public ValueEvent onModifyATB;
    public ValueEvent onModifyEXP;
    public ValueEvent onModifyEXPUIM;
    public ValueEvent onLevelUpHP;
    public ValueEvent onLevelUpMP;
    public UnityEvent onLevelUp;
    public ValueEvent onLevleUpEXPUIM;
    public UnityEvent onDead;
    public HitType currentHitType;
    public Dictionary<HitType, float> HitValue;
    private StateData stateData;

    public GameObject objAllMusicManager;
    AllMusicManager scAllMusicManager;

    public enum HitType { LightHit, HeavyHit, AbilityHit}

    public bool IsDefense { get; set; }

    public bool IsDodge { get; set; }

    public bool IsAttack { get; set; }

    public bool IsDead { get; private set; }

    private void Awake()
    {
        SetSaveListener();
        animator = GetComponent<Animator>();
        actionSchedular = GetComponent<ActionSchedular>();
        InitiateLevel();
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
        HitValue = new Dictionary<HitType, float>()
        {
            { HitType.LightHit, lightHit}, { HitType.HeavyHit, lightHit * 1.1f}, { HitType.AbilityHit, abilityHit } 
        };
    }

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.M))
    //    {
    //        Debug.Log("zero");
    //        transform.position = Vector3.zero;
    //    }
    //}

    private void InitiateLevel()
    {
        MaxHP = progression.GetState(StateType.MaxHP, 0);
        MaxMP = progression.GetState(StateType.MaxMP, 0);
        lightHit = progression.GetState(StateType.Damage, 0);
        LevelUpEXP = (int)progression.GetState(StateType.LevelUpEXP, 0);
        currentHP = MaxHP;
        currentMP = MaxMP;
    }

    private void LevelUp()
    {
        if (currentLevel == 4) return;
        currentLevel++;
        MaxHP = progression.GetState(StateType.MaxHP, currentLevel);
        MaxMP = progression.GetState(StateType.MaxMP, currentLevel);
        lightHit = progression.GetState(StateType.Damage, currentLevel);
        LevelUpEXP = (int)progression.GetState(StateType.LevelUpEXP, currentLevel);
        currentEXP = 0;
        currentHP = MaxHP;
        currentMP = MaxMP;
        onLevelUpHP.Invoke(MaxHP);
        onLevelUpMP.Invoke(MaxMP);
        onLevelUp.Invoke();
        levelUpEffect.Play();
        scAllMusicManager.Normal(scAllMusicManager.MainScene_RankUp);
    }

    public void ModifyEXP(int amount)
    {
        currentEXP += amount;

        if (currentEXP >= LevelUpEXP)
        {
            LevelUp();
            onLevleUpEXPUIM.Invoke(LevelUpEXP);
        }
        else
        {
            onModifyEXP.Invoke(amount);
            onModifyEXPUIM.Invoke(amount);
        }
    }

    internal void ModifyATB(float amount)
    {
        currentATB += amount;
        currentATB = Mathf.Max(currentATB, 0);
        currentATB = Mathf.Min(currentATB, MaxATB);
        //Debug.Log("currentATB: " + currentATB);
        if (MaxATB / 2 - currentATB <= 0.001f && MaxATB - currentATB > 0.001f ) ATBTimes = 1;
        else if (MaxATB - currentATB <= 0.001f) ATBTimes = 2;
        else if (MaxATB / 2 - currentATB > 0.001f) ATBTimes = 0;
        onModifyATB.Invoke(amount);
    }

    internal void ModifyMP(float cost, float recoverValue)
    {
        currentMP -= cost;
        currentMP += recoverValue;
        currentMP = Mathf.Max(currentMP, 0);
        currentMP = Mathf.Min(currentMP, MaxMP);
        onModifyMP.Invoke(-cost + recoverValue);
    }

    internal void ModifyHP(float damageValue, float recoverValue)
    {
        currentHP -= damageValue - totalDefenseValue;
        currentHP += recoverValue;
        currentHP = Mathf.Max(currentHP, 0);
        currentHP = Mathf.Min(currentHP, MaxHP);
        onModifyHP.Invoke(-damageValue + recoverValue);
        if (currentHP > 0.001f && damageValue > 0.001f)
        {
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("DefenseLoop")) animator.SetTrigger("OnDefenseHit");
            else if (actionSchedular.CurrentAction.Priority == 0)
            {
                animator.SetTrigger("ByHit");

            }

            //else animator.SetTrigger("ByHit");
        }
        else if (currentHP <= 0.001f) Dead(true);
    }

    internal bool Dead(bool death)
    {
        IsDead = death;
        SetAnimation(death);
        return IsDead;
    }

    private void SetAnimation(bool death)
    {
        if (death) animator.SetTrigger("isDead");
        animator.SetBool("isAlive", !death);
        onDead.Invoke();
    }

    public void SaveCheckPoint()
    {
        stateData = new StateData(this);
        //Debug.Log("save position: " + stateData.position);
    }

    public void LoadCheckPoint()
    {
        StartCoroutine(SetPosition());
        animator.Play("NonCombatIdle");
        animator.SetBool("onBattle", false);
        animator.SetBool("isAlive", true);
        animator.ResetTrigger("ByHit");
        GameSystem.IsCombat = false;
        actionSchedular.SetCurrentActionAsNull();
        

        currentLevel = stateData.currentLevel;
        MaxHP = progression.GetState(StateType.MaxHP, currentLevel);
        MaxMP = progression.GetState(StateType.MaxMP, currentLevel);
        lightHit = progression.GetState(StateType.Damage, currentLevel);
        currentEXP = stateData.currentEXP;
        currentATB = stateData.currentATB;
        currentHP = MaxHP;
        currentMP = MaxMP;
        onLevelUpHP.Invoke(MaxHP);
        onLevelUpMP.Invoke(MaxMP);
        IsDead = stateData.IsDead;

    }

    private IEnumerator SetPosition()
    {
        animator.applyRootMotion = false;
        transform.position = stateData.position;
        transform.rotation = stateData.rotation;
        //Debug.Log("save position: " + stateData.position);
        yield return new WaitForSeconds(0.1f);
        //yield return 0;
    
        animator.applyRootMotion = true;
    }

    public void SetSaveListener()
    {
        sceneManager.onArriveCheckPoint.AddListener(() => SaveCheckPoint());
        sceneManager.onLoad.AddListener(() => LoadCheckPoint());
    }

    private class StateData
    {
        public int currentLevel;
        public bool IsDead;
        public float currentEXP;
        public float currentATB;
        public Vector3 position;
        public Quaternion rotation;

        public StateData(State state)
        {
            currentLevel = state.currentLevel;
            IsDead = state.IsDead;
            currentEXP = state.currentEXP;
            currentATB = state.currentATB;
            position = state.transform.position;
            rotation = state.transform.rotation;
        }
    }
}
