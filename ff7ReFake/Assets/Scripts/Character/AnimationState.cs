﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationState : MonoBehaviour
{
    Animator animator;
    ActionSchedular actionSchedular;
    State state;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        actionSchedular = GetComponent<ActionSchedular>();
        state = GetComponent<State>();
    }

    private bool Test()
    {
        return true;
    }
    void Update()
    {
        SetLaunchMagicWeight();
        SetDefenseWeight();
        SetDefenseState();
        SetDodgeState();
        SetAttackState();
        SetBeHitState();

        if (animator.GetAnimatorTransitionInfo(1).IsName("LaunchMagic -> Idle")) actionSchedular.SetCurrentActionAsNull();
        

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Combo0")) state.currentHitType = State.HitType.AbilityHit;
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Combo1")) state.currentHitType = State.HitType.AbilityHit;
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Combo2")) state.currentHitType = State.HitType.AbilityHit;
    }

    private void SetBeHitState()
    {
        if (animator.GetAnimatorTransitionInfo(0).IsName("BeHit -> Idle")) actionSchedular.SetCurrentActionAsNull();
        if (animator.GetAnimatorTransitionInfo(0).IsName("BeHit -> Idle")) animator.ResetTrigger("ByHit");
    }

    private void SetAttackState()
    {
        if (animator.GetAnimatorTransitionInfo(0).IsName("Attack1 -> Idle")) actionSchedular.SetCurrentActionAsNull();
        if (animator.GetAnimatorTransitionInfo(0).IsName("Attack2 -> Idle")) actionSchedular.SetCurrentActionAsNull();
        if (animator.GetAnimatorTransitionInfo(0).IsName("Attack3 -> Idle")) actionSchedular.SetCurrentActionAsNull();
    
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack1"))
        {
            state.currentHitType = State.HitType.LightHit;
            state.IsAttack = true;
        }
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack2"))
        {
            state.currentHitType = State.HitType.LightHit;
            state.IsAttack = true;
        }
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack3"))
        {
            state.currentHitType = State.HitType.HeavyHit;
            state.IsAttack = true;
        }

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Idle") || animator.GetCurrentAnimatorStateInfo(0).IsName("RunStart")) state.IsAttack = false;
    }

    private void SetDodgeState()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Dodge")) state.IsDodge = true;
        else state.IsDodge = false;
        if (animator.GetAnimatorTransitionInfo(0).IsName("Dodge -> Idle")) actionSchedular.SetCurrentActionAsNull();
    }

    private void SetDefenseState()
    {
        if (animator.GetAnimatorTransitionInfo(0).IsName("DefenseEnd -> Idle"))
        {
            state.IsDefense = false;
            actionSchedular.SetCurrentActionAsNull();
        }
        if (animator.GetAnimatorTransitionInfo(0).IsName("Idle -> DefenseStart")) state.IsDefense = true;
    }

    private void SetLaunchMagicWeight()
    {
        if (animator.GetCurrentAnimatorStateInfo(1).IsName("LaunchMagic")) animator.SetLayerWeight(1, 1);
        else animator.SetLayerWeight(1, 0);
    }

    private void SetDefenseWeight()
    {
        if (animator.GetCurrentAnimatorStateInfo(2).IsName("GreatSword_Defense_Hit")) animator.SetLayerWeight(2, 1);
        else animator.SetLayerWeight(2, 0);
    }
}
