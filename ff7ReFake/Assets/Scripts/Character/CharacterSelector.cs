﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class CharacterSelector : MonoBehaviour
{
    [HideInInspector] public GameObject CurrentCharacter;
    [HideInInspector] public GameObject NonCurrentCharacter;

    public List<GameObject> Characters = new List<GameObject>();

    public UnityEvent onChangeCharacter;

    private void Awake()
    {
        SetCurrentPlayer(0);
    }
    
    public void SetCurrentPlayer (int order)
    {
        foreach (GameObject character in Characters)
        {
            if (character != Characters[order])
            {
                NonCurrentCharacter = character;
                PlayerController p = character.GetComponent<PlayerController>();
                Mover m = character.GetComponent<Mover>();
                TargetSelector t = character.GetComponent<TargetSelector>();
                t.Unlock();
                p.enabled = false;
                m.enabled = false;
            }
            else
            {
                CurrentCharacter = character;
                PlayerController p = character.GetComponent<PlayerController>();
                Mover m = character.GetComponent<Mover>();
                p.enabled = true;
                m.enabled = true;
            }
        }
        onChangeCharacter.Invoke();
    }
}