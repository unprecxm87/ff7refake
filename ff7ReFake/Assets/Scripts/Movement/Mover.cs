﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Mover : MonoBehaviour, IAction
{    
    [SerializeField] private float rotateSpeed;
    [SerializeField] private float allowRotation;
    public Vector3 moveDirection;
    private Animator animator;
    private CharacterController characterController;
    private ActionSchedular actionSchedular;
    private float moveSpeed;
    private float horizontal;
    private float vertical;
    private Vector3 gravity;
    private Vector2 input;
    private bool isStay;
    private bool stopMove;
    private bool isHandleSword;
    private bool isMoveOn;
    private State state;

    public int Priority { get; private set; }

    private void Awake()
    {
        Priority = GameSystem.GetActionPriority(ActionPriority.Move);
        animator = GetComponent<Animator>();
        characterController = GetComponent<CharacterController>();
        actionSchedular = GetComponent<ActionSchedular>();
        state = GetComponent<State>();
    }

    private void Start()
    {
        gravity = new Vector3(0, -9.8f * Time.deltaTime, 0);
        moveSpeed = 5.0f;
        isHandleSword = false;
    }

    private void Update()
    {
        actionSchedular.SetCurrentAction(this);
        if (actionSchedular.CurrentAction == this as IAction && !isHandleSword && !state.IsDead)
        {
            horizontal = Input.GetAxis(InputModule.AnalogStick("L X Axis"));
            vertical = Input.GetAxis(InputModule.AnalogStick("L Y Axis"));
        }
        else CancelAction();

        input = new Vector2(horizontal, vertical);
        StartCoroutine(DetectStay());
        SetAnimation();
        StartAction();
        StandOnGround();
        HandleSword();

        //actionSchedular.SetCurrentAction(this);
        //if (actionSchedular.CurrentAction == this as IAction && !isHandleSword)
        //{
        //    horizontal = Input.GetAxis(InputModule.AnalogStick("L X Axis"));
        //    vertical = Input.GetAxis(InputModule.AnalogStick("L Y Axis"));
        //}
        //else CancelAction();

        //input = new Vector2(horizontal, vertical);
        //StartCoroutine(DetectStay());
        //SetAnimation();
        //StartAction();
        //StandOnGround();
        //HandleSword();
    }

    private void HandleSword()
    {
        if (!GameSystem.IsCombat && animator.GetBool("onBattle"))
        { 
            isHandleSword = true;
            animator.SetBool("onBattle", false);
        }
        if (GameSystem.IsCombat && !animator.GetBool("onBattle"))
        {
            isHandleSword = true;
            animator.SetBool("onBattle", true);
        }
    }

    public void EndHandleSword()
    {
        isHandleSword = false;
    }

    private void StandOnGround()
    {
        if (!characterController.isGrounded)
        {
            characterController.Move(gravity);
        }
    }

    //private void SetAnimation()
    //{
    //    animator.SetFloat("Speed", input.magnitude + Input.GetAxis("R2"));
    //    if (input.magnitude < 0.01) animator.SetFloat("Speed", 0);
    //    if (Input.GetAxis("R2") > 0.01) moveSpeed = 15.0f;
    //    else moveSpeed = 10.0f;
    //}

    //private void SetAnimation()
    //{
    //    animator.SetFloat("Speed", movemove.magnitude);
    //    //Debug.Log("speed: " + animator.GetFloat("Speed") + " " + movemove.magnitude);
    //}

    //public void SetStay()
    //{
    //    if (!isMoveOn) movemove = Vector3.zero;
    //    Debug.Log("movemove: " + movemove);
    //}

    //public void StartAction()
    //{
    //    if (input.sqrMagnitude > allowRotation)
    //    {
    //        moveDirection = GetMoveDirection(vertical, horizontal);
    //        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(moveDirection), Time.deltaTime * rotateSpeed);
    //    }
    //    if (input.sqrMagnitude > 0.9f)
    //    {
    //        movemove = GetMoveDirection(vertical, horizontal);
    //        isMoveOn = true;
    //    }
    //    else isMoveOn = false;
    //    characterController.Move(movemove * Time.deltaTime * moveSpeed);
    //}

    private void SetAnimation()
    {
        if (isStay) animator.SetFloat("Speed", 0);
        else animator.SetFloat("Speed", moveDirection.magnitude);
    }

    private IEnumerator DetectStay()
    {
        Vector2 lastInput = new Vector2(horizontal, vertical);
        yield return new WaitForSeconds(0.05f);
        Vector2 currentInput = new Vector2(horizontal, vertical);
        isStay = lastInput == Vector2.zero && currentInput == Vector2.zero;
    }

    public void StartAction()
    {
        //if (input.sqrMagnitude > allowRotation && !stopMove)
        if (input.sqrMagnitude > allowRotation)
        {
            moveDirection = GameSystem.GetDirectionWithCamera(vertical, horizontal);
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(moveDirection), Time.deltaTime * rotateSpeed);
            //characterController.Move(moveDirection * Time.deltaTime * moveSpeed);
            //animator.SetFloat("Speed", moveDirection.magnitude + Input.GetAxis("R2"));
            //if (moveDirection.magnitude < 0.1) animator.SetFloat("Speed", 0);
        }
    }

    private void SetDirectValue(ref float v, ref float h)
    {
        if (vertical > 0.01f) v = 1;
        else if (vertical < -0.01f) v = -1;
        else if (vertical > -0.01f && vertical < 0.01f) v = 0;
        if (horizontal > 0.01f) h = 1;
        else if (horizontal < -0.01f) h = -1;
        else if (horizontal > -0.01f && horizontal < 0.01f) h = 0;
    }

    public void SetStopMove(int state)
    {
        if (state == 0) stopMove = false;
        else stopMove = true;
    }

    public void CancelAction()
    {
        horizontal = 0;
        vertical = 0;
    }
}
