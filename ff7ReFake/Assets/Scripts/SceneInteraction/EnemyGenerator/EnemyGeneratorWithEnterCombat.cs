﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGeneratorWithEnterCombat : EnemyGenerator
{

    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        GameSystem.IsCombat = true;
    }
}
