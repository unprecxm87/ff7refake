﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class EnemyGenerator : MonoBehaviour, ISaveSystem
{
    [SerializeField] private List<Transform> soldierTransforms = new List<Transform>();
    [SerializeField] private List<Transform> captainTransforms = new List<Transform>();
    [SerializeField] private List<Transform> eliteTransforms = new List<Transform>();
    [SerializeField] private List<Transform> bossTranforms = new List<Transform>();
    [SerializeField] private EnemyPool enemyPool;
    [SerializeField] private CharacterSelector characterSelector;
    [SerializeField] private GameObject block;
    [SerializeField] private SceneController sceneManager;
    [SerializeField] protected DialogueSystem dialogueSystem;
    [SerializeField] protected int beginningDialogueOrder;
    [SerializeField] protected int endingDialogueOrder;
    [SerializeField] protected float endingDialogueDelayTime;
    protected bool isPlayerEnter;
    protected bool isFinishGenerate;
    protected bool isClearEnemies;
    public UnityEvent onClearEnemies;
    public UnityEvent onSetEnemies;
    private EnemyGeneratorData enemyGeneratorData;
    private bool isBlock = true;

    public List<GameObject> Enemies { get; private set; } = new List<GameObject>();

    protected virtual void Awake()
    {
        SetSaveListener();
        onClearEnemies.AddListener(() =>
        {
            isBlock = false;
            if (block != null) block.SetActive(isBlock);
        });
        
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !isFinishGenerate)
        {
            SetEnemies();
            dialogueSystem.ShowDialogue(beginningDialogueOrder);
        }
    }

    protected void SetEnemies()
    {
        GenerateEnemies(soldierTransforms, EnemyType.Soldier);
        GenerateEnemies(captainTransforms, EnemyType.Captain);
        GenerateEnemies(eliteTransforms, EnemyType.Elite);
        GenerateEnemies(bossTranforms, EnemyType.Boss);
        foreach (GameObject character in characterSelector.Characters)
        {
            TargetSelector targetSelector = character.GetComponent<TargetSelector>();
            targetSelector.EnemyList = Enemies;
            //Debug.Log("EnemyList: " + targetSelector.EnemyList.Count);
        }
        onSetEnemies.Invoke();
        isFinishGenerate = true;
    }

    private void GenerateEnemies(List<Transform> enemyTransforms, EnemyType enemyType)
    {
        //Debug.Log("generate:" + enemyType);
        foreach (Transform enemyTransform in enemyTransforms)
        {
            //Debug.Log("enemyTransform: " + enemyTransform);
            GameObject enemy = enemyPool.GenerateEnemy(enemyType, enemyTransform.position);
            EnemyDataBase enemyDataBase = enemy.GetComponent<EnemyDataBase>();
            enemyDataBase.onDead.AddListener((obj) => RemoveEnemy(obj));
            enemy.transform.forward = GameSystem.GetHorizontalDirection(enemy.transform.position, characterSelector.CurrentCharacter.transform.position);
            Enemies.Add(enemy);
        }
    }

    private void RemoveEnemy(GameObject removedTarget)
    {
        Enemies.Remove(removedTarget);
        //Debug.Log(gameObject.name + "Enemies.Count after remove: " + Enemies.Count);

        if (Enemies.Count == 0 && !isClearEnemies)
        {
            //Debug.Log("clear");
            onClearEnemies.Invoke();
            StartCoroutine(DelayDialogueShowing(endingDialogueDelayTime));
            isClearEnemies = true;
        }
    }

    private IEnumerator DelayDialogueShowing(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        dialogueSystem.ShowDialogue(endingDialogueOrder);
    }

    public void SaveCheckPoint()
    {
        enemyGeneratorData = new EnemyGeneratorData(this);
    }

    public void LoadCheckPoint()
    {
        isPlayerEnter = enemyGeneratorData.isPlayerEnter;
        isFinishGenerate = enemyGeneratorData.isFinishGenerate;
        isBlock = enemyGeneratorData.isBlock;
        if (block != null) block.SetActive(isBlock);
        Enemies.Clear();
}

    public void SetSaveListener()
    {
        sceneManager.onArriveCheckPoint.AddListener(() => SaveCheckPoint());
        sceneManager.onLoad.AddListener(() => LoadCheckPoint());
    }

    private class EnemyGeneratorData
    {
        public bool isPlayerEnter;
        public bool isFinishGenerate;
        public bool isBlock;
        public EnemyGeneratorData(EnemyGenerator enemyGenerator)
        {
            isPlayerEnter = enemyGenerator.isPlayerEnter;
            isFinishGenerate = enemyGenerator.isFinishGenerate;
            isBlock = enemyGenerator.isBlock;
        }
    }

}




