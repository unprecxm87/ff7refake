﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator0 : EnemyGenerator
{
    [SerializeField] private TankHandler tankHandler;

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) isPlayerEnter = false;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) isPlayerEnter = true;
        if (tankHandler.isCompleteHandleTank && isPlayerEnter && !isFinishGenerate)
        {
            onSetEnemies.Invoke();
            SetEnemies();
            GameSystem.IsCombat = true;
            dialogueSystem.ShowDialogue(beginningDialogueOrder);
        }
    }
}
