﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TankHandler : MonoBehaviour, ISaveSystem
{
    [SerializeField] protected List<Light> lights = new List<Light>();
    [SerializeField] protected List<ParticleSystem> smoke = new List<ParticleSystem>();
    [SerializeField] protected PlayerController playerController;
    [SerializeField] protected CheckPoint checkPoint;
    [SerializeField] private EnemyGenerator enemyGenerator;
    [SerializeField] private GameObject block;
    [SerializeField] protected SceneController sceneManager;
    [SerializeField] protected CanvasGroup longPressIcon;
    [SerializeField] protected Vector3 iconOffset;
    [SerializeField] protected DialogueSystem dialogueSystem;
    [SerializeField] protected int enterDialogueOrder;
    [SerializeField] protected int finishDialogueOrder;
    [SerializeField] Transform start;
    [SerializeField] Transform goal;
    [SerializeField] Michibiki michibiki;

    public bool isCompleteHandleTank;
    public bool isPlayerEnter;
    public bool isMichibikiFly;
    private bool isClearEnemies;
    private bool isBlock = true;
    private TankHadlerData tankHadlerData;
    private Animator animator;
    private ActionSchedular actionSchedular;
    private bool isArriveGoal;

    protected virtual void Awake()
    {
        SetSaveListener();
        enemyGenerator.onClearEnemies.AddListener(() => isClearEnemies = true);
        playerController.onInteractWithScene.AddListener(() => HandleTank());
        animator = playerController.GetComponent<Animator>();
        actionSchedular = playerController.GetComponent<ActionSchedular>();
    }

    protected virtual void HandleTank()
    {
        if (isPlayerEnter)
        {
            foreach (ParticleSystem s in smoke)
            {
                s.Stop();
            }
            foreach (Light light in lights)
            {
                StartCoroutine(TurnOffLight(light));
            }
            if (block != null)
            {
                isBlock = false;
                block.SetActive(isBlock);
            }
            isCompleteHandleTank = true;
            longPressIcon.DOFade(0, 0.1f);
            StorySystem.Progress[checkPoint] = true;
            sceneManager.onArriveCheckPoint.Invoke();
            //Debug.Log("dialogue");
            if (michibiki != null)
            {
                michibiki.michibikitrail.Clear();
                michibiki.michibikitrail.Play();
            }
            isMichibikiFly = true;
            dialogueSystem.ShowDialogue(finishDialogueOrder);
        }
    }

    private void Update()
    {
        if (isMichibikiFly && StorySystem.Progress[checkPoint] && michibiki != null && !isArriveGoal)
        {
            michibiki.StarMichibiki(start, goal);
            if (Vector3.Distance(michibiki.transform.position, goal.position) < 0.1f) isArriveGoal = true;
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isPlayerEnter = true;
            if (!isCompleteHandleTank && isClearEnemies)
            {
                longPressIcon.transform.parent = transform;
                longPressIcon.transform.localPosition = iconOffset;
                longPressIcon.DOFade(1, 0.1f);
                dialogueSystem.ShowDialogue(enterDialogueOrder);
            }
        }
        if (isPlayerEnter && !StorySystem.Progress[checkPoint] && isClearEnemies)
        {
            GameSystem.IsCombat = false;
            animator.SetBool("Attack", false);
            //actionSchedular.SetCurrentActionAsNull();
        }
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isPlayerEnter = false;
            if (!isCompleteHandleTank) longPressIcon.DOFade(0, 0.1f);
        }
    }

    protected IEnumerator TurnOffLight(Light light)
    {
        while (light.intensity > 0.001f)
        {
            light.intensity -= 0.1f;
            yield return 0;
        }
        light.intensity = 0;
    }

    public void SaveCheckPoint()
    {
        tankHadlerData = new TankHadlerData(this);
    }

    public void LoadCheckPoint()
    {
        isCompleteHandleTank = tankHadlerData.isCompleteHandleTank;
        isPlayerEnter = tankHadlerData.isPlayerEnter;
        isClearEnemies = tankHadlerData.isClearEnemies;
        if (block != null) block.SetActive(isBlock);
    }

    public void SetSaveListener()
    {
        sceneManager.onArriveCheckPoint.AddListener(() => SaveCheckPoint());
        sceneManager.onLoad.AddListener(() => LoadCheckPoint());
    }

    private class TankHadlerData
    {
        public bool isCompleteHandleTank;
        public bool isPlayerEnter;
        public bool isClearEnemies;
        public bool isBlock;

        public TankHadlerData(TankHandler tankHandler)
        {
            isCompleteHandleTank = tankHandler.isCompleteHandleTank;
            isPlayerEnter = tankHandler.isPlayerEnter;
            isClearEnemies = tankHandler.isClearEnemies;
            isBlock = tankHandler.isBlock;
        }
    }
}
