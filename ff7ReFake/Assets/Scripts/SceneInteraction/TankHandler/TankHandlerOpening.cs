﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TankHandlerOpening : TankHandler
{
    protected override void Awake()
    {
        SetSaveListener();
        playerController.onInteractWithScene.AddListener(() => HandleTank());
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isPlayerEnter = true;
            if (!isCompleteHandleTank)
            {
                longPressIcon.transform.SetParent(transform);
                longPressIcon.transform.localPosition = iconOffset;
                longPressIcon.DOFade(1, 0.1f);
                dialogueSystem.ShowDialogue(enterDialogueOrder);
            }
        }
    }
}
