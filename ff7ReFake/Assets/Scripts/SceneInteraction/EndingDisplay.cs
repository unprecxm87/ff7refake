﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingDisplay : MonoBehaviour
{
    [SerializeField] private EnemyGenerator enemyGenerator;
    [SerializeField] private CanvasGroup canvas;

    private void Awake()
    {
        enemyGenerator.onClearEnemies.AddListener(() => StartCoroutine(DisplayEnding()));
    }

    private IEnumerator DisplayEnding()
    {
        yield return new WaitForSeconds(5);
        canvas.DOFade(1, 0.1f);
        StorySystem.Progress[CheckPoint.Boss] = true;
    }
}
