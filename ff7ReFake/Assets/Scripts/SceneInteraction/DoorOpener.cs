﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class DoorOpener : MonoBehaviour
{
    [SerializeField] private bool inverse;
    [SerializeField] private CheckPoint checkPoint;
    private Vector3 originalPosition;


    private void Awake()
    {
        originalPosition = transform.position;
    }
    void Update()
    {
        if (StorySystem.Progress[checkPoint] && inverse) transform.position = Vector3.MoveTowards(transform.position, originalPosition - (transform.right * 3), 0.01f);
        else if (StorySystem.Progress[checkPoint] && !inverse) transform.position = Vector3.MoveTowards(transform.position, originalPosition + (transform.right * 3), 0.01f);
    }
}
