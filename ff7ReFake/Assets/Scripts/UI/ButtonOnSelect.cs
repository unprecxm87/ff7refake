﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonOnSelect : MonoBehaviour, ICancelHandler
{
    [SerializeField] private CharacterSelector characterSelector;
    [SerializeField] private int skillType;
    [SerializeField] private int skillItem;
    [SerializeField] private UIManager uIManager;
    [SerializeField] private CanvasGroup currentCanvasGroup;
    [SerializeField] private CanvasGroup nextCanvasGroup;
    [SerializeField] private CanvasGroup previousCanvasGroup;
    private Button button;
    private SkillLauncher skillLauncher;
    private TargetSelector targetSelector;
    public GameObject objAllMusicManager;
    AllMusicManager scAllMusicManager;

    public void OnCancel(BaseEventData eventData)
    {
        uIManager.ShowList(currentCanvasGroup, previousCanvasGroup);
        uIManager.title.text = "command";
    }

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => Onclick());
    }

    private void Onclick()
    {
        skillLauncher = characterSelector.CurrentCharacter.GetComponent<SkillLauncher>();
        targetSelector = characterSelector.CurrentCharacter.GetComponent<TargetSelector>();
        skillLauncher.SkillType = skillType;
        skillLauncher.SkillItem = skillItem;
        skillLauncher.SetTargetList();
        targetSelector.Select();
        if (skillType == 0) { uIManager.ShowList(currentCanvasGroup, nextCanvasGroup); uIManager.ShowTargetGroups(); }
        if (skillType==1) { uIManager.ShowList(currentCanvasGroup, nextCanvasGroup); uIManager.ShowTargetGroups(); }
        uIManager.title.text = "Target";
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
        scAllMusicManager.Normal(scAllMusicManager.MainScene_UISelectInto);
    }
}
