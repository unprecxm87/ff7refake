﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverDisplay : MonoBehaviour
{
    [SerializeField] private State playerState;
    [SerializeField] private Button continueButton;
    [SerializeField] private Button backButton;
    [SerializeField] private SceneController sceneController;
    [SerializeField] private CanvasGroup fadeBlack;
    private CanvasGroup canvas;
    
    
    private void Awake()
    {
        playerState.onDead.AddListener(() => DisplayGameOver());
        continueButton.onClick.AddListener(() => StartCoroutine(Reload()));
        backButton.onClick.AddListener(() => SceneManager.LoadScene("MainMenu"));
        canvas = GetComponent<CanvasGroup>();
    }

    private IEnumerator Reload()
    {
        fadeBlack.DOFade(1, 0.1f);
        canvas.DOFade(0, 0.01f).SetUpdate(true);
        canvas.interactable = false;
        while (true)
        {
            if (fadeBlack.alpha > 0.999f)
            {
                sceneController.onLoad.Invoke();
                break;
            }
            yield return 0;
        }
        yield return new WaitForSeconds(1);
        fadeBlack.DOFade(0, 0.1f);
        
    }

    private void DisplayGameOver()
    {
        EventSystem.current.SetSelectedGameObject(null);
        canvas.DOFade(1, 0.01f).SetUpdate(true);
        canvas.interactable = true;
        EventSystem.current.SetSelectedGameObject(canvas.transform.GetChild(3).gameObject);
    }
}
