﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AIM : MonoBehaviour
{
    [SerializeField] CharacterSelector characterSelector;
    [SerializeField] private Vector2 offset;
    public RectTransform AimIcon;
    public CanvasGroup AimCanvas;
    public RectTransform UICanvas;
    private TargetSelector currentTargetSelector;


    private void Awake()
    {
        characterSelector.onChangeCharacter.AddListener(() => ChangeTargetSelector());
        currentTargetSelector = characterSelector.Characters[0].GetComponent<TargetSelector>();
    }

    private void ChangeTargetSelector()
    {
        currentTargetSelector = characterSelector.CurrentCharacter.GetComponent<TargetSelector>();
    }

    void Update()
    {
        //if (targetSelector.IsLockTarget)
        //{
        //    AimCanvas.DOFade(1, 0.1f).SetUpdate(true);
        //    Vector2 TargetPos = Camera.main.WorldToScreenPoint(targetSelector.LockTarget.transform.position);
        //    Vector2 CamTargetPos = new Vector2();
        //    RectTransformUtility.ScreenPointToLocalPointInRectangle(UIcanvas.transform as RectTransform, TargetPos, Camera.main, out CamTargetPos);
        //    AimIcon.anchoredPosition = CamTargetPos;
        //}
        //else
        //{
        //    AimCanvas.DOFade(0, 0.1f).SetUpdate(true);
        //}
        //if (targetSelector.IsSelectTarget)
        //{
        //AimCanvas.DOFade(1, 0.1f).SetUpdate(true);
        ////Vector2 TargetPos = Camera.main.WorldToScreenPoint(targetSelector.SelectTarget.transform.position);
        //Vector3 TargetPos = Camera.main.WorldToScreenPoint(obj.transform.position);
        //Vector2 CamTargetPos;
        //RectTransformUtility.ScreenPointToLocalPointInRectangle(UICanvas, TargetPos, Camera.main, out CamTargetPos);
        ////RectTransformUtility.ScreenPointToLocalPointInRectangle(UICanvas.transform as RectTransform, TargetPos, Camera.main, out CamTargetPos);
        //Debug.Log("screen: " + TargetPos);
        //Debug.Log("rect: " + CamTargetPos);
        //AimIcon.anchoredPosition = CamTargetPos;
        //}
        //else
        //{
        //    AimCanvas.DOFade(0, 0.1f).SetUpdate(true);
        //}

        //if (targetSelector.IsLockTarget)
        //{
        //    SetAimPosition(targetSelector.LockTarget.transform.position);
        //}
        //else if (!targetSelector.IsLockTarget && !targetSelector.IsSelectTarget) AimCanvas.DOFade(0, 0.1f).SetUpdate(true);
        //if (targetSelector.IsSelectTarget)
        //{
        //    SetAimPosition(targetSelector.SelectTarget.transform.position);
        //}
        //else if (!targetSelector.IsLockTarget && !targetSelector.IsSelectTarget) AimCanvas.DOFade(0, 0.1f).SetUpdate(true);
        
        if (currentTargetSelector.IsLockTarget || currentTargetSelector.IsSelectTarget)
        {
            if (currentTargetSelector.IsLockTarget) SetAimPosition(currentTargetSelector.LockTarget.transform);
            if (currentTargetSelector.IsSelectTarget) SetAimPosition(currentTargetSelector.SelectTarget.transform);
        }
        else AimCanvas.DOFade(0, 0.1f).SetUpdate(true);
    }

    private void SetAimPosition(Transform target)
    {
        AimCanvas.DOFade(1, 0.1f).SetUpdate(true);
        Vector2 viewportBottom = Camera.main.WorldToViewportPoint(target.position);
        Vector2 viewportTop = Camera.main.WorldToViewportPoint(target.GetChild(0).position);
        Vector2 viewportCenter = (viewportBottom + viewportTop) / 2;
        Vector2 CamTargetPos = new Vector2((viewportCenter.x * UICanvas.sizeDelta.x) - (UICanvas.sizeDelta.x * 0.5f),
            (viewportCenter.y * UICanvas.sizeDelta.y) - (UICanvas.sizeDelta.y * 0.5f));
        AimIcon.anchoredPosition = CamTargetPos + offset;
    }
}
