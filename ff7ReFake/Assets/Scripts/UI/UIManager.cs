﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Rendering;

public class UIManager : MonoBehaviour
{
    public Dictionary<GameObject, GameObject> enemy = new Dictionary<GameObject, GameObject>();
    #region Data
    [SerializeField] private TargetSelector targetSelector;
    [SerializeField] private State state;
    [SerializeField] private CharacterSelector characterSelector;
    [SerializeField] private Vector2 targetPosition;
    [SerializeField] private Vector2 originPosition;
    public CanvasGroup WaitMode;
    public CanvasGroup CommandGroups;
    public CanvasGroup Attack;
    public CanvasGroup AbilityList;
    public CanvasGroup MagicList;

    public CanvasGroup TargetGroup;
    public CanvasGroup Aim;
    public CanvasGroup ATBimageleft;
    public CanvasGroup ATBimageright;
    public Slider KennyATBSlider;
    public Slider KennyHPSlider;
    public Slider KennyMPSlider;
    public Slider KennyEXPSlider;
    public RectTransform WaitModePos;
    public RectTransform AttackPos;
    public Transform commandgroups;
    public Transform targetgroups;
    public Transform abilitygroups;
    public Transform magicgroups;
    private State kennyState;
    public TextMeshProUGUI title;

    #endregion
    void Start()
    {
        kennyState = characterSelector.Characters[0].GetComponent<State>();
        kennyState.onModifyHP.AddListener((value) => UpdateHealthBar(value, KennyHPSlider));
        kennyState.onModifyMP.AddListener((value) => UpdateMpBar(value, KennyMPSlider));
        kennyState.onModifyATB.AddListener((value) => UpdateATB(value, KennyATBSlider));
        kennyState.onLevelUpHP.AddListener((value) => FillHealthBar(value, KennyHPSlider));
        kennyState.onLevelUpMP.AddListener((value) => FillMpBar(value, KennyMPSlider));
        kennyState.onModifyEXPUIM.AddListener((value => UpdateHealthBar(value, KennyEXPSlider)));
        kennyState.onLevleUpEXPUIM.AddListener((value) => ClearEXPBar(value, KennyEXPSlider));
        kennyState.onDead.AddListener(() =>
        {
            AllCanvasGroupResetWhenDead();
            CancelCommand();
        });
        KennyHPSlider.maxValue = kennyState.MaxHP;
        KennyHPSlider.value = KennyHPSlider.maxValue;
        KennyMPSlider.maxValue = kennyState.MaxMP;
        KennyMPSlider.value = KennyMPSlider.maxValue;
        KennyATBSlider.maxValue = kennyState.MaxATB;
        KennyATBSlider.value = KennyMPSlider.minValue;
        KennyEXPSlider.maxValue = kennyState.LevelUpEXP;
        KennyEXPSlider.value = KennyEXPSlider.minValue;
    }

    private void ClearEXPBar(float newValue, Slider player)
    {
        player.value = 0;
        player.maxValue = newValue;
        player.DOValue(player.value, 0.1f);
    }

    void Update()
    {
        if(TargetGroup.alpha == 1)
        {
            if (enemy.Count != 0) EventSystem.current.SetSelectedGameObject(enemy[targetSelector.SelectTarget]);
        }
    }

    /// <summary>
    /// ATB條累積
    /// </summary>
    /// <param name="value"></param>
    public void UpdateATB(float ATB, Slider player)
    {
        player.DOComplete();
        player.value += ATB;
        player.value = Mathf.Clamp(player.value, player.minValue, player.maxValue);
        player.DOValue(player.value, 0.15f);
        if (player.value >= 25 )
        {
            ATBimageleft.DOFade(1, 0.4f);
        }
        else
        {
            ATBimageleft.DOFade(0, 0.4f);
        }
        if (player.value == 50 )
        {
            ATBimageright.DOFade(1, 0.4f);
        }
        else
        {
            ATBimageright.DOFade(0, 0.4f);
        }
    }



    /// <summary>
    /// 血量
    /// </summary>
    /// <param name="value"></param>
    public void UpdateHealthBar(float Damage, Slider player)
    {
        player.DOComplete();
        player.value += Damage;
        player.value = Mathf.Clamp(player.value, player.minValue, player.maxValue);
        player.DOValue(player.value, 0.1f);
    }

    public void FillHealthBar(float newValue, Slider player)
    {
        player.DOComplete();
        player.maxValue = newValue;
        player.value = newValue;
        player.DOValue(player.value, 0.1f);
    }

    public void UpdateEXPBar(float Damage, Slider player)
    {
        player.DOComplete();
        player.value += Damage;
        player.value = Mathf.Clamp(player.value, player.minValue, player.maxValue);
        player.DOValue(player.value, 0.1f);
    }

    /// <summary>
    /// 魔力
    /// </summary>
    /// <param name="value"></param>
    public void UpdateMpBar(float usedMP, Slider player)
    {
        player.DOComplete();
        player.value += usedMP;
        player.value = Mathf.Clamp(player.value, player.minValue, player.maxValue);
        player.DOValue(player.value, 0.1f);
    }

    public void FillMpBar(float newValue, Slider player)
    {
        player.DOComplete();
        player.maxValue = newValue;
        player.value = newValue;
        player.DOValue(player.value, 0.1f);
    }

    /// <summary>
    /// 戰鬥ui浮現
    /// </summary>
    public void ShowTacticalMenu()
    {
        EventSystem.current.SetSelectedGameObject(null);

        WaitMode.DOFade(1, 0.4f).SetUpdate(true);
        WaitMode.interactable = true;
        WaitModePos.DOAnchorPos3D(new Vector3(257.9f, 291.8f, 0), 0.5f).SetUpdate(true);


        CommandGroups.DOFade(1, 0.4f).SetUpdate(true);
        CommandGroups.interactable = true;

        Attack.DOFade(0, 0.4f).SetUpdate(true);
        Attack.interactable = false;
        AttackPos.DOAnchorPos3D(new Vector3(-1200f, -388f, 0), 0.5f).SetUpdate(true);

        EventSystem.current.SetSelectedGameObject(WaitMode.transform.GetChild(0).GetChild(0).gameObject);
    }

    /// <summary>
    /// 關閉戰鬥ui
    /// </summary>
    public void ResetToFirstGroup()
    {
        EventSystem.current.SetSelectedGameObject(null);
        WaitModePos.DOAnchorPos3D(new Vector3(-200f, 291.8f, 0), 0.5f).SetUpdate(true);
        WaitMode.DOFade(0, 0.4f);

        CommandGroups.DOFade(0, 0.4f).SetUpdate(true);
        CommandGroups.interactable = false;

        Attack.DOFade(1, 0.4f);
        Attack.interactable = true;
        AttackPos.DOAnchorPos3D(new Vector3(-625f, -388f, 0), 0.5f).SetUpdate(true);

        EventSystem.current.SetSelectedGameObject(Attack.transform.GetChild(0).gameObject);
    }

    /// <summary>
    /// 控制ui上下層
    /// </summary>
    /// <param name="currentCanvasGroup"></param>
    /// <param name="nextCanvasGroup"></param>
    public void ShowList (CanvasGroup currentCanvasGroup, CanvasGroup nextCanvasGroup)
    {
        EventSystem.current.SetSelectedGameObject(null);
        currentCanvasGroup.DOFade(0, 0.4f).SetUpdate(true);
        currentCanvasGroup.interactable = false;

        nextCanvasGroup.DOFade(1, 0.4f).SetUpdate(true);
        nextCanvasGroup.interactable = true;
        EventSystem.current.SetSelectedGameObject(nextCanvasGroup.transform.GetChild(0).gameObject);
    }

    /// <summary>
    /// 生成敵人列表
    /// </summary>
    /// <param name="click"></param>
    public void ShowTargetGroups()
    {
        EventSystem.current.SetSelectedGameObject(null);
        for (int i = 0; i < targetgroups.childCount; i++)
        {
            if (targetSelector.CurrentTargetList.Count - 1 >= i)
            {
                targetgroups.GetChild(i).GetComponent<CanvasGroup>().DOFade(1, 0.4f).SetUpdate(true);
                targetgroups.GetChild(i).GetComponent<CanvasGroup>().interactable = true;
                targetgroups.GetChild(i).GetComponentInChildren<TextMeshProUGUI>().text = targetSelector.CurrentTargetList[i].name;
                enemy.Add(targetSelector.CurrentTargetList[i], targetgroups.GetChild(i).gameObject);
            }
            else
            {
                targetgroups.GetChild(i).GetComponentInChildren<TextMeshProUGUI>().text = "";
                targetgroups.GetChild(i).GetComponent<CanvasGroup>().alpha = 0;
                targetgroups.GetChild(i).GetComponent<CanvasGroup>().interactable = false;
            }
        }
    }

    /// <summary>
    /// 技能onclick之後回到最初介面
    /// </summary>
    public void AllCanvasGroupReset()
    {
        TargetGroup.DOFade(0, 0.4f).SetUpdate(true);
        TargetGroup.interactable = false;
        //AbilityList.alpha = 0;
        //AbilityList.interactable = false;
        //MagicList.alpha = 0;
        //MagicList.interactable = false;
        //CommandGroups.alpha = 0;
        //CommandGroups.interactable = false;

        WaitMode.DOFade(0, 0.4f).SetUpdate(true);

        Attack.DOFade(1,0.4f).SetUpdate(true);
        Attack.interactable = true;
        AttackPos.DOAnchorPos3D(new Vector3(-625f, -388f, 0), 0.5f).SetUpdate(true);

        EventSystem.current.SetSelectedGameObject(Attack.transform.GetChild(0).gameObject);

        for (int i = 0; i < targetgroups.childCount; i++)
        {
            targetgroups.GetChild(i).GetComponent<CanvasGroup>().DOFade(0, 0.4f).SetUpdate(true);
            targetgroups.GetChild(i).GetComponent<CanvasGroup>().interactable = false;
        }
        enemy.Clear();
    }

    public void AllCanvasGroupResetWhenDead()
    {
        TargetGroup.DOFade(0, 0.4f).SetUpdate(true);
        TargetGroup.interactable = false;
        AbilityList.alpha = 0;
        AbilityList.interactable = false;
        MagicList.alpha = 0;
        MagicList.interactable = false;
        CommandGroups.alpha = 0;
        CommandGroups.interactable = false;
        WaitMode.DOFade(0, 0.4f).SetUpdate(true);
        Attack.DOFade(1, 0.4f).SetUpdate(true);
        Attack.interactable = true;

        for (int i = 0; i < targetgroups.childCount; i++)
        {
            targetgroups.GetChild(i).GetComponent<CanvasGroup>().DOFade(0, 0.4f).SetUpdate(true);
            targetgroups.GetChild(i).GetComponent<CanvasGroup>().interactable = false;
        }
        enemy.Clear();
    }

    /// <summary>
    /// taticalmode觸發
    /// </summary>
    public void EnterCommand()
    {
        Time.timeScale = 0.01f;
        //Time.timeScale = 0.5f;
        ShowTacticalMenu();
        GameSystem.IsCommand = true;
    }

    /// <summary>
    /// 關閉tatocalmode
    /// </summary>
    public void CancelCommand()
    {
        Time.timeScale = 1.0f;
        GameSystem.IsCommand = false;
    }
}
