﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GetAbilityDisplay : MonoBehaviour
{
    [SerializeField] private SceneController sceneController;
    [SerializeField] private TextMeshProUGUI text;
    private CanvasGroup canvas;
    private RectTransform rectTransform;
    [SerializeField] private Vector2 targetPosition;
    [SerializeField] private Vector2 originPosition;
    

    private void Awake()
    {
        sceneController.onArriveCheckPoint.AddListener(() =>
        {
            rectTransform.anchoredPosition = originPosition;
            StartCoroutine(MoveDisplay());
            StartCoroutine(DisplayAbility());
        });


        canvas = GetComponent<CanvasGroup>();
        rectTransform = GetComponent<RectTransform>();
    }



    private IEnumerator MoveDisplay()
    {
        while ((rectTransform.anchoredPosition - targetPosition).sqrMagnitude > 0.001f)
        {
            rectTransform.anchoredPosition = Vector3.Lerp(rectTransform.anchoredPosition, targetPosition, 0.1f);
            yield return 0;
        }

    }

    private IEnumerator DisplayAbility()
    {
        canvas.DOFade(1, 0.1f);
        text.text = "Ability Get!";
        yield return new WaitForSeconds(2);
        canvas.DOFade(0, 0.1f);
    }
}
