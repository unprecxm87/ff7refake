﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BossHealthBar : MonoBehaviour
{
    [SerializeField] private MasterFSM efsm;
    [SerializeField] private BossFSM efsm2;
    public Slider BigGuyBar;
    [SerializeField] private Slider ShieldBar;

    public Text BigGuyName;
    public CanvasGroup BossHealthSystemEnable;
    public CanvasGroup BossShieldBarEnable;
    private List<string> BossName;
    [SerializeField] EnemyGenerator enemyGenerator1;
    [SerializeField] EnemyGenerator enemyGenerator2;
    [SerializeField] EnemyPool enemyPool;
    [SerializeField] State playerSate;
    private bool isAddElite;
    private bool isAddBoss;

    void Start()
    {
        BossName =new List<string>() { "幻影堡壘", "衝鋒鋼球王" };
        enemyGenerator1.onSetEnemies.AddListener(() => EliteCombat(0));
        enemyGenerator2.onSetEnemies.AddListener(() => BossCombat(1));
        playerSate.onDead.AddListener(() => HideLifeBar());
    }



    void Update()
    {
        
    }
    public void BigGuyTakeDamage(float damage)
    {
        BigGuyBar.DOComplete();
        BigGuyBar.value -= damage;
        BigGuyBar.value = Mathf.Clamp(BigGuyBar.value, BigGuyBar.minValue, BigGuyBar.maxValue);
        BigGuyBar.DOValue(BigGuyBar.value, 0.3f);
        if (BigGuyBar.value <= 0)
        {
            BossHealthSystemEnable.DOFade(0, 0.3f);
        }
    }
    public void ShieldTakeDamage(float damage)
    {
        ShieldBar.DOComplete();
        //Debug.Log("ShieldBar.value1: " + ShieldBar.value);
        ShieldBar.value -= damage;
        //Debug.Log("ShieldBar.value2: " + ShieldBar.value);
        ShieldBar.value = Mathf.Clamp(ShieldBar.value, ShieldBar.minValue, ShieldBar.maxValue);
        //Debug.Log("damage: " + damage);
        //Debug.Log("ShieldBar.value3: " + ShieldBar.value);
        ShieldBar.DOValue(ShieldBar.value, 0.3f);
    }

    public void ShieldFill()
    {
        ShieldBar.DOComplete();
        ShieldBar.value = 200;
        Debug.Log("ShieldBar.value: " + ShieldBar.value);
        //ShieldBar.value = Mathf.Clamp(ShieldBar.value, ShieldBar.minValue, ShieldBar.maxValue);
        ShieldBar.DOValue(ShieldBar.value, 0.3f);
    }
    void EliteCombat(int order)
    {
        Debug.Log("elite");
        efsm = GameObject.Find("幻影堡壘").GetComponent<MasterFSM>();
        BossHealthSystemEnable.DOFade(1, 0.3f);
        BigGuyName.text = BossName[order];
        if (!isAddElite)
        {
            efsm.onUV.AddListener((damage) => BigGuyTakeDamage(damage));
            BigGuyBar.maxValue = efsm.mData.f_Hp;
            isAddElite = true;
        }
        BigGuyBar.value = BigGuyBar.maxValue;
    }
    void BossCombat(int order)
    {
        efsm2 = GameObject.Find("衝鋒鋼球王").GetComponent<BossFSM>();
        BossHealthSystemEnable.DOFade(1, 0.3f);
        BossShieldBarEnable.DOFade(1, 0.3f);
        BigGuyName.text = BossName[order];
        if(!isAddBoss)
        {
            efsm2.onUV.AddListener((damage) => BigGuyTakeDamage(damage));
            efsm2.VEForShield.AddListener((damage) => ShieldTakeDamage(damage));
            efsm2.VEFill.AddListener(() => ShieldFill());
            BigGuyBar.maxValue = efsm2.mData.f_Hp;
            ShieldBar.maxValue = efsm2.mData.f_ShieldHP;
            isAddBoss = true;
        }
        BigGuyBar.value = BigGuyBar.maxValue;
        ShieldBar.value = 0; //ShieldBar.maxValue;
    }

    private void HideLifeBar()
    {
        BossHealthSystemEnable.DOFade(0, 0.3f);
        BossShieldBarEnable.DOFade(0, 0.3f);
    }
}
