﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemyhealthbar : MonoBehaviour
{
    [SerializeField] private EnemyFSM01 efsm;
    public Slider EnemyHealth;
    public Transform MainCam;
    public Canvas HealthBar;

    void Start()
    {
        efsm.onUV.AddListener(   (damage) => EnemyTakeDamage(damage)   );
        EnemyHealth.maxValue = efsm.mData.f_Hp;
        EnemyHealth.value = efsm.mData.f_Hp;
        HealthBar.enabled = true;
    }

    private void OnEnable()
    {
        EnemyHealth.maxValue = efsm.mData.f_Hp;
        EnemyHealth.value = efsm.mData.f_Hp;
    }

    private void LateUpdate()
    {
        transform.LookAt(transform.position + MainCam.forward);
    }
    public void EnemyTakeDamage(float damage)
    {
        EnemyHealth.DOComplete();
        EnemyHealth.value -= damage;
        EnemyHealth.value = Mathf.Clamp(EnemyHealth.value, EnemyHealth.minValue, EnemyHealth.maxValue);
        EnemyHealth.DOValue(EnemyHealth.value, 0.1f);
        if(EnemyHealth.value <= 1f)
        {
            gameObject.SetActive(false);
        }
    }
}
