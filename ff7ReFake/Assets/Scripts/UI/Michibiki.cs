﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Michibiki : MonoBehaviour
{
    public GameObject michibiki;
    public GameObject Target;
    public ParticleSystem michibikitrail;
    public float Value;
    [SerializeField] private float speed;

    public void StarMichibiki(Transform michibikistart,Transform goal)
    {
        //michibikitrail.Play();
        //Vector3 center = (goal.position + michibikistart.position) / 10;
        //center -= new Vector3(-Value, Value, 0);
        //Vector3 start = michibikistart.position - center;
        //Vector3 end = goal.position - center;
        //michibikistart.position = Vector3.Slerp(start, end, speed);
        michibikistart.position = Vector3.Lerp(michibikistart.position, goal.position, speed);
        //michibikistart.position += center;
        float dis = Vector3.Distance(michibikistart.position, goal.position);
        if(dis<=1)
        {
            michibikitrail.Stop();
            //if (StorySystem.Progress[CheckPoint.ThirdEasy]) michibiki.SetActive(false);
        }
    }
}
