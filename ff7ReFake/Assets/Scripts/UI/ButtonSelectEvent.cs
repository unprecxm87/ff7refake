﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;
using DG.Tweening;

public class ButtonSelectEvent : MonoBehaviour, ISubmitHandler, ISelectHandler, IDeselectHandler
{
    public RectTransform OnSelectButton;
    public GameObject objAllMusicManager;
    AllMusicManager scAllMusicManager;
    //private Vector3 pos;
    //void Start()
    //{
    //    pos = transform.position;
    //    StartCoroutine(WaitForFrames());
    //}

    //IEnumerator WaitForFrames()
    //{
    //    yield return new WaitForSecondsRealtime(0.5f);
    //    pos = transform.position;
    //}
    public void OnDeselect(BaseEventData eventData)
    {
        OnSelectButton.DOAnchorPos3DX(156.6f, 0.4f).SetUpdate(true);
    }

    public void OnSelect(BaseEventData eventData)
    {
        OnSelectButton.DOAnchorPos3DX(170.6f,0.4f).SetUpdate(true);
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
        scAllMusicManager.Normal(scAllMusicManager.MainScene_UISelect);
    }

    public void OnSubmit(BaseEventData eventData)
    {
        OnSelectButton.DOPunchPosition(Vector3.right, 0.2f, 10, 1).SetUpdate(true);
    }
}
