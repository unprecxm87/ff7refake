﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadSceneManager : MonoBehaviour
{
    public CanvasGroup MainMenu;
    private static LoadSceneManager MyInstance;
    public static LoadSceneManager Instance() { return MyInstance; }
    public System.Action<float> MyLoadingProgress;
    public GameObject objAllMusicManager;
    AllMusicManager scAllMusicManager;
    private void Awake()
    {
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
        MyInstance = this;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(MainMenu.transform.GetChild(0).gameObject);
    }

   //public void NewGameOnClick()
   // {
   //     SceneManager.LoadScene("MainScene");
   // }

    public IEnumerator NewGameOnClick(int sceneindex,IEnumerator customloading)
    {
        scAllMusicManager.Normal(scAllMusicManager.MainMenu_Select);
        yield return new WaitForSeconds(0.1f);
        AsyncOperation ao = SceneManager.LoadSceneAsync(sceneindex);
        if(ao == null)
        {
            yield break;
        }
        float FPrograss = 0.0f;
        MyLoadingProgress(FPrograss);
        ao.allowSceneActivation = false;
        while(ao.isDone == false)
        {
            FPrograss += 0.01f;
            MyLoadingProgress(FPrograss);
            if(ao.progress>0.89f && FPrograss > 0.3f)
            {
                break;
            }
            yield return 0;
        }
        yield return customloading;
        ao.allowSceneActivation = true;
    }
}
