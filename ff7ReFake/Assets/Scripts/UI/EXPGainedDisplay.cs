﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EXPGainedDisplay : MonoBehaviour
{
    [SerializeField] private State playerState;
    [SerializeField] private TextMeshProUGUI text;
    private CanvasGroup canvas;
    private RectTransform rectTransform;
    [SerializeField] private Vector2 targetPosition;
    [SerializeField] private Vector2 originPosition;

    private void Awake()
    {
        playerState.onModifyEXP.AddListener((value) =>
        {
            rectTransform.anchoredPosition = originPosition;
            StartCoroutine(MoveDisplay());
            StartCoroutine(DisplayEXP(value));
        });
        playerState.onLevelUp.AddListener(() =>
        {
            rectTransform.anchoredPosition = originPosition;
            StartCoroutine(MoveDisplay());
            StartCoroutine(DisplayLevelUp());
        });

        canvas = GetComponent<CanvasGroup>();
        rectTransform = GetComponent<RectTransform>();
    }



    private IEnumerator MoveDisplay()
    {
        while ((rectTransform.anchoredPosition - targetPosition).sqrMagnitude > 0.001f)
        {
            rectTransform.anchoredPosition = Vector3.Lerp(rectTransform.anchoredPosition, targetPosition, 0.1f);
            yield return 0;
        }

    }

    private IEnumerator DisplayEXP(float value)
    {
        canvas.DOFade(1, 0.1f);
        text.text = Convert.ToInt32(value) + " EXP Gained";
        yield return new WaitForSeconds(2);
        canvas.DOFade(0, 0.1f);
    }

    private IEnumerator DisplayLevelUp()
    {
        canvas.DOFade(1, 0.1f);
        text.text = "Level Up!";
        yield return new WaitForSeconds(2);
        canvas.DOFade(0, 0.1f);
    }
}
