﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using TMPro;
using UnityEngine;

public class FloatingTextMain : MonoBehaviour
{
    public GameObject DamageText;
    public float DestroyTime = 3f;
    public Vector3 Offset = new Vector3(0, 2, 0);
    public Vector3 RandomIntensity = new Vector3(0.5f, 0, 0);
    void Start()
    {
        Destroy(gameObject, DestroyTime);
        transform.localPosition += Offset;
        transform.localPosition += new Vector3(Random.Range(-RandomIntensity.x, RandomIntensity.x),
        Random.Range(-RandomIntensity.y, RandomIntensity.y),
        Random.Range(-RandomIntensity.z, RandomIntensity.z));
    }
    public void ShowFloatingText()
    {
        var GetDamage = Instantiate(DamageText, transform.position, Quaternion.identity, transform);
        //GetDamage.GetComponent<TextMeshPro>().text = 
    }
}
