﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIDoMove : MonoBehaviour
{
    public RectTransform CurrentSkillPos;
    public CanvasGroup CurrentSkillAlpha;
    private Button button;
    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => Onclick());
    }
    private void Onclick()
    {
        CurrentSkillFadeIn(CurrentSkillPos, CurrentSkillAlpha);
    }
    public void CurrentSkillFadeIn(RectTransform currentskillpos,CanvasGroup currentskillalpha)
    {
            currentskillalpha.DOFade(1, 1.0f);
            currentskillpos.DOLocalMove(new Vector3(768.0f, 200.0f, 0f), 0.5f);
    }

}
