﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class buttonUI : MonoBehaviour, ICancelHandler
{
    [SerializeField] private UIManager uIManager;
    [SerializeField] private CanvasGroup currentCanvasGroup;
    [SerializeField] private CanvasGroup nextCanvasGroup;
    [SerializeField] private CanvasGroup previousCanvasGroup;
    private Button button;
    public GameObject objAllMusicManager;
    AllMusicManager scAllMusicManager;

    public void OnCancel(BaseEventData eventData)
    {
        if(previousCanvasGroup == uIManager.WaitMode)
        {
            uIManager.ResetToFirstGroup();
        }
        uIManager.CancelCommand();
    }

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => Onclick());
    }

    private void Onclick()
    {
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
        scAllMusicManager.Normal(scAllMusicManager.MainScene_UISelectInto);
        uIManager.ShowList(currentCanvasGroup, nextCanvasGroup);
        if(nextCanvasGroup == uIManager.AbilityList)
        {
            uIManager.title.text = "Ability";
        }
        else
        {
            uIManager.title.text = "Magic";
        }
    }
}
