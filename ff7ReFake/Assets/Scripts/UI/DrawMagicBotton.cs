﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawMagicBotton : MonoBehaviour
{
    public Image MagicValue;
    public CanvasGroup DrawMagic;
    public Transform MainCam;

    private void LateUpdate()
    {
        transform.LookAt(transform.position + MainCam.forward);
    }
    public void OnDrawMagic(float drawmagictime)
    {
        MagicValue.fillAmount += drawmagictime;
    }

}
