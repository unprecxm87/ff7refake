﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonOnLaunch : MonoBehaviour, ICancelHandler
{
    [SerializeField] private CharacterSelector characterSelector;
    [SerializeField] private UIManager uIManager;
    [SerializeField] private CanvasGroup currentCanvasGroup;
    private Button button;
    private SkillLauncher skillLauncher;
    private TargetSelector targetSelector;
    public GameObject objAllMusicManager;
    AllMusicManager scAllMusicManager;

    public void OnCancel(BaseEventData eventData)
    {
        skillLauncher = characterSelector.CurrentCharacter.GetComponent<SkillLauncher>();
        targetSelector = characterSelector.CurrentCharacter.GetComponent<TargetSelector>();
        targetSelector.UnSelect();
        if (skillLauncher.SkillType == 0)
        {
            uIManager.ShowList(currentCanvasGroup, uIManager.AbilityList);
            uIManager.enemy.Clear();
            uIManager.title.text = "Ability";
        }
        if (skillLauncher.SkillType == 1)
        {
            uIManager.ShowList(currentCanvasGroup, uIManager.MagicList);
            uIManager.enemy.Clear();
            uIManager.title.text = "Magic";
        }
    }

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => Onclick());
    }

    private void Onclick()
    {
        skillLauncher = characterSelector.CurrentCharacter.GetComponent<SkillLauncher>();
        targetSelector = characterSelector.CurrentCharacter.GetComponent<TargetSelector>();
        skillLauncher.LaunchTarget = targetSelector.SelectTarget;
        skillLauncher.StartAction();
        targetSelector.UnSelect();
        
        uIManager.AllCanvasGroupReset();
        uIManager.CancelCommand();
        uIManager.title.text = "command";
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
        scAllMusicManager.Normal(scAllMusicManager.MainScene_UISelectInto);
    }
}
