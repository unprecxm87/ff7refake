﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class LoadingSceneControler : MonoBehaviour
{
    public CanvasGroup MainMenuScene;
    public CanvasGroup LoadingScene;
    public Slider LoadingBar;
    private Button button;
    void Start()
    {
        LoadSceneManager.Instance().MyLoadingProgress = UpdateProgress;
        GameSystem.IsCombat = false;
        StorySystem.Progress[CheckPoint.Boss] = false;
    }

    void Update()
    {

    }
    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => Onclick());
    }
    private void Onclick()
    {
        MainMenuScene.DOFade(0, 0.2f);
        MainMenuScene.interactable = false;
        MainMenuScene.blocksRaycasts = false;
        LoadingScene.DOFade(1, 0.2f);
        StartCoroutine(LoadSceneManager.Instance().NewGameOnClick(1, DoLoadingScene()));
    }
    public void UpdateProgress(float fProgress)
    {
        LoadingBar.value = fProgress;
    }
    IEnumerator DoLoadingScene()
    {
        float FProgress = 0.3f;
        UpdateProgress(FProgress);
        yield return new WaitForSeconds(1.0f);
        FProgress = 0.5f;
        UpdateProgress(FProgress);
        yield return new WaitForSeconds(1.0f);
        FProgress = 0.7f;
        UpdateProgress(FProgress);
        yield return new WaitForSeconds(1.0f);
        FProgress = 0.9f;
        UpdateProgress(FProgress);
    }
}
