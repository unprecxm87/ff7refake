﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheildGetHit : MonoBehaviour
{
    public GameObject SheildGotHitEffect;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            var sheildgethit = Instantiate(SheildGotHitEffect, transform) as GameObject;
            Debug.Log("d1");
            Destroy(sheildgethit , 2.0f);
            Debug.Log("d2");
        }
    }

}
