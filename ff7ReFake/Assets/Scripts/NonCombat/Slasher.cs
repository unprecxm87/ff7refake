﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slasher : MonoBehaviour, IAction
{
    private Animator animator;

    public int Priority { get; private set; }

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void CancelAction()
    {

    }

    public void StartAction()
    {
        animator.SetTrigger("Attack");
    }
}
