﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastWeapon : MonoBehaviour
{
    public bool isFiring = false;
    public ParticleSystem[] muzzleFlash;
    public ParticleSystem[] muzzleFlashForLaser;
    public ParticleSystem hitEffect;
    public ParticleSystem hitEffectForMasterGun;
    public ParticleSystem hitEffectForMasterLaser;
    public TrailRenderer tracerEffect;
    public TrailRenderer tracerEffectForMasterGun;
    public TrailRenderer tracerEffectForMasterLaser;
    public Transform raycastOrigin;
    public Transform raycastOriginForMasterGun;
    public Transform raycastOriginForMasterLaser;

    public Transform raycastDestination;
    public Transform tmp_CHT_Master;
    public Transform raycastDestinationForMasterLaser;

    public GameObject objAllMusicManager;
    AllMusicManager scAllMusicManager;



    Ray ray;
    RaycastHit hitInfo;

    public void StartFiring()
    {
        isFiring = true;
        foreach(var particle in muzzleFlash)
        {
            particle.Emit(1);
        }
        ray.origin = raycastOrigin.position;
        ray.direction = raycastDestination.position - raycastOrigin.position;
        var tracer = Instantiate(tracerEffect, ray.origin, Quaternion.identity);
        tracer.AddPosition(ray.origin);
        if (Physics.Raycast(ray, out hitInfo))
        {
            hitEffect.transform.position = hitInfo.point;
            hitEffect.transform.forward = hitInfo.normal;
            hitEffect.Emit(1);

            tracer.transform.position = hitInfo.point;
            if(hitInfo.collider.gameObject.CompareTag("Player"))
            {
                Fighter Ft = hitInfo.collider.gameObject.GetComponent<Fighter>();
                Ft.BeHit(1);
            }
        }
    }
    public void StartFiringForMasterGun()
    {
        isFiring = true;
        foreach (var particle in muzzleFlash)
        {
            particle.Emit(1);
        }
        ray.origin = raycastOriginForMasterGun.position;
        ray.direction = tmp_CHT_Master.position - raycastOriginForMasterGun.position;
        var tracer = Instantiate(tracerEffectForMasterGun, ray.origin, Quaternion.identity);
        tracer.AddPosition(ray.origin);

        RaycastHit[] Hits;
        Hits = Physics.RaycastAll(ray.origin, ray.direction, 10000.0f);
        for (int i = 0; i < Hits.Length; i++)
        {
            RaycastHit hit = Hits[i];

            if (hit.collider.gameObject.CompareTag("Enemy")) return;
            if (Physics.Raycast(ray, out hitInfo))
            {
                hitEffectForMasterGun.transform.position = hitInfo.point;
                hitEffectForMasterGun.transform.forward = hitInfo.normal;
                hitEffectForMasterGun.Emit(1);
                scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
                scAllMusicManager.Normal(scAllMusicManager.MainScene_MasterShot);
                tracer.transform.position = hitInfo.point;
                if (hitInfo.collider.gameObject.CompareTag("Player"))
                {
                    Fighter Ft = hitInfo.collider.gameObject.GetComponent<Fighter>();
                    Ft.BeHit(1);
                }
            }
        }

            
    }
    public void StartLaser()
    {
        isFiring = true;
        foreach (var particle in muzzleFlashForLaser)
        {
            particle.Emit(1);
        }

        ray.origin = raycastOriginForMasterLaser.position;
        ray.direction = raycastDestinationForMasterLaser.position - raycastOriginForMasterLaser.position;
        var tracer = Instantiate(tracerEffectForMasterLaser, ray.origin, Quaternion.identity);
        tracer.AddPosition(ray.origin);
        //tracer.SetPosition(0, ray.origin);
        RaycastHit[] Hits;
        Hits = Physics.RaycastAll(ray.origin, ray.direction, 10000.0f);
        for (int i = 0; i < Hits.Length; i++)
        {
            RaycastHit hit = Hits[i];

            if (hit.collider.gameObject.CompareTag("Enemy")) return;

            if (Physics.Raycast(ray, out hitInfo))
            {
                hitEffectForMasterLaser.transform.position = hitInfo.point;
                hitEffectForMasterLaser.transform.forward = hitInfo.normal;
                hitEffectForMasterLaser.Emit(1);
                scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
                scAllMusicManager.Normal(scAllMusicManager.MainScene_MasterLaserBomb);
                ray.direction.Normalize();
                tracer.transform.position = hitInfo.point + ray.direction*3;
                if (hitInfo.collider.gameObject.CompareTag("Player"))
                {
                    Fighter Ft = hitInfo.collider.gameObject.GetComponent<Fighter>();
                    Ft.BeHit(100);
                }
            }
        }
    }
    public void StopFiring()
    {
        isFiring = false;
    }
}
