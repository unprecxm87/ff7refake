﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using TMPro;
//using UnityEditor.Rendering;
using UnityEngine;

public class MasterFSM : EnemyDataBase
{
    public GameObject RAttackTurnPart;
    public GameObject MasterGun;
    public GameObject MasterLaser;
    public float f_TurnAroundToTargetTime;
    public GameObject tmp_CHT_Master;
    public float TurnMoveToTarget_TurnSpeed;
    public float TurnMoveToTarget_MoveSpeed;
    public GameObject father_ChangePoints;
    public GameObject choose_ChangePoints;
    public GameObject CrossHairTarget_Master;
    public Rigidbody rg;
    public Collider Body;
    public ParticleSystem PS;
    public ParticleSystem PS2;
    public ParticleSystem PS3;
    public ParticleSystem PS4;
    public ParticleSystem PS5;
    public GameObject TargetForPS3;




    Animator Anim_MasterGun;
    Animator Anim_MasterLaser;
    AnimatorStateInfo MainCurrentBaseStage;
    AnimatorStateInfo LaserCurrentBaseStage;
    int i_idleFrequency;
    int i_shootingFrequency;
    bool b_isIdle;
    bool b_isTurnCurrect;
    bool b_isOnPoint;
    int oriPoint;
    bool b_isCheckPoint;
    bool b_ishaveTmpPoint;
    int i_tmp_ChooseTurnAroundToTarget;
    int i_tmp_ChooseChangePoint;
    int i_Shoots;
    int i_tmp_Shoots;
    bool b_CheckShootsNumBer;
    bool b_OnceShooted;
    bool b_OnDrop;
    float f_TragetToDrop;
    TargetSelector targetSelector;


    private void OnEnable()
    {
        CurrentEnemyTarget = null;
        b_isIdle = false;
        f_CurrentTime = 0.0f;
        i_idleFrequency = 0;
        i_shootingFrequency = 0;
        CurrentState = eFSMState.UnFight;
        b_BeHit = false;
        b_CheckShootsNumBer = false;
        i_tmp_Shoots = 0;
        mData.f_Hp = MaxHP;
        DissolveAmount = 1;
    }

    private void Start()
    {
        Anim = GetComponent<Animator>();
        Anim_MasterGun = MasterGun.GetComponent<Animator>();
        Anim_MasterLaser = MasterLaser.GetComponent<Animator>();
        targetSelector = Ft.gameObject.GetComponent<TargetSelector>();
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
        //CurrentEnemyTarget = null;
        //b_isIdle = false;
        //f_CurrentTime = 0.0f;
        //i_idleFrequency = 0;
        //i_shootingFrequency = 0;
        //CurrentState = eFSMState.UnFight;
        //b_BeHit = false;
        //b_CheckShootsNumBer = false;
        //i_tmp_Shoots = 0;
    }
    void Update()
    {
        //Debug.Log("Current State " + CurrentState);
        if (CurrentState == eFSMState.Idle)
        {
            CheckTarget();
            Func_BeHit();
            Anim.SetBool("Move", false);
            if (b_isIdle != true)
            {
                f_IdleTime = UnityEngine.Random.Range(1, 3);
                b_isIdle = true;
            }
            if (f_CurrentTime >= f_IdleTime)
            {
                i_idleFrequency += 1;
                if (i_idleFrequency == 5)
                {
                    CurrentState = eFSMState.StartToShoot;
                    i_tmp_Shoots = 0;
                    b_CheckShootsNumBer = false;
                    b_ishaveTmpPoint = false;
                    f_CurrentTime = 0.0f;
                    b_isTurnCurrect = false;
                    i_idleFrequency = 0;
                    return;
                }
                i_tmp_Choose = UnityEngine.Random.Range(0, 7);
                if (i_tmp_Choose == 0 || i_tmp_Choose == 1)
                {
                    CurrentState = eFSMState.DropToPoint;
                    StartCoroutine(Disslove(1));
                    b_isCheckPoint = false;
                    b_isOnPoint = false;
                    b_OnDrop = false;
                    f_TragetToDrop = 0.0f;
                    f_CurrentTime = 0.0f;
                    return;
                }
                else
                {
                    CurrentState = eFSMState.TurnAroundToTarget;
                    f_CurrentTime = 0.0f;
                    b_isTurnCurrect = false;
                    return;
                }
            }
            else
            {
                f_CurrentTime += Time.deltaTime;
                return;
            }
        }
        else if (CurrentState == eFSMState.TurnAroundToTarget)
        {
            CheckTarget();
            Func_BeHit();
            TurnToTarget(ref b_isTurnCurrect, mData.TargetObject);
            if (b_isTurnCurrect)
            {
                i_tmp_ChooseTurnAroundToTarget = Random.Range(0, 3);
                if (i_tmp_ChooseTurnAroundToTarget == 0 || i_tmp_ChooseTurnAroundToTarget == 1)
                {
                    CurrentState = eFSMState.StartToShoot;
                    i_tmp_Shoots = 0;
                    b_CheckShootsNumBer = false;
                    b_ishaveTmpPoint = false;
                    f_CurrentTime = 0.0f;
                    b_isTurnCurrect = false;
                    i_idleFrequency = 0;
                    return;
                }
                else
                {
                    CurrentState = eFSMState.Idle;
                    f_CurrentTime = 0.0f;
                    b_isIdle = false;
                    return;
                }
            }
            else
            {
                return;
            }
        }
        //else if (CurrentState == eFSMState.ChangePoint)
        //{
        //    CheckTarget();
        //    Func_BeHit();
        //    if (b_isCheckPoint != true)
        //    {
        //        do
        //        {
        //            i_tmp_ChooseChangePoint = UnityEngine.Random.Range(0, 6);
        //        }
        //        while (i_tmp_ChooseChangePoint == oriPoint);
        //        b_isCheckPoint = true;
        //    }
        //    choose_ChangePoints = father_ChangePoints.transform.GetChild(i_tmp_ChooseChangePoint).gameObject;
        //    TurnMoveToTarget(choose_ChangePoints);
        //    TurnToTargetForShooting(ref b_isTurnCurrect, mData.TargetObject);
        //    Anim.SetBool("Move", true);
        //    CheckOnCurrectPoint(choose_ChangePoints, ref b_isOnPoint);
        //    if (b_isOnPoint)
        //    {
        //        oriPoint = i_tmp_ChooseChangePoint;
        //        i_tmp_Choose = Random.Range(1, 5);
        //        if (i_tmp_ChooseChangePoint == 5)
        //        {
        //            CurrentState = eFSMState.StartToShoot;
        //            i_tmp_Shoots = 0;
        //            b_CheckShootsNumBer = false;
        //            b_ishaveTmpPoint = false;
        //            f_CurrentTime = 0.0f;
        //            b_isTurnCurrect = false;
        //            i_idleFrequency = 0;
        //            return;
        //        }
        //        else
        //        {
        //            CurrentState = eFSMState.TurnAroundToTarget;
        //            f_CurrentTime = 0.0f;
        //            b_isTurnCurrect = false;
        //            b_isIdle = false;
        //            return;
        //        }
        //    }
        //}
        else if (CurrentState == eFSMState.DropToPoint)
        {
            CheckTarget();
            Func_BeHit();
            if (b_isCheckPoint != true)
            {
                do
                {
                    i_tmp_ChooseChangePoint = UnityEngine.Random.Range(0, 6);
                }
                while (i_tmp_ChooseChangePoint == oriPoint);
                b_isCheckPoint = true;
            }
            choose_ChangePoints = father_ChangePoints.transform.GetChild(i_tmp_ChooseChangePoint).gameObject;
            if(f_CurrentTime >= 3.0f)
            {
                

                if (b_isOnPoint)
                {
                    if (f_TragetToDrop >= 4.5f)
                    {
                        oriPoint = i_tmp_ChooseChangePoint;
                        Body.enabled = true;
                        rg.useGravity = true;
                        rg.velocity = new Vector3(0, -10, 0);
                        MoveOnDrop(father_ChangePoints.transform.GetChild(i_tmp_ChooseChangePoint + 6).gameObject, ref b_OnDrop);
                        if (b_OnDrop)
                        {
                            Vector3 vpos = mData.TargetObject.transform.position - mData.EnemySelf.transform.position;
                            vpos.y = 0;
                            float dis = vpos.magnitude;
                            Debug.Log("dis = "+ dis);
                            if (dis <= 11.0f)
                            {
                                Ft.BeHit(200);
                            }
                            i_tmp_Choose = Random.Range(1, 5);
                            if (i_tmp_ChooseChangePoint == 5)
                            {
                                CurrentState = eFSMState.StartToShoot;
                                i_tmp_Shoots = 0;
                                b_CheckShootsNumBer = false;
                                b_ishaveTmpPoint = false;
                                f_CurrentTime = 0.0f;
                                b_isTurnCurrect = false;
                                i_idleFrequency = 0;
                                return;
                            }
                            else
                            {
                                CurrentState = eFSMState.TurnAroundToTarget;
                                f_CurrentTime = 0.0f;
                                b_isTurnCurrect = false;
                                b_isIdle = false;
                                return;
                            }
                        }
                        return;
                    }
                    else if(f_TragetToDrop >= 3.5f)
                    {
                        StartCoroutine(Disslove(0));
                        targetSelector.Lock();
                        f_TragetToDrop += Time.deltaTime;
                        return;
                    }
                    else
                    {
                        //StartCoroutine(Disslove(0));
                        f_TragetToDrop += Time.deltaTime;
                        return;
                    }
                }
                else
                {
                    PS3.transform.position = father_ChangePoints.transform.GetChild(i_tmp_ChooseChangePoint + 6).gameObject.transform.position;
                    PS3.Play();
                    //Body.enabled = false;
                    //rg.useGravity = false;
                    MoveVer(choose_ChangePoints, ref b_isOnPoint);
                    return;
                }
            }
            else
            {
                StartCoroutine(Disslove(1));
                targetSelector.Unlock();
                Body.enabled = false;
                rg.useGravity = false;
                f_CurrentTime += Time.deltaTime;
                return;
            }
            
        }
        else if (CurrentState == eFSMState.StartToShoot)
        {
            Anim.SetBool("Move", false);
            CheckTarget();
            Func_BeHit();
            if (i_shootingFrequency == 5)
            {
                CurrentState = eFSMState.StartToLaser;
                f_CurrentTime = 0.0f;
                i_shootingFrequency = 0;
                i_tmp_Shoots = 0;
                PS2.Play();
                return;
            }
            if (b_CheckShootsNumBer != true)
            {
                i_Shoots = Random.Range(5, 11);
                b_CheckShootsNumBer = true;
            }
            if(b_isTurnCurrect)
            {
                if (b_ishaveTmpPoint != true)//紀錄當下位置
                {
                    tmp_CHT_Master.transform.position = CrossHairTarget_Master.transform.position;
                    b_ishaveTmpPoint = true;
                }
                if (f_CurrentTime > 0.1f)
                {
                    CurrentState = eFSMState.Shooting;
                    b_OnceShooted = false;
                    Anim_MasterGun.enabled = true;//機槍攻擊
                    f_CurrentTime = 0.0f;
                    return;
                }
                else
                {
                    f_CurrentTime += Time.deltaTime;
                    return;
                }
            }
            else
            {
                TurnToTargetForShooting(ref b_isTurnCurrect, mData.TargetObject);
                //Debug.Log("3");
                return;
            }
        }
        else if (CurrentState == eFSMState.Shooting)
        {
            Anim.SetBool("Move", false);
            CheckTarget();
            Func_BeHit();
            //Debug.Log(i_tmp_Shoots);
            if (b_OnceShooted)
            {
                RCW.StopFiring();
                Anim_MasterGun.enabled = false;
                if (i_tmp_Shoots >= i_Shoots)
                {
                    CurrentState = eFSMState.Idle;
                    i_shootingFrequency += 1;
                    f_CurrentTime = 0.0f;
                    b_isIdle = false;
                }
                else
                {
                    i_tmp_Shoots += 1;
                    CurrentState = eFSMState.StartToShoot;
                    b_isTurnCurrect = false;
                    b_ishaveTmpPoint = false;
                    b_CheckShootsNumBer = false;
                    b_ishaveTmpPoint = false;
                    f_CurrentTime = 0.0f;
                    b_isTurnCurrect = false;
                    i_idleFrequency = 0;
                    return;
                }
            }
            else
            {
                MainCurrentBaseStage = Anim_MasterGun.GetCurrentAnimatorStateInfo(0);
                Anim.SetBool("Move", false);
                Anim_MasterGun.SetTrigger("GunAttack");
                CheckTarget();
                Func_BeHit();
                if (MainCurrentBaseStage.IsName("Shoot"))
                {
                    RCW.StartFiringForMasterGun();
                    b_OnceShooted = true;
                    return;
                }
                if (MainCurrentBaseStage.IsName("Over"))
                {
                    RCW.StopFiring();
                    Anim_MasterGun.enabled = false;
                    if (i_tmp_Shoots >= i_Shoots)
                    {
                        CurrentState = eFSMState.Idle;
                        f_CurrentTime = 0.0f;
                        b_isIdle = false;
                    }
                    else
                    {
                        i_tmp_Shoots += 1;
                        CurrentState = eFSMState.StartToShoot;
                        b_ishaveTmpPoint = false;
                        b_isTurnCurrect = false;
                        b_CheckShootsNumBer = false;
                        b_ishaveTmpPoint = false;
                        f_CurrentTime = 0.0f;
                        b_isTurnCurrect = false;
                        i_idleFrequency = 0;
                        return;
                    }
                }
            }
            //Debug.Log("3");
            return;
            
        }
        else if (CurrentState == eFSMState.StartToLaser)
        {
            Anim.SetBool("Move", false);
            CheckTarget();
            Func_BeHit();
            if(f_CurrentTime >= 5)
            {
                CurrentState = eFSMState.Laser;
                PS.loop = false;
                PS.Stop();
                PS2.Stop();
                b_OnceShooted = false;
                Anim_MasterLaser.enabled = true;//機槍攻擊
                f_CurrentTime = 0.0f;
                i_tmp_Shoots = 0;
                return;
            }
            else if(f_CurrentTime >= 4.7)
            {
                if (b_ishaveTmpPoint != true)//紀錄當下位置
                {
                    tmp_CHT_Master.transform.position = CrossHairTarget_Master.transform.position;
                    b_ishaveTmpPoint = true;
                    f_CurrentTime += Time.deltaTime;
                    return;
                }
                f_CurrentTime += Time.deltaTime;
                return;
            }
            else
            {
                PS.loop = true;
                PS.Play();
                TurnToTargetForShooting(ref b_isTurnCurrect, mData.TargetObject);
                f_CurrentTime += Time.deltaTime;
                return;
            }
        }
        else if (CurrentState == eFSMState.Laser)
        {
            CheckTarget();
            Func_BeHit();
            if (b_OnceShooted)
            {
                RCW.StopFiring();
                Anim_MasterGun.enabled = false;
                CurrentState = eFSMState.Idle;
                b_isTurnCurrect = false;
                b_ishaveTmpPoint = false;
                b_CheckShootsNumBer = false;
                f_CurrentTime = 0.0f;
                b_isTurnCurrect = false;
                i_idleFrequency = 0;
                return;
                
            }
            else
            {
                LaserCurrentBaseStage = Anim_MasterLaser.GetCurrentAnimatorStateInfo(0);
                Anim.SetBool("Move", false);
                Anim_MasterLaser.SetTrigger("GunAttack");
                CheckTarget();
                Func_BeHit();
                if (LaserCurrentBaseStage.IsName("Shoot"))
                {
                    RCW.StartLaser();
                    b_OnceShooted = true;
                    return;
                }
                if (MainCurrentBaseStage.IsName("Over"))
                {
                    RCW.StopFiring();
                    Anim_MasterGun.enabled = false;
                    CurrentState = eFSMState.Idle;
                    f_CurrentTime = 0.0f;
                    b_isIdle = false;
                }
            }
            return;
        }
        //else if (CurrentState == eFSMState.MeleeExclusion)
        //{
        //    CheckTarget();
        //    Func_BeHit();
        //    if (/*在一定範圍以內*/)
        //    {
        //        if (f_inRangeToMeleeTime >= 12.0f)
        //        {
        //            跳起來來擊退
        //            確定動作完成
        //            然後遠程攻擊
        //            回到idle
        //        }
        //        else
        //        {
        //            f_inRangeToMeleeTime += Time.deltaTime;
        //        }
        //    }
        //}
        else if (CurrentState == eFSMState.Dead)
        {
            if (mData.b_IsDead != true)
            {
                PS5.Play();
                Body.enabled = false;
                rg.isKinematic = true;
                CurrentState = eFSMState.Finish;
                mData.b_IsDead = true;
                onDead.Invoke(gameObject);
                return;
            }
            return;
            //if (MainCurrentBaseStage.IsName("Legs_Heavy_fall_back") && MainCurrentBaseStage.normalizedTime >= 0.7)
            //{
            //    rg.isKinematic = true;
            //    Body.enabled = false;
            //    CurrentState = eFSMState.Finish;
            //    return;
            //}
            //if (MainCurrentBaseStage.IsName("Legs_Heavy_fall_back"))
            //{
            //    return;
            //}
        }
        else if (CurrentState == eFSMState.TargetDead)
        {
            //關閉所有動作開關
        }
        else if (CurrentState == eFSMState.UnFight)
        {
            if (GameSystem.IsCombat)
            {
                //transform.GetChild(1).gameObject.SetActive(true);
                StartCoroutine(Disslove(0));
                StartCoroutine(StartCombatState(1));
                return;

            }
            return;
        }
        else if (CurrentState == eFSMState.Finish)
        {
            //End
            StartCoroutine(Disslove(1));
        }
    }
    void CheckTarget()
    {
        if (TargetNowCurrentState.currentHP <= 0)
        {
            CurrentState = eFSMState.TargetDead;
            return;
        }
    }
    void Func_BeHit()
    {
        if (b_BeHit)
        {
            float damage = Ft.GetHitValueAndReward();
            mData.f_Hp -= damage;
            onUV.Invoke(damage);
            b_BeHit = false;
            if (mData.f_Hp <= 0)
            {
                CurrentState = eFSMState.Dead;
                return;
            }
            //CurrentState = eFSMState.ByHit;
            return;
        }
    }
    void TurnToTarget(ref bool b_isTurnCurrect, GameObject TARGET)
    {
        Vector3 targetDirection = TARGET.transform.position - mData.EnemySelf.transform.position;
        targetDirection.y = 0;
        float step = 5.0f * Time.deltaTime;
        Vector3 newDirection = Vector3.RotateTowards(mData.EnemySelf.transform.forward, targetDirection, step, 0.0f);
        mData.EnemySelf.transform.rotation = Quaternion.LookRotation(newDirection);
        float result = Vector3.Dot(mData.EnemySelf.transform.forward.normalized, targetDirection.normalized);
        if (result > 0.999f)
        {
            b_isTurnCurrect = true;
        }
        else
        {
            b_isTurnCurrect = false;
        }
    }
    void TurnMoveToTarget(GameObject TARGET)
    { 
        Vector3 targetDirection = TARGET.transform.position - mData.EnemySelf.transform.position;
        targetDirection.y = 0;
        float step = TurnMoveToTarget_TurnSpeed * Time.deltaTime;
        Vector3 newDirection = Vector3.RotateTowards(mData.EnemySelf.transform.forward, targetDirection, step, 0.0f);
        mData.EnemySelf.transform.rotation = Quaternion.LookRotation(newDirection);
        float stepp = TurnMoveToTarget_MoveSpeed * Time.deltaTime;
        mData.EnemySelf.transform.localPosition = Vector3.MoveTowards(gameObject.transform.localPosition, TARGET.transform.position, stepp);
    }

    void TurnToTargetForShooting(ref bool b_isTurnCurrect, GameObject TARGET)
    {
        Vector3 targetDirection = TARGET.transform.position - RAttackTurnPart.transform.position;
        targetDirection.y = 0;
        float step = 5.0f * Time.deltaTime;
        Vector3 newDirection = Vector3.RotateTowards(RAttackTurnPart.transform.forward, targetDirection, step, 0.0f);
        RAttackTurnPart.transform.rotation = Quaternion.LookRotation(newDirection);
        float result = Vector3.Dot(RAttackTurnPart.transform.forward.normalized, targetDirection.normalized);
        //Debug.Log("reullt : " + result);
        if (result > 0.999f)
        {
            b_isTurnCurrect = true;
        }
        else
        {
            b_isTurnCurrect = false;
        }
    }
    void CheckOnCurrectPoint(GameObject TARGET, ref bool b_isOnPoint)
    {
        Vector3 pos = (mData.EnemySelf.transform.position - TARGET.transform.position);
        float distance = Mathf.Sqrt((pos.x * pos.x) + (pos.z * pos.z));
        if (distance<0.001f)
        {
            b_isOnPoint = true;
        }
        else
        {
            b_isOnPoint = false;
        }
    }
    void CheckOnCurrectDownPoint(GameObject TARGET, ref bool b_OnDrop)
    {
        Vector3 pos = (mData.EnemySelf.transform.position - TARGET.transform.position);
        float distance = Mathf.Sqrt((pos.x * pos.x) + (pos.z * pos.z) + (pos.y*pos.y));
        if (distance < 0.001f)
        {
            b_OnDrop = true;
        }
        else
        {
            b_OnDrop = false;
        }
    }
    void  MoveVer(GameObject CHOOSEPOINT, ref bool canDrop)
    {
        rg.MovePosition(CHOOSEPOINT.transform.position);
        canDrop = true;
    }
    void MoveOnDrop(GameObject TARGET, ref bool b_OnDrop)
    {
        Vector3 pos = (TARGET.transform.position - transform.position);
        pos.x = 0;
        pos.z = 0;
        float distance = pos.magnitude;
        //Debug.Log("distance = " + distance);
        if (distance < 0.02f)
        {
            b_OnDrop = true;
            PS4.transform.position = father_ChangePoints.transform.GetChild(i_tmp_ChooseChangePoint + 6).gameObject.transform.position;
            PS3.transform.position = father_ChangePoints.transform.GetChild(i_tmp_ChooseChangePoint + 6).gameObject.transform.position;
            PS4.Play();
            scAllMusicManager.Normal(scAllMusicManager.MainScene_MasterDrop);
            PS3.Stop();
        }
        else
        {
            b_OnDrop = false;
        }
    }
    private void OnDrawGizmos()
    {
        if (mData == null)
        {
            return;
        }
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward * 2.0f);
        if (CurrentState == eFSMState.Idle)
        {
            Gizmos.color = Color.green;
        }
        else if (CurrentState == eFSMState.Chase)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(this.transform.position, mData.v3_Target);
        }
        if (CurrentState == eFSMState.LookAndRAttack)
        {
            Gizmos.color = Color.gray;
        }
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, mData.f_Remote_AttackRange);

        if (CurrentState == eFSMState.ChaseAndMAttackAndBack)
        {
            Gizmos.color = Color.gray;
        }
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(this.transform.position, mData.f_Melee_AttackRange);

        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(this.transform.position, mData.f_Sight);
    }
}
