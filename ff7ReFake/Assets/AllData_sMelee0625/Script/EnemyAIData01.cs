﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

[System.Serializable]
public class EnemyAIData01
{
    public float f_Radius;//半徑
    public float f_ProbeLength;//探測長度
    public float f_Speed;//移動速度
    public float f_MaxSpeed;//最高移動速度
    public GameObject EnemySelf;//敵人本身
    public float f_Sight;//視野
    public float f_Melee_AttackRange;//近攻擊範圍
    public float f_Remote_AttackRange;//遠攻擊範圍
    public float f_LookRange;//對峙範圍
    public float f_MaxRot;
    public float f_Rot;
    public float f_Hp = 50;//血量
    public bool b_IsDead = false;//是否死亡
    public GameObject TargetObject;//當下目標
    public Vector3 v3_Target;
    public Vector3 v3_CurrentVector;
    public float f_TempTurnForce;
    public float f_MoveForce;
    public bool b_Move;
    public bool b_Col;
    public bool isTargetDead = false;//是否目標死亡
    public float f_ShieldHP;

}