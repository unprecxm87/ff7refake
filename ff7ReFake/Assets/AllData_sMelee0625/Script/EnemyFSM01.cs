﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyFSM01 : EnemyDataBase
{
    public Rigidbody rg;

    float f_LookAndRAttackTime;
    AnimatorStateInfo MainCurrentBaseStage;

    [SerializeField] private Collider weapon;
    [SerializeField] private Collider Body;
    int i_tmp_KeepLook;
    private void OnEnable()
    {
        DissolveAmount = 1;
        Anim = GetComponent<Animator>();
        CurrentEnemyTarget = null;
        CurrentState = eFSMState.UnFight;
        f_CurrentTime = 0.0f;
        f_IdleTime = UnityEngine.Random.Range(3.0f, 5.0f);
        b_BeHit = false;
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
        mData.f_Hp = MaxHP;
        mData.b_IsDead = false;
        rg.isKinematic = false;
        Body.enabled = true;
    }

    private GameObject CheckEnemyInSight(ref bool bMAttack, ref bool bRAttack)
    {
        GameObject PlayerSelf = sMMain.Instance.GetPlayer();
        Vector3 v3_ETP = PlayerSelf.transform.position - this.transform.position;
        float fDist = v3_ETP.magnitude;
        if (fDist < mData.f_Melee_AttackRange)
        {
            bMAttack = true;
            return PlayerSelf;
        }
        else if (fDist < mData.f_Remote_AttackRange)
        {
            bRAttack = true;
            return PlayerSelf;
        }
        else if (fDist < mData.f_Sight)
        {
            bMAttack = false;
            bRAttack = false;
            return PlayerSelf;
        }
        return null;
    }

 

    private void SetWeapon(int state)
    {
        if (state == 1) weapon.enabled = true;
        if (state == 0) weapon.enabled = false;
    }
    private void SetKick()
    {
        scAllMusicManager.Normal(scAllMusicManager.MainScene_MeleeKick);
    }
    private void SetShooting()
    {
        scAllMusicManager.Normal(scAllMusicManager.MainScene_MeleeShooting);
    }

    //private void JumpBackward(int state)
    //{
    //    if(state == 1)
    //    {
    //        //rb.MovePosition(transform.position + -transform.forward * Time.deltaTime);
    //        transform.Translate(-Vector3.forward * Time.deltaTime * 110.0f, Space.Self);
    //    }
    //}
    private bool CheckTargetEnemyInSight(GameObject target, ref bool bMAttack, ref bool bRAttack)
    {
        GameObject PlayerSelf = target;
        Vector3 v3_ETP = PlayerSelf.transform.position - this.transform.position;
        float fDist = v3_ETP.magnitude;
        if (fDist < mData.f_Melee_AttackRange)
        {
            bMAttack = true;
            return true;
        }
        else if(fDist < mData.f_Remote_AttackRange - 0.2f)
        {
            bRAttack = true;
            return true;
        }
        else if (fDist < mData.f_Sight)
        {
            bMAttack = false;
            bRAttack = false;
            return true;
        }
        return false;
    }

    private bool ACheckTargetEnemyInSight(Vector3 target, ref bool bMAttack, ref bool bRAttack)
    {
        Vector3 v3_ETP = target - this.transform.position;
        float fDist = v3_ETP.magnitude;
        if (fDist < mData.f_Melee_AttackRange)
        {
            bMAttack = true;
            return true;
        }
        else if (fDist < mData.f_Remote_AttackRange - 0.2f)
        {
            bRAttack = true;
            return true;
        }
        else if (fDist < mData.f_Sight)
        {
            bMAttack = false;
            bRAttack = false;
            return true;
        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            StartCoroutine(Disslove(1));
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            StartCoroutine(Disslove(0));
        }
        //transform.position = new Vector3(this.transform.position.x, 13.76f, this.transform.position.z);
        Anim.SetFloat("TargetNowCurrentHP", TargetNowCurrentState.currentHP);
        //Debug.Log(gameObject.name + "Current State " + CurrentState);
        MainCurrentBaseStage = Anim.GetCurrentAnimatorStateInfo(0);
        if (CurrentState == eFSMState.Idle)
        {
            Anim.SetBool("OnFight", true);
            Anim.SetBool("Move", false);
            Anim.SetBool("ChaseMove", false);
            bool bMAttack = false;
            bool bRAttack = false;
            if (TargetNowCurrentState.currentHP <= 0)
            {
                CurrentState = eFSMState.TargetDead;
                return;
            }
            if (b_BeHit)
            {
                float damage = Ft.GetHitValueAndReward();
                mData.f_Hp -= damage;
                onUV.Invoke(damage);
                b_BeHit = false;
                if (mData.f_Hp <= 0)//HP是否歸零
                {
                    CurrentState = eFSMState.Dead;
                    scAllMusicManager.Normal(scAllMusicManager.MainScene_MeleeDead);
                    return;
                }
                CurrentState = eFSMState.ByHit;
                return;
            }
            CurrentEnemyTarget = CheckEnemyInSight(ref bRAttack, ref bMAttack);
            if (CurrentEnemyTarget != null)
            {
                mData.TargetObject = CurrentEnemyTarget;
                CurrentState = eFSMState.Chase;
            }
            else
            {
                return;
            }
            if (f_CurrentTime > f_IdleTime)
            {
                f_CurrentTime = 0.0f;
                f_IdleTime = 0.5f;
                CurrentState = eFSMState.Idle;
            }
            else
            {
                f_CurrentTime += Time.deltaTime;
            }
        }
        else if (CurrentState == eFSMState.Chase)
        {
            bool bMAttack = false;
            bool bRAttack = false;
            if (TargetNowCurrentState.currentHP <= 0)
            {
                CurrentState = eFSMState.TargetDead;
                return;
            }
            if (b_BeHit)
            {
                float damage = Ft.GetHitValueAndReward();
                mData.f_Hp -= damage;
                onUV.Invoke(damage);
                b_BeHit = false;
                if (mData.f_Hp <= 0)//HP是否歸零
                {
                    CurrentState = eFSMState.Dead;
                    scAllMusicManager.Normal(scAllMusicManager.MainScene_MeleeDead);
                    return;
                }
                CurrentState = eFSMState.ByHit;
                return;
            }
            bool bCheck = CheckTargetEnemyInSight(CurrentEnemyTarget, ref bMAttack, ref bRAttack);
            if (bCheck == false)
            {
                CurrentState = eFSMState.Idle;
                f_CurrentTime = 0.0f;
                f_IdleTime = UnityEngine.Random.Range(3.0f, 5.0f);
                Anim.SetBool("Move", false);
                return;
            }  
            if (bRAttack)
            {
                CurrentState = eFSMState.LookAndRAttack;
                i_tmp_KeepLook = UnityEngine.Random.Range(0, 2);
                f_LookAndRAttackTime = UnityEngine.Random.Range(1.0f, 3.0f);
                f_CurrentTime = 0.0f;
                Anim.SetBool("Move", false);
                return;
            }
            //else if(bMAttack)
            //{
            //    CurrentState = eFSMState.ChaseAndMAttackAndBack;
            //    mData.f_LookAttackTime = UnityEngine.Random.Range(1.0f, 3.0f);
            //    f_CurrentTime = 0.0f;
            //    Anim.SetBool("Move", false);
            //    return;
            //}
            else
            {
                Anim.SetBool("Move", true);
                mData.v3_Target = mData.TargetObject.transform.position;
                Seek(mData);
                Move(mData);
            }
        }
        else if (CurrentState == eFSMState.LookAndRAttack)
        {
            bool bMAttack = false;
            bool bRAttack = false;
            if (TargetNowCurrentState.currentHP <= 0)
            {
                CurrentState = eFSMState.TargetDead;
                return;
            }
            if (b_BeHit)
            {
                float damage = Ft.GetHitValueAndReward();
                mData.f_Hp -= damage;
                onUV.Invoke(damage);
                b_BeHit = false;
                if (mData.f_Hp <= 0)//HP是否歸零
                {
                    CurrentState = eFSMState.Dead;
                    scAllMusicManager.Normal(scAllMusicManager.MainScene_MeleeDead);
                    return;
                }
                CurrentState = eFSMState.ByHit;
                return;
            }
            if (MainCurrentBaseStage.IsName("Shooting")) return;
            bool bCheck = CheckTargetEnemyInSight(CurrentEnemyTarget, ref bMAttack, ref bRAttack);
            if (bCheck == false)
            {
                CurrentState = eFSMState.Idle;
                f_CurrentTime = 0.0f;
                f_IdleTime = UnityEngine.Random.Range(3.0f, 5.0f);
                return;
            }
            if (f_CurrentTime < f_LookAndRAttackTime)
            {
                if (i_tmp_KeepLook == 0)
                {
                    Anim.SetBool("StareLeft", true);
                    this.transform.LookAt(mData.TargetObject.transform);
                    transform.Translate(-Vector3.right * Time.deltaTime * 1.0f, Space.Self);
                    f_CurrentTime += Time.deltaTime;
                }
                else
                {
                    Anim.SetBool("StareRight", true);
                    this.transform.LookAt(mData.TargetObject.transform);
                    transform.Translate(Vector3.right * Time.deltaTime * 1.0f, Space.Self);
                    f_CurrentTime += Time.deltaTime;
                }
            }
            else
            {
                //Anim.SetBool("StareLeft", false);
                //Anim.SetBool("StareRight", false);
                i_tmp_Choose = UnityEngine.Random.Range(0, 10);
                if (i_tmp_Choose == 0 || i_tmp_Choose == 1 || i_tmp_Choose == 2)
                {
                    CurrentState = eFSMState.ChaseAndMAttackAndBack;
                    Anim.SetBool("StareLeft", false);
                    Anim.SetBool("StareRight", false);
                    return;
                }
                else if (i_tmp_Choose == 5 || i_tmp_Choose == 3 || i_tmp_Choose == 4 || i_tmp_Choose == 6)
                {
                    Anim.SetTrigger("Shooting");
                    CurrentState = eFSMState.Shooting;
                    //Debug.Log("5");
                    f_CurrentTime = 0.0f;
                    return;
                }
                else
                {
                    Anim.SetBool("StareLeft", false);
                    Anim.SetBool("StareRight", false);
                    i_tmp_KeepLook = UnityEngine.Random.Range(0, 2);
                    f_CurrentTime = 0.0f;
                    return;
                }
            }
        }
        else if (CurrentState == eFSMState.Shooting)
        {
            Anim.SetBool("ChaseMove", false);
            Anim.SetBool("StareLeft", false);
            Anim.SetBool("StareRight", false);
            if (TargetNowCurrentState.currentHP <= 0)
            {
                CurrentState = eFSMState.TargetDead;
                return;
            }
            if (b_BeHit)
            {
                float damage = Ft.GetHitValueAndReward();
                mData.f_Hp -= damage;
                onUV.Invoke(damage);
                b_BeHit = false;
                if (mData.f_Hp <= 0)//HP是否歸零
                {
                    CurrentState = eFSMState.Dead;
                    scAllMusicManager.Normal(scAllMusicManager.MainScene_MeleeDead);
                    return;
                }
                CurrentState = eFSMState.ByHit;
                return;
            }
            if (MainCurrentBaseStage.IsName("Shooting"))
            {
                RCW.StartFiring();
                //Debug.Log("1");
                return;
            }

            if (MainCurrentBaseStage.IsName("Shooting") && MainCurrentBaseStage.normalizedTime >= 1)
            {
                RCW.StopFiring();
                //Debug.Log("2");
                CurrentState = eFSMState.LookAndRAttack;
                return;
            }
            if (MainCurrentBaseStage.IsName("rifle aiming idle"))
            {
                CurrentState = eFSMState.LookAndRAttack;
                //Debug.Log("3");
                return;
            }
            //Debug.Log("4");
            return;

        }
        else if (CurrentState == eFSMState.ChaseAndMAttackAndBack)
        {
            if (TargetNowCurrentState.currentHP <= 0)
            {
                CurrentState = eFSMState.TargetDead;
                return;
            }
            if (b_BeHit)
            {
                float damage = Ft.GetHitValueAndReward();
                mData.f_Hp -= damage;
                onUV.Invoke(damage);
                b_BeHit = false;
                if (mData.f_Hp <= 0)//HP是否歸零
                {
                    CurrentState = eFSMState.Dead;
                    scAllMusicManager.Normal(scAllMusicManager.MainScene_MeleeDead);
                    return;
                }
                //CurrentState = eFSMState.ByHit;
                //return;
            }
            Vector3 v = mData.TargetObject.transform.position - mData.EnemySelf.transform.position;
            float fDist = v.magnitude;
            if (fDist < 1.4f)
            {
                Anim.SetBool("ChaseMove", false);
                Anim.SetTrigger("Attack_Leg01");
                //rg.isKinematic = false;
                CurrentState = eFSMState.Back;
                return;
            }
            else
            {
                Anim.SetBool("ChaseMove", true);
                this.transform.forward = v;
                //Seek(mData);
                //Move(mData);
                transform.Translate(Vector3.forward * Time.deltaTime * 8.0f, Space.Self);
                //rg.isKinematic = true;
                return;
            }
        }
        else if(CurrentState == eFSMState.Back)
        {
            if (TargetNowCurrentState.currentHP <= 0)
            {
                CurrentState = eFSMState.TargetDead;
                //Debug.Log("1");
                return;
            }
            if (b_BeHit)
            {
                float damage = Ft.GetHitValueAndReward();
                mData.f_Hp -= damage;
                onUV.Invoke(damage);
                b_BeHit = false;
                if (mData.f_Hp <= 0)//HP是否歸零
                {
                    CurrentState = eFSMState.Dead;
                    scAllMusicManager.Normal(scAllMusicManager.MainScene_MeleeDead);
                    return;
                }
                //CurrentState = eFSMState.ByHit;
                //return;
            }
            if (MainCurrentBaseStage.IsName("ChaseRun"))
            {
                //Debug.Log("2");
                Anim.SetFloat("TargetNowCurrentHP", TargetNowCurrentState.currentHP);
                if (TargetNowCurrentState.currentHP <= 0)
                {
                    CurrentState = eFSMState.TargetDead;
                    //Debug.Log("3");
                    return;
                }
                return;
            }
            if (MainCurrentBaseStage.IsName("Attack_Leg01"))
            {
                //Debug.Log("4");
                Anim.SetFloat("TargetNowCurrentHP", TargetNowCurrentState.currentHP);
                if (TargetNowCurrentState.currentHP <= 0)
                {
                    CurrentState = eFSMState.TargetDead;
                    //Debug.Log("5");
                    return;
                }
                return;
            }
            if (MainCurrentBaseStage.IsName("rifle aiming idle"))
            {
                //Debug.Log("6");
                return;
            }
            if (MainCurrentBaseStage.IsName("jump backward") && MainCurrentBaseStage.normalizedTime>=1)
            {
                CurrentState = eFSMState.LookAndRAttack;
                //rg.isKinematic = false;
                //Debug.Log("7");
                return;
            }
            if (MainCurrentBaseStage.IsName("jump backward"))
            {
                transform.Translate(-Vector3.forward * Time.deltaTime * 5.0f, Space.Self);
                //rg.isKinematic = true;
                //Debug.Log("8");
                return;                
            }
            CurrentState = eFSMState.LookAndRAttack;
            //Debug.Log("9");
            return;
        }
        else if (CurrentState == eFSMState.ByHit)
        {
            //rg.isKinematic = false;
            Anim.SetTrigger("BeHit");
            weapon.enabled = false;
            Anim.SetBool("Move", false);
            if (TargetNowCurrentState.currentHP <= 0)
            {
                CurrentState = eFSMState.TargetDead;
                return;
            }
            if (mData.f_Hp <= 0)//HP是否歸零
            {
                CurrentState = eFSMState.Dead;
                scAllMusicManager.Normal(scAllMusicManager.MainScene_MeleeDead);
                return;
            }
            if (b_BeHit)
            {
                CurrentState = eFSMState.ByHit;
                b_BeHit = false;
                float damage = Ft.GetHitValueAndReward();
                mData.f_Hp -= damage;
                onUV.Invoke(damage);
                return;
            }
            
            CurrentState = eFSMState.LookAndRAttack;
            return;
        }
        else if (CurrentState == eFSMState.Dead)
        {
            
            Anim.SetFloat("TargetNowCurrentHP", TargetNowCurrentState.currentHP);
            if (MainCurrentBaseStage.IsName("knockdown_A") && MainCurrentBaseStage.normalizedTime >= 0.05)
            {
                Body.enabled = false;
                rg.isKinematic = true;
            }
            if (MainCurrentBaseStage.IsName("knockdown_A") && MainCurrentBaseStage.normalizedTime >= 0.7)
            {
                //Debug.Log("11111");
                //rg.isKinematic = true;
                weapon.enabled = false;
                CurrentState = eFSMState.Finish;
                return;
            }
            if (MainCurrentBaseStage.IsName("knockdown_A"))
            {
                //Debug.Log("22222");
                //transform.Translate(-Vector3.forward * Time.deltaTime * 4.0f, Space.Self);
                return;
            }
            if (!mData.b_IsDead == true)
            {
                //Debug.Log("33333");
                Anim.SetTrigger("Dead");
                mData.b_IsDead = true;
                onDead.Invoke(gameObject);
                onGainEXP.Invoke(UnityEngine.Random.Range(EXPGained - 5, EXPGained + 5));
                return;
            }
            return;
        }
        else if (CurrentState == eFSMState.TargetDead)
        {
            Anim.SetFloat("TargetNowCurrentHP", TargetNowCurrentState.currentHP);
            Anim.SetBool("Move", false);
            Anim.SetBool("Look", false);
            Anim.SetBool("ChaseMove", false);
            Anim.SetBool("StareLeft", false);
            Anim.SetBool("StareRight", false);
        }
        else if (CurrentState == eFSMState.UnFight)
        {
            if (GameSystem.IsCombat)
            {
                transform.GetChild(1).gameObject.SetActive(true);
                StartCoroutine(Disslove(0));
                StartCoroutine(StartCombatState(1));
            }
            return;
        }
        else if(CurrentState == eFSMState.Finish)
        {
            //End
            StartCoroutine(Disslove(1));
        }
    }
    private void OnDrawGizmos()
    {
        if(mData == null)
        {
            return;
        }
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward * 2.0f);
        if (CurrentState == eFSMState.Idle)
        {
            Gizmos.color = Color.green;
        }
        else if (CurrentState == eFSMState.Chase)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(this.transform.position, mData.v3_Target);
        }
        if (CurrentState == eFSMState.LookAndRAttack)
        {
            Gizmos.color = Color.gray;
        }
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, mData.f_Remote_AttackRange);

        if (CurrentState == eFSMState.ChaseAndMAttackAndBack)
        {
            Gizmos.color = Color.gray;
        }
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(this.transform.position, mData.f_Melee_AttackRange);

        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(this.transform.position, mData.f_Sight);
    }
    //static public void Move(EnemyAIData01 data)
    //{
    //    if (data.b_Move == false)
    //    {
    //        return;
    //    }
    //    Transform t = data.EnemySelf.transform;
    //    Vector3 cPos = data.EnemySelf.transform.position;
    //    Vector3 vR = t.right;
    //    Vector3 vOriF = t.forward;
    //    Vector3 vF = data.v3_CurrentVector;

    //    if (data.f_TempTurnForce > data.f_MaxRot)
    //    {
    //        data.f_TempTurnForce = data.f_MaxRot;
    //    }
    //    else if (data.f_TempTurnForce < -data.f_MaxRot)
    //    {
    //        data.f_TempTurnForce = -data.f_MaxRot;
    //    }

    //    vF = vF + vR * data.f_TempTurnForce;
    //    //vF.Normalize();
    //    t.forward = vF;


    //    data.f_Speed = data.f_Speed + data.f_MoveForce * Time.deltaTime;
    //    if (data.f_Speed < 0.01f)
    //    {
    //        data.f_Speed = 0.01f;
    //    }
    //    else if (data.f_Speed > data.f_MaxSpeed)
    //    {
    //        data.f_Speed = data.f_MaxSpeed;
    //    }

    //    if (data.b_Col == false)
    //    {
    //            t.forward = vOriF;
    //    }
    //    else
    //    {
    //        if (data.f_Speed < 0.02f)
    //        {
    //            if (data.f_TempTurnForce > 0)
    //            {
    //                t.forward = vR;
    //            }
    //            else
    //            {
    //                t.forward = -vR;
    //            }

    //        }
    //    }

    //    cPos = cPos + t.forward * data.f_Speed;
    //    t.position = cPos;
    //}

    static public void Move(EnemyAIData01 data)
    {
        if (data.b_Move == false)
        {
            return;
        }
        Transform t = data.EnemySelf.transform;
        Vector3 cPos = data.EnemySelf.transform.position;
        Vector3 vR = t.right;
        Vector3 vOriF = t.forward;
        Vector3 vF = data.v3_CurrentVector;

        if (data.f_TempTurnForce > data.f_MaxRot)
        {
            data.f_TempTurnForce = data.f_MaxRot;
        }
        else if (data.f_TempTurnForce < -data.f_MaxRot)
        {
            data.f_TempTurnForce = -data.f_MaxRot;
        }

        vF = vF + vR * data.f_TempTurnForce;
        //vF.Normalize();
        t.forward = vF;


        data.f_Speed = data.f_Speed + data.f_MoveForce * Time.deltaTime;
        if (data.f_Speed < 0.01f)
        {
            data.f_Speed = 0.01f;
        }
        else if (data.f_Speed > data.f_MaxSpeed)
        {
            data.f_Speed = data.f_MaxSpeed;
        }

        //if (data.b_Col == false)
        //{
        //    Debug.Log("CheckCollision");
        //    if (CheckCollision(data))
        //    {
        //        Debug.Log("CheckCollision true");
        //        t.forward = vOriF;
        //    }
        //    else
        //    {
        //        Debug.Log("CheckCollision false");
        //    }
        //}
        //else
        //{
            if (data.f_Speed < 0.02f)
            {
                if (data.f_TempTurnForce > 0)
                {
                    t.forward = vR;
                }
                else
                {
                    t.forward = -vR;
                }

            }
        //}

        cPos = cPos + t.forward * data.f_Speed;
        t.position = cPos;
    }
    static public bool CheckCollision(EnemyAIData01 data)
    {
        List<Obstacle> m_AvoidTargets = sMMain.Instance.GetObstacles();
        if (m_AvoidTargets == null)
        {
            return false;
        }
        Transform ct = data.EnemySelf.transform;
        Vector3 cPos = ct.position;
        Vector3 cForward = ct.forward;
        Vector3 vec;

        float fDist = 0.0f;
        float fDot = 0.0f;
        int iCount = m_AvoidTargets.Count;
        for (int i = 0; i < iCount; i++)
        {
            vec = m_AvoidTargets[i].transform.position - cPos;
            vec.y = 0.0f;
            fDist = vec.magnitude;
            if (fDist > data.f_ProbeLength + m_AvoidTargets[i].m_fRadius)
            {
                m_AvoidTargets[i].m_eState = Obstacle.eState.OUTSIDE_TEST;
                continue;
            }

            vec.Normalize();
            fDot = Vector3.Dot(vec, cForward);
            if (fDot < 0)
            {
                m_AvoidTargets[i].m_eState = Obstacle.eState.OUTSIDE_TEST;
                continue;
            }
            m_AvoidTargets[i].m_eState = Obstacle.eState.INSIDE_TEST;
            float fProjDist = fDist * fDot;
            float fDotDist = Mathf.Sqrt(fDist * fDist - fProjDist * fProjDist);
            if (fDotDist > m_AvoidTargets[i].m_fRadius + data.f_Radius)
            {
                continue;
            }

            return true;


        }
        return false;
    }


    static public bool CollisionAvoid(EnemyAIData01 data)
    {
        List<Obstacle> m_AvoidTargets = sMMain.Instance.GetObstacles();
        Transform ct = data.EnemySelf.transform;
        Vector3 cPos = ct.position;
        Vector3 cForward = ct.forward;
        data.v3_CurrentVector = cForward;
        Vector3 vec;
        float fFinalDotDist = 1.0f;
        float fFinalProjDist;
        Vector3 vFinalVec = Vector3.forward;
        Obstacle oFinal = null;
        float fDist = 0.0f;
        float fDot = 0.0f;
        float fFinalDot = 0.0f;
        int iCount = m_AvoidTargets.Count;
        float fFinalR = 1.0f;
        float fMinDist = 10000.0f;
        for (int i = 0; i < iCount; i++)
        {
            vec = m_AvoidTargets[i].transform.position - cPos;
            vec.y = 0.0f;
            fDist = vec.magnitude;
            if (fDist > data.f_ProbeLength + m_AvoidTargets[i].m_fRadius)
            {
                m_AvoidTargets[i].m_eState = Obstacle.eState.OUTSIDE_TEST;
                continue;
            }

            vec.Normalize();
            fDot = Vector3.Dot(vec, cForward);
            if (fDot < 0)
            {
                m_AvoidTargets[i].m_eState = Obstacle.eState.OUTSIDE_TEST;
                continue;
            }
            else if (fDot > 1.0f)
            {
                fDot = 1.0f;
            }
            m_AvoidTargets[i].m_eState = Obstacle.eState.INSIDE_TEST;
            float fProjDist = fDist * fDot;
            float fDotDist = Mathf.Sqrt(fDist * fDist - fProjDist * fProjDist);
            float fR = m_AvoidTargets[i].m_fRadius + data.f_Radius;
            if (fDotDist > fR)
            {
                continue;
            }

            if (fDist < fMinDist)
            {
                fMinDist = fDist;
                fFinalDotDist = fDotDist;
                fFinalProjDist = fProjDist;
                vFinalVec = vec;
                oFinal = m_AvoidTargets[i];
                fFinalDot = fDot;
                fFinalR = fR;
            }

        }

        if (oFinal != null)
        {
            Vector3 vCross = Vector3.Cross(cForward, vFinalVec);
            float fTurnMag = 1.0f - (fFinalDotDist / fFinalR);//Mathf.Sqrt(1.0f - fFinalDot * fFinalDot);
            if (vCross.y > 0.0f)
            {
                fTurnMag = -fTurnMag;
            }
            data.f_TempTurnForce = fTurnMag;

            float fTotalLen = data.f_ProbeLength + oFinal.m_fRadius;
            float fRatio = fMinDist / fTotalLen;
            if (fRatio > 1.0f)
            {
                fRatio = 1.0f;
            }
            fRatio = 1.0f - fRatio;
            data.f_MoveForce = -fRatio;
            oFinal.m_eState = Obstacle.eState.COL_TEST;
            data.b_Col = true;
            data.b_Move = true;
            return true;
        }
        data.b_Col = false;
        return false;
    }

    static public bool Flee(EnemyAIData01 data)
    {
        Vector3 cPos = data.EnemySelf.transform.position;
        Vector3 vec = data.v3_Target - cPos;
        vec.y = 0.0f;
        float fDist = vec.magnitude;
        data.f_TempTurnForce = 0.0f;
        if (data.f_ProbeLength < fDist)
        {
            if (data.f_Speed > 0.01f)
            {
                data.f_MoveForce = -1.0f;
            }

            data.b_Move = true;
            return false;
        }

        Vector3 vf = data.EnemySelf.transform.forward;
        Vector3 vr = data.EnemySelf.transform.right;
        data.v3_CurrentVector = vf;
        vec.Normalize();
        float fDotF = Vector3.Dot(vf, vec);
        if (fDotF < -0.96f)
        {
            fDotF = -1.0f;
            data.v3_CurrentVector = -vec;
            //  data.m_Go.transform.forward = -vec;
            data.f_TempTurnForce = 0.0f;
            data.f_Rot = 0.0f;
        }
        else
        {
            if (fDotF > 1.0f)
            {
                fDotF = 1.0f;
            }
            float fDotR = Vector3.Dot(vr, vec);

            if (fDotF > 0.0f)
            {

                if (fDotR > 0.0f)
                {
                    fDotR = 1.0f;
                }
                else
                {
                    fDotR = -1.0f;
                }

            }
            data.f_TempTurnForce = -fDotR;

            // data.m_fTempTurnForce *= 0.1f;


        }

        data.f_MoveForce = -fDotF;
        data.b_Move = true;
        return true;
    }

    //static public bool Seek(EnemyAIData01 data)
    //{
    //    Vector3 cPos = data.EnemySelf.transform.position;
    //    Vector3 vec = data.v3_Target - cPos;
    //    vec.y = 0.0f;
    //    float fDist = vec.magnitude;
    //    if (fDist < data.f_Speed + 0.001f)
    //    {
    //        Vector3 vFinal = data.v3_Target;
    //        vFinal.y = cPos.y;
    //        data.EnemySelf.transform.position = vFinal;
    //        data.f_MoveForce = 0.0f;
    //        data.f_TempTurnForce = 0.0f;
    //        data.f_Speed = 0.0f;
    //        data.b_Move = false;
    //        return false;
    //    }
    //    Vector3 vf = data.EnemySelf.transform.forward;
    //    Vector3 vr = data.EnemySelf.transform.right;
    //    data.v3_CurrentVector = vf;
    //    vec.Normalize();
    //    float fDotF = Vector3.Dot(vf, vec);
    //    if (fDotF > 0.96f)
    //    {
    //        fDotF = 1.0f;
    //        data.v3_CurrentVector = vec;
    //        data.f_TempTurnForce = 0.0f;
    //        data.f_Rot = 0.0f;
    //    }
    //    else
    //    {
    //        if (fDotF < -1.0f)
    //        {
    //            fDotF = -1.0f;
    //        }
    //        float fDotR = Vector3.Dot(vr, vec);

    //        if (fDotF < 0.0f)
    //        {

    //            if (fDotR > 0.0f)
    //            {
    //                fDotR = 1.0f;
    //            }
    //            else
    //            {
    //                fDotR = -1.0f;
    //            }

    //        }
    //        if (fDist < 3.0f)
    //        {
    //            fDotR *= ((1.0f - fDist / 3.0f) + 1.0f);
    //        }
    //        data.f_TempTurnForce = fDotR;

    //    }

    //    if (fDist < 3.0f)
    //    {
    //        if (data.f_Speed > 0.1f)
    //        {
    //            data.f_MoveForce = -(1.0f - fDist / 3.0f) * 5.0f;
    //        }
    //        else
    //        {
    //            data.f_MoveForce = fDotF * 100.0f;
    //        }

    //    }
    //    else
    //    {
    //        data.f_MoveForce = 100.0f;
    //    }



    //    data.b_Move = true;
    //    return true;
    //}
    static public bool Seek(EnemyAIData01 data)
    {
        Vector3 cPos = data.EnemySelf.transform.position;
        Vector3 vec = data.v3_Target - cPos;
        vec.y = 0.0f;
        float fDist = vec.magnitude;
        if (fDist < data.f_Speed + 0.001f)
        {
            Vector3 vFinal = data.v3_Target;
            vFinal.y = cPos.y;
            data.EnemySelf.transform.position = vFinal;
            data.f_MoveForce = 0.0f;
            data.f_TempTurnForce = 0.0f;
            data.f_Speed = 0.0f;
            data.b_Move = false;
            return false;
        }
        Vector3 vf = data.EnemySelf.transform.forward;
        Vector3 vr = data.EnemySelf.transform.right;
        data.v3_CurrentVector = vf;
        vec.Normalize();
        float fDotF = Vector3.Dot(vf, vec);
        if (fDotF > 0.96f)
        {
            fDotF = 1.0f;
            data.v3_CurrentVector = vec;
            data.f_TempTurnForce = 0.0f;
            data.f_Rot = 0.0f;
        }
        else
        {
            if (fDotF < -1.0f)
            {
                fDotF = -1.0f;
            }
            float fDotR = Vector3.Dot(vr, vec);

            if (fDotF < 0.0f)
            {

                if (fDotR > 0.0f)
                {
                    fDotR = 1.0f;
                }
                else
                {
                    fDotR = -1.0f;
                }

            }
            if (fDist < 3.0f)
            {
                fDotR *= ((1.0f - fDist / 3.0f) + 1.0f);
            }
            data.f_TempTurnForce = fDotR;

        }

        if (fDist < 3.0f)
        {
            Debug.Log(data.f_Speed);
            if (data.f_Speed > 0.1f)
            {
                data.f_MoveForce = -(1.0f - fDist / 3.0f) * 5.0f;
            }
            else
            {
                data.f_MoveForce = fDotF * 100.0f;
            }

        }
        else
        {
            data.f_MoveForce = 100.0f;
        }



        data.b_Move = true;
        return true;
    }
}
