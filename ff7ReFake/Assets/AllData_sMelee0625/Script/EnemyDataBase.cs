﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyDataBase : MonoBehaviour
{
    protected enum eFSMState
    {
        UnFight,
        Idle,
        TurnAroundToTarget,
        ChangePoint,
        DropToPoint,
        Chase,
        LookAndRAttack,
        SRoll,
        BRoll,
        StopToIdle,
        NextOne,
        ReturnToIdle,
        Shield,
        TurnToBBAttack,
        BBA,
        BBACheckAttackTrue,
        StartToShoot,
        StartToLaser,
        Shooting,
        Laser,
        MeleeExclusion,
        ChaseAndMAttackAndBack,
        OHMYGOD,
        Back,
        ByHit,
        Dead,
        Finish,
        TargetDead
    }
    protected eFSMState CurrentState;
    public EnemyAIData01 mData;
    protected float f_CurrentTime;
    protected bool b_BeHit;//是否受到傷害    
    protected float f_IdleTime;
    protected GameObject CurrentEnemyTarget;
    [SerializeField] protected CheckPoint checkPoint;
    public State TargetNowCurrentState;
    protected int i_tmp_Choose;//Int暫存值
    public Fighter Ft;
    public ValueEvent onUV;
    public RayCastWeapon RCW;
    public ObjectEvent onDead;
    protected Animator Anim;
    public ParticleSystem Shield;
    public GameObject objAllMusicManager;
    protected AllMusicManager scAllMusicManager;

    [SerializeField] private List<Renderer> renderers = new List<Renderer>();
    [SerializeField] private float dissolveDeltaAmount;
    public float DissolveAmount = 1;
    [SerializeField] protected float MaxHP;
    public ObjectEvent onFinishDisappear;
    public ValueEvent onGainEXP;
    [SerializeField] protected int EXPGained;


    protected IEnumerator Disslove(int state)
    {
        while (Mathf.Abs(DissolveAmount - state) > 0.001f)
        {
            //Debug.Log(gameObject.name + "dissolveAmount: " + dissolveAmount);
            foreach (Renderer renderer in renderers) renderer.material.SetFloat("_DissolveAmount", DissolveAmount);
            if (DissolveAmount < state) DissolveAmount += dissolveDeltaAmount;
            else DissolveAmount -= dissolveDeltaAmount;
            yield return 0;
        }
        foreach (Renderer renderer in renderers) renderer.material.SetFloat("_DissloveAmount", state);
        if (state == 1 && Mathf.Abs(DissolveAmount - state) < 0.001f && mData.b_IsDead) onFinishDisappear.Invoke(gameObject);
    }

    protected IEnumerator StartCombatState(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        CurrentState = eFSMState.Idle;
        Anim.SetBool("OnFight", true);
    }

    internal void BeHit()
    {
        //Debug.Log("behit!!!!!!!!!!!!!!!!!!!");
        b_BeHit = true;
    }
}
