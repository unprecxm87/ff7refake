﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossHairTarget : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform ori;
    public Transform CHTarget;

    Ray ray;
    RaycastHit hitInfo;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        ray.origin = ori.position;
        ray.direction = ori.forward;
        Physics.Raycast(ray, out hitInfo);
        CHTarget.position = hitInfo.point;
    }
}
