﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sMMain : MonoBehaviour
{
    public static sMMain Instance;

    private List<Obstacle> Obstacles;
    public GameObject Player;
    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        Obstacles = new List<Obstacle>();
        GameObject[] gos = GameObject.FindGameObjectsWithTag("Enemy");
        if (gos != null || gos.Length > 0)
        {
            //Debug.Log(gos.Length);
            foreach (GameObject go in gos)
            {
                Obstacles.Add(go.GetComponent<Obstacle>());
            }
        }
    }

    public GameObject GetPlayer()
    {
        return Player;
    }

    public List<Obstacle> GetObstacles()
    {
        return Obstacles;
    }

    // Update is called once per frame
    void Update()
    {

    }
}