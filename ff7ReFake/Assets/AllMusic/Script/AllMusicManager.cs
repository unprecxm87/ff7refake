﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AllMusicManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject MainMenu_Background;
    public GameObject MainMenu_Select;
    public GameObject MainScene_KennyWalk;
    public GameObject MainScene_SwordSlashHit1;
    public GameObject MainScene_SwordSlashHit2;
    public GameObject MainScene_Background;
    public GameObject MainScene_SwordWave;
    public GameObject MainScene_SwordWave123;
    public GameObject MainScene_HealMagic;
    public GameObject MainScene_thunderMagic;
    public GameObject MainScene_MeleeKick;
    public GameObject MainScene_MeleeShooting;
    public GameObject MainScene_MeleeDead;
    public GameObject MainScene_UISelect;
    public GameObject MainScene_UISelectInto;
    public GameObject MainScene_RankUp;
    public GameObject MainScene_DrawMagic;
    public GameObject MainScene_Combo0;
    public GameObject MainScene_Combo1;
    public GameObject MainScene_Combo2;
    public GameObject MainScene_Unarm;
    public GameObject MainScene_Equip;
    public GameObject MainScene_Dodge;
    public GameObject MainScene_MasterLaserBomb;
    public GameObject MainScene_MasterShot;
    public GameObject MainScene_MasterDrop;
    public GameObject MainScene_BossRedShield;
    public GameObject MainScene_BossBBAShield;

    void Awake()
    {
        Scene scene = SceneManager.GetActiveScene();
        if(scene.name == "MainScene")
        {
            Instantiate(MainScene_Background);
        }
        else if(scene.name == "MainMenu")
        {
            Instantiate(MainMenu_Background);
        }
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Normal(GameObject MusicPrefabName)
    {
        Instantiate(MusicPrefabName);
    }

    public IEnumerator WaitAndPlay(float seconds, GameObject sound)
    {
        yield return new WaitForSeconds(seconds);
        Instantiate(sound);
    }
}
