#ifndef SimpleURPToonLitOutlineExample_Shared_Include
#define SimpleURPToonLitOutlineExample_Shared_Include
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
struct Attributes
{
    float3 positionOS   : POSITION;
    half3 normalOS     : NORMAL;
    half4 tangentOS    : TANGENT;
    float2 uv           : TEXCOORD0;
};
struct Varyings
{
    float2 uv                       : TEXCOORD0;
    float4 positionWSAndFogFactor   : TEXCOORD2; //worldPosition+Fog
    half3 normalWS                 : TEXCOORD3;

#ifdef _MAIN_LIGHT_SHADOWS
    float4 shadowCoord              : TEXCOORD6; // 計算光照到每片貼圖產生的陰影
#endif
    float4 positionCS               : SV_POSITION;
};

////////////////////////////////////
// TOONSHADER的Properties
///////////////////////////////////
sampler2D _BaseMap; 
sampler2D _EmissionMap;

CBUFFER_START(UnityPerMaterial)

    // base color
    float4 _BaseMap_ST;
    half4 _BaseColor;

    // alpha
    float _UseAlphaClipping;
    half _Cutoff;

    //lighting
    half3 _IndirectLightConstColor;
    half _IndirectLightMultiplier;
    half _DirectLightMultiplier;
    half _CelShadeMidPoint;
    half _CelShadeSoftness;

    // shadow mapping
    half _ReceiveShadowMappingAmount;

    //emission
    float _UseEmission;
    half3 _EmissionColor;
    half3 _EmissionMapChannelMask;

    // outline
    float _OutlineWidth;
    half3 _OutlineColor;

CBUFFER_END

//只在此HLSL運算的參數不用丟CBUFFER
half3 _LightDirection;

struct SurfaceData
{
    half3 albedo;
    half  alpha;
    half3 emission;
};
struct LightingData
{
    half3 normalWS;
    float3 positionWS;
    half3 viewDirectionWS;
    float4 shadowCoord;
};

//////////////////////
// 外描邊+影子開關
//////////////////////

struct VertexShaderWorkSetting
{
    bool isOutline;
    bool applyShadow;
};
VertexShaderWorkSetting GetOutlineShadowSetting()
{
    VertexShaderWorkSetting output;
    output.isOutline = false;
    output.applyShadow = false;
    return output;
}

///////////////////////////////
// vertex shader 開始
///////////////////////////////

float3 TransformPositionOSToOutlinePositionOS(Attributes input)//外描邊位置定義
{
    float3 outlineNormalOSUnitVector = normalize(input.normalOS); //外描邊的向量=當前對象的單位法向量
    return input.positionOS + outlineNormalOSUnitVector * _OutlineWidth; //回傳當前對象的頂點座標+外描邊向量*外描邊高度
}

// isOutline = false進行一般的mvp矩陣轉換
// isOutline = true進行一般的mvp矩陣轉換+把normalOS頂點往外推
Varyings VertexShaderWork(Attributes input, VertexShaderWorkSetting setting)//setting = VertexShaderWorkSetting output
{
    Varyings output;

    //外描邊座標
    if(setting.isOutline)
    {
        input.positionOS = TransformPositionOSToOutlinePositionOS(input);
    }
    //定義頂點座標(VertexPositionInputs在CORE裡面)
    VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS);

    //定義N,B,T(VertexNormalInputs在CORE裡面)
    VertexNormalInputs vertexNormalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

    // 計算霧化
    float fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

    // 貼圖座標
    output.uv = TRANSFORM_TEX(input.uv,_BaseMap);

    //FOG感覺用不到
    output.positionWSAndFogFactor = float4(vertexInput.positionWS, fogFactor);
    output.normalWS = vertexNormalInput.normalWS;

#ifdef _MAIN_LIGHT_SHADOWS
    output.shadowCoord = GetShadowCoord(vertexInput);
#endif
    output.positionCS = vertexInput.positionCS;

    // ShadowCaster pass needs special process to clipPos, else shadow artifact will appear
    //-------------------------------------------------------------------------------------
    //產生陰影
    if(setting.applyShadow)
    {
        //參考 ShadowCasterPass.hlsl 裡的 GetShadowPositionHClip()
        float3 positionWS = vertexInput.positionWS;
        float3 normalWS = vertexNormalInput.normalWS;
        float4 positionCS = TransformWorldToHClip(ApplyShadowBias(positionWS, normalWS, _LightDirection));

        #if UNITY_REVERSED_Z
        positionCS.z = min(positionCS.z, positionCS.w * UNITY_NEAR_CLIP_VALUE);
        #else
        positionCS.z = max(positionCS.z, positionCS.w * UNITY_NEAR_CLIP_VALUE);
        #endif
        output.positionCS = positionCS;
    }
    //--------------------------------------------------------------------------------------    

    return output;
}
/////////////////////////////VERTEX SHADER結束////////////////////////////////

////////////////////////////
// FRAGMENT SHADER開始
// 貼圖運算
////////////////////////////
half4 GetFinalBaseColor(Varyings input)
{
    return tex2D(_BaseMap, input.uv) * _BaseColor;
}
half3 GetFinalEmissionColor(Varyings input)
{
    if(_UseEmission)
    {
        return tex2D(_EmissionMap, input.uv).rgb * _EmissionColor.rgb * _EmissionMapChannelMask;
    }

    return 0;
}
void DoClipTestToTargetAlphaValue(half alpha) 
{
    if(_UseAlphaClipping)
    {   
        clip(alpha - _Cutoff);
    }
}
//貼圖重新計算
SurfaceData InitializeSurfaceData(Varyings input)
{
    SurfaceData output;

    // albedo & alpha
    float4 baseColorFinal = GetFinalBaseColor(input);
    output.albedo = baseColorFinal.rgb;
    output.alpha = baseColorFinal.a;
    DoClipTestToTargetAlphaValue(output.alpha);//early exit if possible

    //emission
    output.emission = GetFinalEmissionColor(input);

    return output;
}

/////////////////////////
// 燈光運算
/////////////////////////

#include "SimpleURPToonLitOutlineExample_LightingEquation.hlsl"

half3 ShadeAllLights(SurfaceData surfaceData, LightingData lightingData)
{
    // 平行光
    half3 indirectResult = ShadeGI(surfaceData, lightingData);

    Light mainLight;
#ifdef _MAIN_LIGHT_SHADOWS
    mainLight = GetMainLight(lightingData.shadowCoord);
#else
    mainLight = GetMainLight();
#endif 

    // 主光
    half3 mainLightResult = ShadeMainLight(surfaceData, lightingData, mainLight);
    //==============================================================================================
    // 動態光
    half3 additionalLightSumResult = 0;

#ifdef _ADDITIONAL_LIGHTS
    int additionalLightsCount = GetAdditionalLightsCount();
    for (int i = 0; i < additionalLightsCount; ++i)
    {
        
        Light light = GetAdditionalLight(i, lightingData.positionWS);

        additionalLightSumResult += ShadeAdditionalLight(surfaceData, lightingData, light);
    }
#endif
    //==============================================================================================
    // emission
    half3 emissionResult = surfaceData.emission;

    return CompositeAllLightResults(indirectResult, mainLightResult, additionalLightSumResult, emissionResult);
}

half3 ConvertSurfaceColorToOutlineColor(half3 originalSurfaceColor)
{
    return originalSurfaceColor * _OutlineColor;
}

/////////////////////////
// 最後顏色輸出
/////////////////////////
half4 ShadeFinalColor(Varyings input, bool isOutline)
{
    //照明參數都丟進來

    //SurfaceData:
    SurfaceData surfaceData = InitializeSurfaceData(input);

    //LightingData:
    LightingData lightingData;
    lightingData.positionWS = input.positionWSAndFogFactor.xyz;//WORLDPOSITION+FOG
    lightingData.viewDirectionWS = SafeNormalize(GetCameraPositionWS() - lightingData.positionWS);//V = (H-L)
    lightingData.normalWS = normalize(input.normalWS); //WORLDNORMAL

#ifdef _MAIN_LIGHT_SHADOWS
    lightingData.shadowCoord = input.shadowCoord;//影子加進來
#endif
 
    //////////////////////////////
    // 燈光運算
    //////////////////////////////

    half3 color = ShadeAllLights(surfaceData, lightingData);

    //////////////////////////////
    // 結束
    /////////////////////////////

    // outline顏色
    if(isOutline)
    {
        color = ConvertSurfaceColorToOutlineColor(color);
    }

    // 加上fog
    half fogFactor = input.positionWSAndFogFactor.w;
    color = MixFog(color, fogFactor);

    //最終顏色
    return half4(color,1);
}
//ALPHACLIP
half4 BaseColorAlphaClipTest(Varyings input)
{
    DoClipTestToTargetAlphaValue(GetFinalBaseColor(input).a);
    return 0;
}

#endif
