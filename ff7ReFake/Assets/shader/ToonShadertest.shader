﻿Shader "Universal Render Pipeline/ToonShaderWithOutLine"
{
    Properties
    {
        [Header(Base Color)]
        [HDR][MainColor]_BaseColor("MainColor", Color) = (1,1,1,1)
        [MainTexture]_BaseMap("BaseMap", 2D) = "white" {}

        [Header(Alpha)]
        [Toggle]_UseAlphaClipping("AlphaClipON", Float) = 1//低於多少alpha會裁切掉
        _Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5//alpha低於0.5裁切掉

        [Header(Lighting)]
        _IndirectLightConstColor("MainLightColor", Color) = (0.5,0.5,0.5,1)
        _IndirectLightMultiplier("MLCPower", Range(0,1)) = 1
        _DirectLightMultiplier("Brightness", Range(0,1)) = 0.25
        _CelShadeMidPoint("Shadow", Range(-1,1)) = -.5
        _CelShadeSoftness("ShadowSmooth", Range(0,1)) = 0.05

        [Header(Shadow mapping)]
        _ReceiveShadowMappingAmount("ReceiveShadowMappingAmount", Range(0,1)) = 0.5

        [Header(Emission)]
        [Toggle]_UseEmission("Emission ON", Float) = 0
        [HDR] _EmissionColor("EmissionColor", Color) = (0,0,0)
        _EmissionMap("EmissionMap", 2D) = "white" {}
        _EmissionMapChannelMask("EmissionMapMask", Vector) = (1,1,1,1)

        /*[Header(Outline)]
        _OutlineWidth("OutlineWidth", Range(0, 0.1)) = 0.0015
        _OutlineColor("OutlineColor", Color) = (0.3,0.3,0.3,1)*/
    }
        SubShader
        {
            Tags
            {
                "RenderPipeline" = "UniversalRenderPipeline"
            }
            Pass
            {
                Name "SurfaceColor"
                Tags
                {
                    "LightMode" = "UniversalForward"
                }

            HLSLPROGRAM
                #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
                #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
                #pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
                #pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
                #pragma multi_compile _ _SHADOWS_SOFT
                #pragma multi_compile_fog

                #include "SimpleURPToonLitOutlineExample_Shared.hlsl"

                #pragma vertex BaseColorPassVertex
                #pragma fragment BaseColorPassFragment

                Varyings BaseColorPassVertex(Attributes input)
                {
                    VertexShaderWorkSetting setting = GetOutlineShadowSetting();

                    return VertexShaderWork(input, setting);
                }

                half4 BaseColorPassFragment(Varyings input) : SV_TARGET
                {
                    return ShadeFinalColor(input, false);
                }

                ENDHLSL
            }

            //Pass
            //{
            //    Name "Outline"
            //    Cull Front // 遮蔽攝影機照射面

            //    HLSLPROGRAM


            //    #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
            //    #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
            //    #pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
            //    #pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
            //    #pragma multi_compile _ _SHADOWS_SOFT
            //    #pragma multi_compile_fog


            //    #include "SimpleURPToonLitOutlineExample_Shared.hlsl"

            //    #pragma vertex OutlinePassVertex
            //    #pragma fragment OutlinePassFragment

            //    Varyings OutlinePassVertex(Attributes input)
            //    {
            //        VertexShaderWorkSetting setting = GetOutlineShadowSetting();

            //        setting.isOutline = true;

            //        return VertexShaderWork(input, setting);
            //    }

            //    half4 OutlinePassFragment(Varyings input) : SV_TARGET
            //    {
            //        
            //        return ShadeFinalColor(input, true);
            //    }

            //    ENDHLSL
            //}


                
            Pass
            {
                Name "ShadowCaster"
                Tags{"LightMode" = "ShadowCaster"}

                ColorMask 0//遮蔽顏色

                HLSLPROGRAM

                #pragma vertex ShadowCasterPassVertex
                #pragma fragment ShadowCasterPassFragment

                #include "SimpleURPToonLitOutlineExample_Shared.hlsl"

                Varyings ShadowCasterPassVertex(Attributes input)
                {
                    VertexShaderWorkSetting setting = GetOutlineShadowSetting();

                    setting.isOutline = false; 
                    setting.applyShadow = true;

                    return VertexShaderWork(input, setting);
                }

                half4 ShadowCasterPassFragment(Varyings input) : SV_TARGET
                {
                    return BaseColorAlphaClipTest(input);
                }
                ENDHLSL
            }
        }
}