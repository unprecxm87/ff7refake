﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class onTriggerEnter : MonoBehaviour
{
    public UnityEvent ue;
    public GameObject enemy;

    public bool b_isHitByBoos;
    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            Debug.Log("2");
            Fighter Ft = other.gameObject.GetComponent<Fighter>();
            Ft.BeHit(1);
            ue.Invoke();
            b_isHitByBoos = true;
        }
    }
}
