﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.UI;
using UnityEngine;
using UnityEngine.Assertions.Must;
//using UnityEngine.WSA;
using UnityEngine.Events;

public class BossFSM : EnemyDataBase
{
    public BoxCollider NormalCollider;
    public GameObject tmp_Target;
    public float TurnMoveToTarget_TurnSpeed;
    public float TurnMoveToTarget_MoveSpeed;
    public ParticleSystem ShieldLaser;
    public ParticleSystem ShieldThis;
    public ParticleSystem psRoll1;
    public ParticleSystem psRoll2;
    public ParticleSystem psRedShield;
    public ParticleSystem psBBA;
    public ValueEvent VEForShield;
    public UnityEvent VEFill;
    public GameObject EYE1;
    public GameObject EYE2;
    public GameObject BossBBATarget;


    AnimatorStateInfo MainCurrentBaseStage;
    bool b_isTurnCurrect;
    bool b_istmpTargetOK;
    bool b_isHitHit;
    int i_idleFrequency;
    int i_tmp_ChooseTurnAroundToTarget;
    int i_tmp_BRollWallNumber;
    int i_tmp_BRollCN;
    int i_SRollFrequency;
    int i_tmp_OHMYGOD;
    bool b_isRollPS;
    bool b_isAngry;
    bool b_FirstTime;
    bool b_isAngryAndBigAttack;
    
    int i_tmp_BBA;
    bool b_tmpBBA;
    Rigidbody rg;

    private void OnEnable()
    {
        Anim = GetComponent<Animator>();
        b_BeHit = false;
        f_CurrentTime = 0.0f;
        CurrentEnemyTarget = null;
        CurrentState = eFSMState.UnFight;
        i_idleFrequency = 0;
        b_isAngry = false;
        b_FirstTime = false;
        b_isAngryAndBigAttack = false;
        rg = GetComponent<Rigidbody>();
        mData.f_Hp = MaxHP;
        DissolveAmount = 1;

        b_isTurnCurrect = false;
        b_istmpTargetOK = false;
        b_isHitHit = false;
        i_idleFrequency = 0;
        i_tmp_ChooseTurnAroundToTarget = 0;
        i_tmp_BRollWallNumber = 0;
        i_tmp_BRollCN = 0;
        i_SRollFrequency = 0;
        i_tmp_OHMYGOD = 0;
        b_isRollPS = false;
        b_isAngry = false;
        b_FirstTime = false;
        b_isAngryAndBigAttack = false;

        i_tmp_BBA = 0;
        b_tmpBBA = false;
    }

    void Start()
    {
        scAllMusicManager = objAllMusicManager.GetComponent<AllMusicManager>();
        //Anim = GetComponent<Animator>();
        //b_BeHit = false;
        //f_CurrentTime = 0.0f;
        //CurrentEnemyTarget = null;
        //CurrentState = eFSMState.UnFight;
        //i_idleFrequency = 0;
        //b_isAngry = false;
        //b_FirstTime = false;
        //b_isAngryAndBigAttack = false;
        //rg = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!b_isAngry && mData.f_Hp <= 300)
        {
            int intensity = 2;
            float f = Mathf.Pow(2, intensity);
            Color col = new Color(255 * f, 0, 0);
            EYE1.transform.Translate(0.002f, 0, 0);
            EYE1.transform.Rotate(0, 0, 45, Space.Self);
            Renderer EYE1M = EYE1.GetComponent<Renderer>();
            EYE1M.material.SetColor("Color_B9138E63", col); //.color = Color.red;
            EYE2.transform.Translate(-0.002f, 0, 0);
            EYE2.transform.Rotate(0, 0, -45, Space.Self);
            Renderer EYE2M = EYE2.GetComponent<Renderer>();
            EYE2M.material.SetColor("Color_B9138E63", col); //.color = Color.red;
            b_isAngry = true;
        }
        if (b_isAngry && !b_isAngryAndBigAttack)
        {
            CurrentState = eFSMState.TurnToBBAttack;
            f_CurrentTime = 0;
            b_isAngryAndBigAttack = true;
            return;
        }
        if (CurrentState != eFSMState.Shield && CurrentState != eFSMState.UnFight && b_FirstTime == false)
        {
            if (f_CurrentTime >= 3.0f)
            {
                psRedShield.Play();
                scAllMusicManager.Normal(scAllMusicManager.MainScene_BossRedShield);
                b_FirstTime = true;
                f_CurrentTime = 0;
                return;
            }
            else
            {
                f_CurrentTime += Time.deltaTime;
                return;
            }
        }
        //Debug.Log("Current State " + CurrentState);
        if(CurrentState == eFSMState.Idle)
        {
            CheckTarget();
            Func_BeHit();
            if(mData.EnemySelf.transform.position.y>=13.79)
            {
                return;
            }
            MainCurrentBaseStage = Anim.GetCurrentAnimatorStateInfo(0);
            if(!MainCurrentBaseStage.IsName("anim_Idle_Loop_S"))
            {
                return;
            }
            f_IdleTime = 1;
            
            if (f_CurrentTime >= f_IdleTime)
            {
                i_idleFrequency += 1;
                if (i_idleFrequency == 5)
                {
                    CurrentState = eFSMState.SRoll;
                    b_isRollPS = false;
                    b_istmpTargetOK = false;
                    f_CurrentTime = 0.0f;
                    i_idleFrequency = 0;
                    return;
                }
                i_tmp_Choose = UnityEngine.Random.Range(2, 7);
                if (i_tmp_Choose == 0 || i_tmp_Choose == 1)
                {
                    CurrentState = eFSMState.ChangePoint;
                    f_CurrentTime = 0.0f;
                    return;
                }
                else
                {
                    CurrentState = eFSMState.TurnAroundToTarget;
                    f_CurrentTime = 0.0f;
                    return;
                }
            }
            else
            {
                f_CurrentTime += Time.deltaTime;
                return;
            }

        }
        else if(CurrentState == eFSMState.TurnAroundToTarget)
        {
            CheckTarget();
            Func_BeHit();
            TurnToTarget(ref b_isTurnCurrect, mData.TargetObject);
            if(b_isTurnCurrect)
            {
                i_tmp_ChooseTurnAroundToTarget = Random.Range(0, 3);
                if (i_tmp_ChooseTurnAroundToTarget == 0 || i_tmp_ChooseTurnAroundToTarget == 1)
                {
                    CurrentState = eFSMState.SRoll;
                    b_istmpTargetOK = false;
                    f_CurrentTime = 0.0f;
                    b_isTurnCurrect = false;
                    i_idleFrequency = 0;
                    b_isRollPS = false;
                    return;
                }
                else
                {
                    CurrentState = eFSMState.Idle;
                    f_CurrentTime = 0.0f;
                    return;
                }
            }
            else
            {
                return;
            }
        }
        else if (CurrentState == eFSMState.SRoll)
        {
            CheckTarget();
            Func_BeHit();
            if(i_SRollFrequency>=5)
            {
                CurrentState = eFSMState.BRoll;
                b_istmpTargetOK = false;
                i_tmp_BRollWallNumber = Random.Range(5, 11);
                i_SRollFrequency = 0;
                return;
            }
            if (f_CurrentTime>=2.0f)
            {
                TurnMoveToTarget(tmp_Target, ref b_isHitHit);
                Debug.Log("1");
                Vector3 vpos = mData.TargetObject.transform.position - mData.EnemySelf.transform.position;
                vpos.y = 0;
                float dis = vpos.magnitude;
                if(dis <= 3f)
                {
                    Debug.Log("HITHITHITHITHITHIHTITITITI");
                    Ft.BeHit(100);
                    Anim.SetBool("Roll_Anim", false);
                    psRoll1.Stop();
                    psRoll2.Stop();
                    NormalCollider.size = new Vector3(0.145f, 0.1f, 0.145f);
                    NormalCollider.center = new Vector3(-0.001f, 0.0498f, -0.009f);
                    CurrentState = eFSMState.StopToIdle;
                    i_SRollFrequency += 1;
                    i_tmp_OHMYGOD = 2;
                    f_CurrentTime = 0;
                    return;
                }
                if(b_isHitHit)
                {
                    Anim.SetBool("Roll_Anim", false);
                    psRoll1.Stop();
                    psRoll2.Stop();
                    NormalCollider.size = new Vector3(0.145f, 0.1f, 0.145f);
                    NormalCollider.center = new Vector3(-0.001f, 0.0498f, -0.009f);
                    CurrentState = eFSMState.StopToIdle;
                    i_SRollFrequency += 1;
                    i_tmp_OHMYGOD = 2;
                    f_CurrentTime = 0;
                    return;
                }
                return;
            }
            else if(f_CurrentTime >= 1.8f)
            {
                if (!b_istmpTargetOK)
                {
                    tmp_Target.transform.position = mData.TargetObject.transform.position;
                    b_istmpTargetOK = true;
                    NormalCollider.size = new Vector3(0.1f, 0.1f, 0.1f);
                    NormalCollider.center = new Vector3(0, 0.0498f, 0);
                    return;
                }
                f_CurrentTime += Time.deltaTime;
                return;
            }
            else if(f_CurrentTime >= 1.3f)
            {
                if(!b_isRollPS)
                {
                    psRoll1.Play();
                    psRoll2.Play();
                    f_CurrentTime += Time.deltaTime;
                    b_isRollPS = true;
                    return;
                }
                f_CurrentTime += Time.deltaTime;
                return;
            }
            else
            {
                Anim.SetBool("Roll_Anim", true);
                transform.LookAt(mData.TargetObject.transform.position);
                f_CurrentTime += Time.deltaTime;
                return;
            }
        }
        else if (CurrentState == eFSMState.BRoll)
        {
            CheckTarget();
            Func_BeHit();
            if(i_tmp_BRollWallNumber > i_tmp_BRollCN)
            {
                if (f_CurrentTime >= 2.0f)
                {
                    TurnMoveToTarget(tmp_Target, ref b_isHitHit);
                    Vector3 vpos = mData.TargetObject.transform.position - mData.EnemySelf.transform.position;
                    vpos.y = 0;
                    float dis = vpos.magnitude;
                    if (dis <= 2f)
                    {
                        Ft.BeHit(200);
                        Anim.SetBool("Roll_Anim", false);
                        psRoll1.Stop();
                        psRoll2.Stop();
                        NormalCollider.size = new Vector3(0.145f, 0.1f, 0.145f);
                        NormalCollider.center = new Vector3(-0.001f, 0.0498f, -0.009f);
                        CurrentState = eFSMState.StopToIdle;
                        i_SRollFrequency += 1;
                        i_tmp_OHMYGOD = 5;
                        f_CurrentTime = 0;
                        return;
                    }
                    if (b_isHitHit)
                    {
                        i_tmp_BRollCN += 1;
                        CurrentState = eFSMState.NextOne;
                        f_CurrentTime = 0;
                        return;
                    }
                    return;
                }
                else if (f_CurrentTime >= 1.95f)
                {
                    if (!b_istmpTargetOK)
                    {
                        tmp_Target.transform.position = mData.TargetObject.transform.position;
                        b_istmpTargetOK = true;
                        return;
                    }
                    f_CurrentTime += Time.deltaTime;
                    return;
                }
                else if (f_CurrentTime >= 1.3f)
                {
                    if (!b_isRollPS)
                    {
                        psRoll1.Play();
                        psRoll2.Play();
                        f_CurrentTime += Time.deltaTime;
                        return;
                    }
                    f_CurrentTime += Time.deltaTime;
                    return;
                }
                else
                {
                    Anim.SetBool("Roll_Anim", true);
                    transform.LookAt(mData.TargetObject.transform.position);
                    f_CurrentTime += Time.deltaTime;
                    return;
                }
            }
            else
            {
                Anim.SetBool("Roll_Anim", false);
                psRoll1.Stop();
                psRoll2.Stop();
                CurrentState = eFSMState.StopToIdle;
                i_SRollFrequency += 1;
                i_tmp_OHMYGOD = 5;
                f_CurrentTime = 0;
                return;
            }
        }
        else if (CurrentState == eFSMState.StopToIdle)
        {
            CheckTarget();
            Func_BeHit();
            MainCurrentBaseStage = Anim.GetCurrentAnimatorStateInfo(0);
            if (MainCurrentBaseStage.IsName("anim_Idle_Loop_S"))
            {
                CurrentState = eFSMState.OHMYGOD;
                return;
            }
            f_CurrentTime = 0.0f;
        }
        else if (CurrentState == eFSMState.NextOne)
        {
            if(f_CurrentTime>=0.5)
            {
                CurrentState = eFSMState.BRoll;
                f_CurrentTime = 0;
                return;
            }
            else
            {
                transform.LookAt(mData.TargetObject.transform.position);
                f_CurrentTime += Time.deltaTime;
                b_istmpTargetOK = false;
                return;
            }
        }
        else if (CurrentState == eFSMState.OHMYGOD)
        {
            CheckTarget();
            Func_BeHit();
            MainCurrentBaseStage = Anim.GetCurrentAnimatorStateInfo(0);
            Anim.SetBool("Open_Anim", false);
            if(MainCurrentBaseStage.IsName("anim_close") && MainCurrentBaseStage.normalizedTime >= 0.95)
            {
                if (f_CurrentTime >= i_tmp_OHMYGOD)
                {
                    Anim.SetBool("Open_Anim", true);
                    f_CurrentTime = 0;
                    CurrentState = eFSMState.ReturnToIdle;
                    return;
                }
                else
                {
                    f_CurrentTime += Time.deltaTime;
                    return;
                }
            }
            return;
        }
        else if (CurrentState == eFSMState.ReturnToIdle)
        {
            CheckTarget();
            Func_BeHit();
            if(f_CurrentTime>=3)
            {
                if (i_SRollFrequency == 2)
                {
                    CurrentState = eFSMState.Shield;
                    scAllMusicManager.Normal(scAllMusicManager.MainScene_BossBBAShield);
                    psRedShield.Clear();
                    psRedShield.Stop();
                    mData.f_ShieldHP = 200;
                    Debug.Log("shield");
                    VEFill.Invoke();
                    ShieldLaser.Play();
                    ShieldThis.Play();
                    b_istmpTargetOK = false;
                    f_CurrentTime = 0;
                    i_SRollFrequency += 1;
                    return;
                }
                psRedShield.Play();
                CurrentState = eFSMState.Idle;
                f_CurrentTime = 0;
                return;
            }
            else
            {
                f_CurrentTime += Time.deltaTime;
                return;
            }
        }
        else if(CurrentState == eFSMState.Shield)
        {
            CheckTarget();
            Func_BeHit();
            if (f_CurrentTime >= 10.0f)
            {
                Vector3 vpos = mData.EnemySelf.transform.position - mData.TargetObject.transform.position;
                vpos.y = 0;
                float dis1 = vpos.magnitude;
                if(dis1 <= 20.0f)
                {
                    Ft.BeHit(200);
                }
                CurrentState = eFSMState.Idle;
                psRedShield.Play();
                mData.f_ShieldHP = 0;
                VEForShield.Invoke(200);
                f_CurrentTime = 0.0f;
                return;
            }
            else
            {
                if(mData.f_ShieldHP<=0)
                {
                    ShieldLaser.Stop();
                    ShieldThis.Clear();
                    ShieldThis.Stop();
                    CurrentState = eFSMState.OHMYGOD;
                    f_CurrentTime = 0;
                    i_tmp_OHMYGOD = 6;
                    return;
                }
                f_CurrentTime += Time.deltaTime;
                return;
            }
        }
        else if (CurrentState == eFSMState.TurnToBBAttack)
        {
            psRedShield.Clear();
            psRedShield.Stop();
            MainCurrentBaseStage = Anim.GetCurrentAnimatorStateInfo(0);
            if (MainCurrentBaseStage.IsName("anim_close") && MainCurrentBaseStage.normalizedTime>=0.99)
            {
                CurrentState = eFSMState.BBA;
                rg.useGravity = false;
                return;
            }
            if(MainCurrentBaseStage.IsName("closed_Roll_Loop"))
            {
                Anim.SetBool("Roll_Anim", false);
                return;
            }
            if(MainCurrentBaseStage.IsName("anim_Idle_Loop_S"))
            {
                Anim.SetBool("Open_Anim", false);
                return;
            }
            return;
        }
        else if (CurrentState == eFSMState.BBA)
        {
            CheckTarget();
            Vector3 vpos = BossBBATarget.transform.position - mData.EnemySelf.transform.position;
            float dis = vpos.magnitude;
            Debug.Log("dis : " + dis);
            if (dis<=1.0f)
            {
                psBBA.Play();
                CurrentState = eFSMState.BBACheckAttackTrue;
                i_tmp_BBA = 1;
                f_CurrentTime = 0.0f;
                return;
            }
            else
            {
                //transform.Translate(vpos * 0.8f * Time.deltaTime, Space.World);
                transform.position = Vector3.Lerp(transform.position, BossBBATarget.transform.position, 0.01f);
                return;
            }
        }
        else if (CurrentState == eFSMState.Dead)
        {
            if(!mData.b_IsDead)
            {
                mData.b_IsDead = true;
                onDead.Invoke(gameObject);
                CurrentState = eFSMState.Finish;
            }
        }
        else if (CurrentState == eFSMState.BBACheckAttackTrue)
        {
            CheckTarget();
            Vector3 vpos = mData.EnemySelf.transform.position - BossBBATarget.transform.position;
            Vector3 tmpvpos = vpos;
            tmpvpos.y = 0;
            float dis = vpos.magnitude;
            if (f_CurrentTime>=10.0f)
            {
                psBBA.Clear();
                psBBA.Stop();
                rg.useGravity = true;
                CurrentState = eFSMState.Idle;
                psRedShield.Play();
                Anim.SetBool("Open_Anim", true);
                f_CurrentTime = 0;
                i_tmp_BBA = 1;
                return;
            }
            else if (f_CurrentTime>=i_tmp_BBA*0.5f)
            {
                if(!b_tmpBBA)
                {
                    i_tmp_BBA += 1;
                    Vector3 vposs = mData.TargetObject.transform.position - mData.EnemySelf.transform.position;
                    float diss = vposs.magnitude;
                    if(diss<=25.0f)
                    {
                        Ft.BeHit(30);
                        b_tmpBBA = true;
                        f_CurrentTime += Time.deltaTime;
                        return;
                    }
                }
            }
            else
            {
                b_tmpBBA = false;
                if (dis >= 10.0f)
                {
                    transform.RotateAround(BossBBATarget.transform.position, Vector3.up, 100 * Time.deltaTime);   //A绕着B的y轴进行旋转。
                }
                else
                {
                    transform.Translate(tmpvpos * 0.8f * Time.deltaTime, Space.World);
                    transform.RotateAround(BossBBATarget.transform.position, Vector3.up, 100 * Time.deltaTime);   //A绕着B的y轴进行旋转。
                }
                f_CurrentTime += Time.deltaTime;
                return;
            }
        }
        else if (CurrentState == eFSMState.TargetDead)
        {
            
            //關閉所有動作開關
        }
        else if (CurrentState == eFSMState.UnFight)
        {
            if (GameSystem.IsCombat)
            {

                float intensity = 1.0f;
                float f = Mathf.Pow(2, intensity);
                Color col = new Color(0 * f, 175 * f, 191 * f);
                EYE1.transform.localPosition = new Vector3(-0.003517041f, 0.0005175673f, 0.002170073f);
                EYE1.transform.rotation = Quaternion.identity;
                Renderer EYE1M = EYE1.GetComponent<Renderer>();
                EYE1M.material.SetColor("Color_B9138E63", col); //.color = Color.red;
                EYE2.transform.localPosition = new Vector3(0.003514206f, 0.0005175653f, 0.002170073f);
                EYE2.transform.rotation = Quaternion.identity;
                Renderer EYE2M = EYE2.GetComponent<Renderer>();
                EYE2M.material.SetColor("Color_B9138E63", col); //.color = Color.red;

                StartCoroutine(Disslove(0));
                StartCoroutine(StartCombatState(1));
            }
            return;
        }
        else if (CurrentState == eFSMState.Finish)
        {
            StartCoroutine(Disslove(1));
            //End
        }
    }
    void CheckTarget()
    {
        if (TargetNowCurrentState.currentHP <= 0)
        {
            CurrentState = eFSMState.TargetDead;
            return;
        }
    }
    void Func_BeHit()
    {
        if (b_BeHit)
        {
            float damage = Ft.GetHitValueAndReward();
            if (psRedShield.isPlaying)
            {
                Debug.Log("psRedShield Hit");
                mData.f_Hp -= damage/5;
                onUV.Invoke(damage/5);
            }
            if (ShieldThis.isPlaying)
            {
                Debug.Log("ShieldThis Hit");
                mData.f_ShieldHP -= damage;
                VEForShield.Invoke(damage);
            }
            if(!psRedShield.isPlaying && !ShieldThis.isPlaying)
            {
                Debug.Log("Normal Hit");
                mData.f_Hp -= damage*1.2f;
                onUV.Invoke(damage*1.2f);
            }
            b_BeHit = false;
            if (mData.f_Hp <= 0)
            {
                CurrentState = eFSMState.Dead;
                return;
            }
            return;
        }
    }
    void TurnToTarget(ref bool b_isTurnCurrect, GameObject TARGET)
    {
        Vector3 targetDirection = TARGET.transform.position - mData.EnemySelf.transform.position;
        targetDirection.y = 0;
        float step = 5.0f * Time.deltaTime;
        Vector3 newDirection = Vector3.RotateTowards(mData.EnemySelf.transform.forward, targetDirection, step, 0.0f);
        mData.EnemySelf.transform.rotation = Quaternion.LookRotation(newDirection);
        float result = Vector3.Dot(mData.EnemySelf.transform.forward.normalized, targetDirection.normalized);
        if (result > 0.999f)
        {
            b_isTurnCurrect = true;
        }
        else
        {
            b_isTurnCurrect = false;
        }
    }
    void TurnMoveToTarget(GameObject tmp, ref bool b_isHitHit)
    {
        Vector3 targetDirection = tmp.transform.position - mData.EnemySelf.transform.position;
        targetDirection.y = 0;
        float step = TurnMoveToTarget_TurnSpeed * Time.deltaTime;
        Vector3 newDirection = Vector3.RotateTowards(mData.EnemySelf.transform.forward, targetDirection, step, 0.0f);
        mData.EnemySelf.transform.rotation = Quaternion.LookRotation(newDirection);
        float stepp = TurnMoveToTarget_MoveSpeed * Time.deltaTime;
        mData.EnemySelf.transform.localPosition = Vector3.MoveTowards(gameObject.transform.localPosition, tmp.transform.position, stepp);
        Vector3 vpos = tmp.transform.position - mData.EnemySelf.transform.position;
        vpos.y = 0;
        float dis = vpos.magnitude;
        if(dis<=0.001)
        {
            b_isHitHit = true;
        }
        else
        {
            b_isHitHit = false;
        }
    }
    private void OnDrawGizmos()
    {
        if (CurrentState == eFSMState.TargetDead)
        {
            Gizmos.color = Color.gray;
        }
        else
        {
            return;
        }
        Gizmos.DrawWireSphere(this.transform.position, 25.0f);
    }
}
